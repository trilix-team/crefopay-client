<?php
/**
 * GetClearingFileList class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class GetClearingFileList
 * The API stub method for the getClearingFileList call
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/getclearingfilelist
 * @package Upg\Library\Api
 */
class GetClearingFileList extends AbstractApi
{
    /**
     * URI for the call
     */
    const GET_CLEARING_FILE_LIST_PATH = 'getClearingFileList';

    /**
     * Construct the stub
     *
     * @param Config                                   $config  Merchant config
     * @param \Upg\Library\Request\GetClearingFileList $request Request to be sent
     */
    public function __construct(Config $config, \Upg\Library\Request\GetClearingFileList $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::GET_CLEARING_FILE_LIST_PATH);
    }
}
