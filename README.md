# CrefoPay Client Library for PHP #

[![Build Status](https://travis-ci.org/vmrose/clientlibrary.svg?branch=master)](https://travis-ci.org/vmrose/clientlibrary)

PHP Client Library for the CrefoPay API.
Based on the API Documentation found here: https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/introduction
For questions regarding this library please send an email to [service@crefopay.de](mailto:service@crefopay.de)

## Known Issues ##
The following issues are known and will be added in coming versions:

 * No issues reported

## Installing the Library ##
Using [composer](https://getcomposer.org/) is the easiest way to install the library:
```bash
$ composer require vmrose/php-clientlibrary
```

Use the composer autoloader in your code:
```php
require_once 'vendor/autoload.php';
```

## Using the Library ##
For extensive information, please refer to the [wiki](https://github.com/vmrose/clientlibrary/wiki) of this repository

### Configuration ###
Every request requires a config object of the class ```Upg\Library\Config```.

The config object should be populated with at least the required fields by providing an associative array at instantiation.
```php
$configData = array(...);
$config = new Upg\Library\Config($configData);
```
The following fields **must** be defined in the array:

 * **merchantPassword** (*string*) This is your password, also known as private key.
 * **merchantID** (*string*) This is your merchant id.
 * **storeID** (*string*) This is your store id.
 * **logLocationMain** (*string*) Main log location file path. Currently not used.
 * **logLocationRequest** (*string*) Log location file path for all API requests.
 * **defaultLocale** (*string*) The default locale, the constants of the class ```Upg\Library\Locale\Codes``` define the current supported languages (or see our [Supported Languages](https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages)).
 * **baseUrl** (*string*) The base URL to which your requests will be sent. Our current endpoints are ```https://sandbox.crefopay.de/2.0``` or ```https://api.crefopay.de/2.0```.

The following fields **can** be defined in the array:

 * **logEnabled** (*bool*) Defines if logging should be enabled. Defaults to *false*.
 * **logLevel** (*int*) The log level, as defined by [Monolog](https://github.com/Seldaek/monolog/blob/master/src/Monolog/Logger.php). You can also use the Config class constants. Defaults to the *ERROR* log level (400).
 * **logLocationMNS** (*string*) Log location file path for our MNS. Currently not used. Defaults to empty string.
 * **logLocationCallbacks** (*string*) Log location file path for callbacks. Currently not used. Defaults to empty string.
 * **defaultRiskClass** (*int*) Default risk class of any transaction. Uses the constants of the class ```Upg\Library\Risk\RiskClass```. Defaults to ```RiskClass::RISK_CLASS_DEFAULT```
 * **sendRequestsWithSalt** (*bool*) Automatically add a salt to requests. The salt can be disabled for testing purposes. Defaults to *true*.

### Requests ###
The Library for requests is split in three parts:
```Upg\Library\Request``` contains the request classes.
```Upg\Library\Request\Objects``` contains classes for the JSON objects that are documented in the API documentation.
If a request has a property that requires a JSON object please pass in the appropriately populated ```Upg\Library\Request\Objects``` class for that property.

All properties in the request and JSON objects have getters and setters. For example, to set a field called userType on the request or object you would call `setUserType` and to get it you would call `getUserType`.

### Sending requests ###
Once you have populated a request object with the appropriate values simply create an instance of a ```Upg\Library\Api```
class for the appropriate method call. Pass in a config object and the request you want to send. Then call the
`sendRequest()` method to send the response. This method might raise an Exception or provide an object of the class SuccessResponse.

The exceptions which can be raised are in ```Upg\Library\Api\Exception```.
For any parsed responses you will have access to an ```Upg\Library\Response\SuccessResponse``` or ```Upg\Library\Response\FailureResponse``` object.
The SuccessResponse object is returned if no exception is thrown.
The FailureResponse object is returned in a ```Upg\Library\Api\Exception\ApiError``` exception.

### Responses ###

The response object has two types of properties:
Fixed properties such as the resultCode which are in every request, and dynamic properties such as allowedPaymentMethods which are not in every request. To access a property that is fixed or dynamic, simply use the following:
```php
$response->getData('resultCode');
$response->getData('allowedPaymentMethods');
```

If any response contains JSON objects or an array of objects then the library will attempt to serialize them into ```Upg\Library\Request\Objects``` classes.
The property names in responses that will be serialized are as follows:

 * allowedPaymentInstruments, paymentInstruments: Array of ```Upg\Library\Request\Objects\PaymentInstrument```
 * billingAddress, shippingAddress: ```Upg\Library\Request\Objects\Address```
 * amount: ```Upg\Library\Request\Objects\Amount```
 * companyData: ```Upg\Library\Request\Objects\Company```
 * paymentInstrument: ```Upg\Library\Request\Objects\PaymentInstrument```
 * userData: ```Upg\Library\Request\Objects\Person```

For example the response of the getUser-API call contains the following properties that will be serialized to the following objects:

 * *companyData* would be serialized to an object of the class ```Upg\Library\Request\Objects\Company```
 * *userData* field would be an ```Upg\Library\Request\Objects\Person``` object
 * *billingAddress* and *shippingAddress* would be ```Upg\Library\Request\Objects\Address``` objects

The MAC validation/calculation for requests and responses is handled by the library and if these fail an exception will be raised.

All variables that are not ISO values are defined in classes as constants for you to use in the request.
For possible fixed field values please see the following constants:

 * *locale*: ```Upg\Library\Locale\Codes```
 * *riskClasses*: ```Upg\Library\Risk\RiskClass```
 * *userType*: ```Upg\Library\User\Type```
 * *salutation*: ```Upg\Library\Request\Objects\Person::SALUTATIONFEMALE``` or ```Upg\Library\Request\Objects\Person::SALUTATIONMALE```
 * *companyRegisterType*: ```Upg\Library\Request\Objects\Company```
 * *paymentInstrumentType*: ```Upg\Library\Request\Objects\PaymentInstrument```
 * *issuer*: ```Upg\Library\Request\Objects\PaymentInstrument```

## Handling Callbacks ##

When using the HostedPage integration types you will receive [callbacks](https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/callback) from the API as a GET request. In that case the client library provides a helper for you: ```Upg\Library\Callback\Handler```

This class takes in the following for the constructor:

 * $config: The configuration object.
 * $data: The data from the get variables which should be an associated array containing the following keys:
   * merchantID
   * storeID
   * orderID
   * resultCode
   * merchantReference : Optional field
   * message : Optional field
   * salt
   * mac
 * $processor: An instance of a class that implements the ```Upg\Library\Callback\ProcessorInterface```, which the method will invoke after validation

The processor should implement two methods:
`sendData` which the handler uses to pass data to the processor to use and another method called `run`, which will get invoked to handle the callback processing.
This processor should return a string which contains a URL where the user should be redirected to after CrefoPay has processed the transaction.

To run the handler simply call the `run` method on the `Handler` object.
Please note the following exceptions may be raised, in which case the store must still send a URL.

 * ```Upg\Library\Callback\Exception\ParamNotProvided```: If a required parameter is not provided
 * ```Upg\Library\Callback\Exception\MacValidation```: If there is a MAC validation issue with the callback parameters

## Handling MNS notifications ##

Similar to the callback handling, the library also provides a helper class to validate MNS notifications.
This class is ```Upg\Library\Mns\Handler```.

It takes the following as a constructor:

  * $config: The config object for the integration
  * $data: The data from the post variables which should be an associated array of the MNS callback
  * $processor: An instance of a class which implements the ```Upg\Library\Mns\ProcessorInterface```, which the method will invoke after validation.

The processor object should implement `sendData` to get data from the handler and a `run` method which executes your callback after successful validation.

The processor callback should avoid processing the request, instead it should save it to a database for asynchronous processing via a cron script.

If you use the handler class to save a MNS to the database for later processing you can assume the MNS is perfectly valid with out checking the MAC.

Please note the MNS call must always return a 200 response to CrefoPay otherwise the CrefoPay system will retry to send this notification and eventually lock the MNS queue for your system.

## Working on the library ##

If you want to contribute to the library, please note that all code should be written according to the **PSR2 standard**.
There are many tools to conform to this standard, e.g. PHP-Codesniffer or PHP Coding Standards Fixer.
We will also specify a contributing guideline in the near future.
