<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\Person as PersonClass;
use Upg\Library\Response\Unserializer\Processor;

/**
 * Class Person
 *
 * Unserializer for user data
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/natural-person
 * @package Upg\Library\Response\Unserializer\Handler
 */
class Person implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return string
     */
    public function getAttributeNameHandler()
    {
        return 'userData';
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return PersonClass
     */
    public function unserializeProperty(Processor $processor, $value)
    {

        $person = new PersonClass();
        $person->setUnserializedData($value);

        $dob = new \DateTime();

        list($year, $month, $day) = explode('-', $value['dateOfBirth']);

        $dob->setDate($year, $month, $day);

        $person->setDateOfBirth($dob);

        return $person;
    }
}
