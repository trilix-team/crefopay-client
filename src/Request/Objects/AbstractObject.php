<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request\Objects;

use Upg\Library\Request\RequestInterface as RequestInterface;

/**
 * Class AbstractObject
 *
 * Abstract class for the json objects used in the requests
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/datastructures
 * @package Upg\Library\Request\Objects
 */
abstract class AbstractObject implements RequestInterface
{
    /**
     * Set data on the object for the unserializer
     *
     * @param array $data
     *
     * @return $this
     */
    public function setUnserializedData(array $data)
    {
        try {
            $reflector = new \ReflectionClass($this);
        } catch (\ReflectionException $re) {
            return $this;
        }

        $properties = $reflector->getProperties();

        foreach ($properties as $property) {
            $key = $property->getName();
            if (array_key_exists($key, $data)) {
                $property->setAccessible(true);
                $property->setValue($this, $data[$key]);
                $property->setAccessible(false);
            }
        }

        return $this;
    }

    /**
     * As all CrefoPay objects are a json when inside of a request
     *
     * Simply return json for all of them
     *
     * @return string
     */
    public function getSerialiseType()
    {
        return 'json';
    }

    /**
     * Gets the data to serialize
     *
     * By default validator data will be same as serialized data
     * However this can be different and classes where it is different shall implement both getSerializerData and
     * toArray separately
     *
     * @return mixed
     */
    public function getSerializerData()
    {
        return $this->toArray();
    }

    /**
     * Returns custom validation rules
     *
     * Return array with validation errors in the following format
     * array(
     *  'class Name'=>array
     *      ('value'=> array(
     *                      'message',
     *                      'message'
     *                      )
     *      )
     * )
     *
     * @return array
     */
    public function customValidation()
    {
        return array();
    }
}
