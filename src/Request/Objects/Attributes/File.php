<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request\Objects\Attributes;

use Upg\Library\Request\Objects\Attributes\Exception\FileCouldNotBeFound;

/**
 * Class File
 *
 * For file parameters in the request
 *
 * @package Upg\Library\Request\Objects\Attributes
 */
class File implements FileInterface
{
    /**
     * The file path
     *
     * @var string
     */
    private $path;

    /**
     * Set the file path
     *
     * @param $path
     *
     * @return $this
     *
     * @throws FileCouldNotBeFound
     */
    public function setPath($path)
    {
        if (!file_exists($path)) {
            throw new FileCouldNotBeFound($path);
        }

        $this->path = $path;
        return $this;
    }

    /**
     * Get the set file path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Return base 64 encoded string with the file
     *
     * @return string
     */
    public function getFileBase64String()
    {
        $data = file_get_contents($this->path);
        if (empty($data)) {
            return '';
        }

        return base64_encode($data);
    }
}
