<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\Amount as AmountClass;
use Upg\Library\Response\Unserializer\Processor;

/**
 * Class Amount
 *
 * Unserializer for amount data
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/company
 * @package Upg\Library\Response\Unserializer\Handler
 */
class Amount implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'amount',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return AmountClass
     */
    public function unserializeProperty(Processor $processor, $value)
    {

        $amount = new AmountClass();
        $amount->setUnserializedData($value);

        return $amount;
    }
}
