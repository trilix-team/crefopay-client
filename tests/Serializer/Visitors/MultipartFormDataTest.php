<?php

namespace Upg\Library\Tests\Serializer\Visitors;

use Upg\Library\Request\Objects\Attributes\File;
use Upg\Library\Serializer\Serializer;
use Upg\Library\Serializer\Visitors\Json;
use Upg\Library\Serializer\Visitors\MultipartFormData;
use Upg\Library\Tests\Mock\Request\MultipartRequest;
use Upg\Library\Tests\Mock\Request\NonRecursiveJsonRequest;

class MultipartFormDataTest extends \PHPUnit\Framework\TestCase
{
    public function testNonRecursiveSerialization()
    {
        $request = new MultipartRequest();

        $multipartRequest = new MultipartFormData();

        $request->data = array(
            'test1' => 1,
            'test2' => 2,
        );

        $serializedData = $multipartRequest->visit($request, new Serializer());

        $this->assertNotEmpty($serializedData, "serializer returned nothing");
        $this->assertEquals(array('test1' => 1, 'test2' => 2), $serializedData, "serializer returned did not work");
    }

    public function testNonRecursiveSerializationWithFile()
    {
        $request = new MultipartRequest();
        $multipartRequest = new MultipartFormData();

        $file = new File();
        $file->setPath(__FILE__);

        $request->data = array(
            'test1' => 1,
            'file' => $file,
        );

        $serializedData = $multipartRequest->visit($request, new Serializer());
        $this->assertNotEmpty($serializedData, "serializer returned nothing");

        $this->assertArrayHasKey('test1', $serializedData, "test1 field is non existent");
        $this->assertArrayHasKey('file', $serializedData, "test1 field is non existent");

        $this->assertEquals(1, $serializedData['test1']);
        $this->assertEquals("FILE::".__FILE__, $serializedData['file']);
    }

    public function testRecursiveSerialization()
    {
        $request = new MultipartRequest();

        $multipartRequest = new MultipartFormData();

        $request->data = array(
            'test1' => 1,
            'json' => new NonRecursiveJsonRequest(),
        );

        $serializer = new Serializer();

        $serializer->setVisitor(new Json());

        $serializedData = $multipartRequest->visit($request, $serializer);

        $this->assertNotEmpty($serializedData, "serializer returned nothing");

        $this->assertArrayHasKey('test1', $serializedData, "test1 field is non existent");
        $this->assertArrayHasKey('json', $serializedData, "test1 field is non existent");

        $this->assertEquals(1, $serializedData['test1']);

        $this->assertJson($serializedData['json']);
        $this->assertJsonStringEqualsJsonString($serializedData['json'], '{"test":1,"test2":2}');
    }
}
