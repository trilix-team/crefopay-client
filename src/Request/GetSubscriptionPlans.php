<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request;

use Upg\Library\Request\Objects\TimeRange;
use Upg\Library\Request\Objects\AmountRange;

use Upg\Library\Validation\Helper\Constants;

/**
 * Class GetSubscriptionPlans
 *
 * @package Upg\Library\Request
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/getsubscriptionplans
 */
class GetSubscriptionPlans extends AbstractRequest
{
    /**
     * Daily subscription plan
     */
    const INTERVAL_DAILY = "DAILY";

    /**
     * Weekly subscription plan
     */
    const INTERVAL_WEEKLY = "WEEKLY";

    /**
     * 4 weekly subscription plan
     */
    const INTERVAL_FOUR_WEEKLY = "FOUR_WEEKLY";

    /**
     * Monthly subscription plan
     */
    const INTERVAL_MONTHLY = "MONTHLY";

    /**
     * Quarterly subscription plan
     */
    const INTERVAL_QUARTERLY = "QUARTERLY";

    /**
     * Biannually subscription plan
     */
    const INTERVAL_BIANNUALLY = "BIANNUALLY";

    /**
     * Yearly subscription plan
     */
    const INTERVAL_YEARLY = "YEARLY";

    /**
     * Tag for the context validation
     */
    const TAG_INTERVAL = "INTERVAL";

    /**
     * The interval of the plan
     *
     * @see GetSubscriptionPlans::INTERVAL_DAILY
     * @see GetSubscriptionPlans::INTERVAL_WEEKLY
     * @see GetSubscriptionPlans::INTERVAL_FOUR_WEEKLY
     * @see GetSubscriptionPlans::INTERVAL_MONTHLY
     * @see GetSubscriptionPlans::INTERVAL_QUARTERLY
     * @see GetSubscriptionPlans::INTERVAL_BIANNUALLY
     * @see GetSubscriptionPlans::INTERVAL_YEARLY
     * @var string
     */
    private $interval;

    /**
     * The time range (1-366)
     *
     * @var TimeRange
     */
    private $trialPeriodRange;

    /**
     * The amount range of the plans (min / max Amount)
     *
     * @var AmountRange
     */
    private $amount;

    /**
     * The page to be returned
     *
     * @var integer
     */
    private $pageNumber;

    /**
     * The amount of plans per page (1-100)
     *
     * @var integer
     */
    private $pageSize;

    /**
     * Set the interval field in the request
     *
     * @see GetSubscriptionPlans::$interval
     *
     * @param string $interval
     *
     * @return $this
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;
        return $this;
    }

    /**
     * Get the value of the interval field
     *
     * @see GetSubscriptionPlans::$interval
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * Set the trialPeriodRange field
     *
     * @see GetSubscriptionPlans::$trialPeriodRange
     *
     * @param TimeRange $trialPeriodRange
     *
     * @return $this
     */
    public function setTrialPeriodRange(TimeRange $trialPeriodRange)
    {
        $this->trialPeriodRange = $trialPeriodRange;
        return $this;
    }

    /**
     * Get value of the trialPeriodRange field
     *
     * @see GetSubscriptionPlans::$trialPeriodRange
     * @return TimeRange
     */
    public function getTrialPeriodRange()
    {
        return $this->trialPeriodRange;
    }

    /**
     * Set the amount field
     *
     * @see GetSubscriptionPlans::$amount
     *
     * @param AmountRange $amount
     *
     * @return $this
     */
    public function setAmount(AmountRange $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get value of the amount field
     *
     * @see GetSubscriptionPlans::$amount
     * @return AmountRange
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the pageNumber field
     *
     * @see GetSubscriptionPlans::$pageNumber
     *
     * @param integer $pageNumber
     *
     * @return $this
     */
    public function setPageNumber($pageNumber)
    {
        $this->pageNumber = $pageNumber;
        return $this;
    }

    /**
     * Get value of the pageNumber field
     *
     * @see GetSubscriptionPlans::$pageNumber
     * @return integer
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * Set the pageSize field
     *
     * @see GetSubscriptionPlans::$pageSize
     *
     * @param integer $pageSize
     *
     * @return $this
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
        return $this;
    }

    /**
     * Get value of the pageSize field
     *
     * @see GetSubscriptionPlans::$pageSize
     * @return integer
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $return = array(
            'amount' => $this->getAmount()
        );

        if ($this->interval) {
            $return['interval'] = $this->getInterval();
        }

        if ($this->trialPeriodRange) {
            $return['trialPeriodRange'] = $this->getTrialPeriodRange();
        }

        if ($this->pageNumber) {
            $return['pageNumber'] = $this->getPageNumber();
        }

        if ($this->pageSize) {
            $return['pageSize'] = $this->getPageSize();
        }

        return $return;
    }

    /**
     * Get the validation data
     *
     * @return array
     */
    public function getClassValidationData()
    {
        $validationData = array();

        $validationData['amount'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Amount is required, but not set"
        );

        if (!empty($this->interval)) {
            $validationData['interval'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateInterval',
                'message' => "Interval is not a valid value"
            );
        }

        if (!empty($this->pageNumber)) {
            $validationData['pageNumber'][] = array(
                'name' => 'Integer',
                'value' => null,
                'message' => "PageNumber is not an integer"
            );
        }

        if (!empty($this->pageSize)) {
            $validationData['pageSize'][] = array(
                'name' => 'Integer',
                'value' => null,
                'message' => "PageSize is not an integer"
            );
            $validationData['pageSize'][] = array(
                'name' => 'Between',
                'value' => array('min' => 1, 'max' => 100),
                'message' => "PageSize can not exceed the boundaries"
            );
        }

        return $validationData;
    }

    /**
     * Validate the interval
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateInterval($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_INTERVAL);
    }
}
