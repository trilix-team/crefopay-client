<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\PaymentInstrument;
use Upg\Library\Response\Unserializer\Processor;

/**
 * Class PaymentInstruments
 *
 * Unserializer for paymentInstrument data
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/company
 * @package Upg\Library\Response\Unserializer\Handler
 */
class PaymentInstruments implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'paymentInstrument',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return PaymentInstrument
     */
    public function unserializeProperty(Processor $processor, $value)
    {

        $paymentInstrument = new PaymentInstrument();
        $paymentInstrument->setUnserializedData($value);

        if ($paymentInstrument->getPaymentInstrumentType() == PaymentInstrument::PAYMENT_INSTRUMENT_TYPE_CARD) {
            list($year, $month) = explode('-', $value['validity']);
            $dateTime = new \DateTime();
            $dateTime->setDate($year, $month, 1);
            $paymentInstrument->setValidity($dateTime);
        }

        return $paymentInstrument;
    }
}
