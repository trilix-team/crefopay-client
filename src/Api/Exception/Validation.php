<?php
/**
 * Validation class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api\Exception;

/**
 * Class Validation
 *
 * Raised if the request does not pass the preceding validation
 *
 * @package Upg\Library\Api\Exception
 */
class Validation extends AbstractException
{
    /**
     * ValidationResults
     *
     * @var array
     */
    private $validationResults;

    /**
     * Constructor
     *
     * @param array $validationResults
     */
    public function __construct($validationResults)
    {
        $this->validationResults = $validationResults;
        parent::__construct("Validation did not pass");
    }

    /**
     * Get the validation errors
     *
     * @deprecated 1.0.3 Backwards compatibility ensured for implementations using this function
     * @return array
     */
    public function getVailidationResults()
    {
        return $this->validationResults;
    }

    /**
     * Get the validation errors
     *
     * @return array
     */
    public function getValidationResults()
    {
        return $this->validationResults;
    }
}
