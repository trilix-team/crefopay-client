<?php

namespace Upg\Library\Tests\Request\Objects;

use Faker\Factory as Factory;

use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Objects\SubscriptionPlan;

use Upg\Library\Tests\Request\AbstractRequestTest;
use Upg\Library\Validation\Validation;

class SubscriptionPlanTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function setUp()
    {
        $faker = Factory::create();


        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown()
    {
        unset($this->faker);
    }

    public function testSubscriptionPlanTestValidationSuccess()
    {
        $plan = new SubscriptionPlan();
        $plan->setPlanReference("D3M0")
            ->setName("Demoabo")
            ->setDescription("Demoabo fuer CrefoWallet")
            ->setAmount(new Amount(1000))
            ->setInterval(SubscriptionPlan::INTERVAL_MONTHLY)
            ->setTrialPeriod(90)
            ->setBasicPaymentsCount(36)
            ->setContactDetails("service@crefopay.de")
            ->setHasSubscribers(true);

        $validation = new Validation();
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testSubscriptionPlanTestValidationPlanReferenceFailure()
    {
        $plan = new SubscriptionPlan();

        $validation = new Validation();

        $plan->setName("Demoabo")
            ->setDescription("Demoabo fuer CrefoWallet")
            ->setAmount(new Amount(1000))
            ->setInterval(SubscriptionPlan::INTERVAL_MONTHLY)
            ->setTrialPeriod(90)
            ->setBasicPaymentsCount(36)
            ->setContactDetails("service@crefopay.de")
            ->setHasSubscribers(true);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'planReference',
            'PlanReference is required',
            $data,
            "PlanReference requirement was not validated"
        );

        $plan->setPlanReference($this->veryLongString);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'planReference',
            'PlanReference must be no more than 15 characters long',
            $data,
            "PlanReference exceeds character limit"
        );
    }

    public function testSubscriptionPlanTestValidationNameFailure()
    {
        $plan = new SubscriptionPlan();

        $validation = new Validation();

        $plan->setPlanReference("D3M0")
            ->setDescription("Demoabo fuer CrefoWallet")
            ->setAmount(new Amount(1000))
            ->setInterval(SubscriptionPlan::INTERVAL_MONTHLY)
            ->setTrialPeriod(90)
            ->setBasicPaymentsCount(36)
            ->setContactDetails("service@crefopay.de")
            ->setHasSubscribers(true);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'name',
            'Name is required',
            $data,
            "Name requirement was not validated"
        );

        $plan->setName($this->veryLongString);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'name',
            'Name must be no more than 25 characters long',
            $data,
            "Name exceeds character limit"
        );
    }

    public function testSubscriptionPlanTestValidationDescriptionFailure()
    {
        $plan = new SubscriptionPlan();

        $validation = new Validation();

        $plan->setPlanReference("D3M0")
            ->setName("Demoabo")
            ->setAmount(new Amount(1000))
            ->setInterval(SubscriptionPlan::INTERVAL_MONTHLY)
            ->setTrialPeriod(90)
            ->setBasicPaymentsCount(36)
            ->setContactDetails("service@crefopay.de")
            ->setHasSubscribers(true);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'description',
            'Description is required',
            $data,
            "Description requirement was not validated"
        );

        $plan->setDescription($this->veryLongString);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'description',
            'Description must be no more than 100 characters long',
            $data,
            "Description exceeds character limit"
        );
    }

    public function testSubscriptionPlanTestValidationAmountFailure()
    {
        $plan = new SubscriptionPlan();

        $validation = new Validation();

        $plan->setPlanReference("D3M0")
            ->setName("Demoabo")
            ->setDescription("Demoabo fuer CrefoWallet")
            ->setInterval(SubscriptionPlan::INTERVAL_MONTHLY)
            ->setTrialPeriod(90)
            ->setBasicPaymentsCount(36)
            ->setContactDetails("service@crefopay.de")
            ->setHasSubscribers(true);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'amount',
            'Amount is required',
            $data,
            "Amount requirement was not validated"
        );
    }

    public function testSubscriptionPlanTestValidationIntervalFailure()
    {
        $plan = new SubscriptionPlan();

        $validation = new Validation();

        $plan->setPlanReference("D3M0")
            ->setName("Demoabo")
            ->setDescription("Demoabo fuer CrefoWallet")
            ->setAmount(new Amount(1000))
            ->setTrialPeriod(90)
            ->setBasicPaymentsCount(36)
            ->setContactDetails("service@crefopay.de")
            ->setHasSubscribers(true);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'interval',
            'Interval is required',
            $data,
            "Interval requirement was not validated"
        );

        $plan->setInterval($this->veryLongString);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'interval',
            'Interval is not a valid value',
            $data,
            "Interval validation did not execute"
        );
    }

    public function testSubscriptionPlanTestValidationTrialPeriodFailure()
    {
        $plan = new SubscriptionPlan();

        $validation = new Validation();

        $plan->setPlanReference("D3M0")
            ->setName("Demoabo")
            ->setDescription("Demoabo fuer CrefoWallet")
            ->setAmount(new Amount(1000))
            ->setInterval(SubscriptionPlan::INTERVAL_MONTHLY)
            ->setTrialPeriod(500)
            ->setBasicPaymentsCount(36)
            ->setContactDetails("service@crefopay.de")
            ->setHasSubscribers(true);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'trialPeriod',
            'TrialPeriod time is not in the allowed range',
            $data,
            "TrialPeriod value validation did not validate"
        );
    }

    public function testSubscriptionPlanTestValidationPaymentCountFailure()
    {
        $plan = new SubscriptionPlan();

        $validation = new Validation();

        $plan->setPlanReference("D3M0")
            ->setName("Demoabo")
            ->setDescription("Demoabo fuer CrefoWallet")
            ->setAmount(new Amount(1000))
            ->setInterval(SubscriptionPlan::INTERVAL_MONTHLY)
            ->setTrialPeriod(90)
            ->setBasicPaymentsCount(10000)
            ->setContactDetails("service@crefopay.de")
            ->setHasSubscribers(true);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'basicPaymentsCount',
            'BasicPaymentsCount value is not in the allowed range',
            $data,
            "BasicPaymentsCount value validation did not validate"
        );
    }

    public function testSubscriptionPlanTestValidationContactDetailsFailure()
    {
        $plan = new SubscriptionPlan();

        $validation = new Validation();

        $plan->setPlanReference("D3M0")
            ->setName("Demoabo")
            ->setDescription("Demoabo fuer CrefoWallet")
            ->setAmount(new Amount(1000))
            ->setInterval(SubscriptionPlan::INTERVAL_MONTHLY)
            ->setTrialPeriod(90)
            ->setBasicPaymentsCount(10000)
            ->setContactDetails($this->veryLongString)
            ->setHasSubscribers(true);
        $validation->getValidator($plan);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\SubscriptionPlan',
            'contactDetails',
            'ContactDetails must be no more than 100 characters long',
            $data,
            "ContactDetails value validation did not validate"
        );
    }
}
