<?php
/**
 * Visitors for serialization
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Serializer\Visitors;

use Upg\Library\Serializer\Exception\VisitorCouldNotBeFound;
use Upg\Library\Serializer\Visitors\VisitorInterface as VisitorInterface;
use Upg\Library\Request\RequestInterface as RequestInterface;
use Upg\Library\Serializer\Serializer as Serializer;

/**
 * Class AbstractVisitor
 *
 * Abstract class for visitors
 *
 * @package Upg\Library\Serializer\Visitors
 */
abstract class AbstractVisitor implements VisitorInterface
{

    /**
     * Checks whether the value needs walker for the sub properties
     *
     * @param Object $value
     *
     * @return bool
     */
    protected function checkIfWalkerIsNeeded($value)
    {
        if ($value instanceof RequestInterface) {
            return true;
        }

        return false;
    }

    /**
     * Checks the provided array and serializes deeper values
     *
     * @param array                              $data
     * @param \Upg\Library\Serializer\Serializer $serializer
     *
     * @return array
     * @throws VisitorCouldNotBeFound
     */
    protected function checkSerializeArray($data, Serializer $serializer)
    {
        foreach ($data as $key => $value) {
            if ($this->checkIfWalkerIsNeeded($value)) {
                $data[$key] = $serializer->serialize($value);
            }
        }

        return $data;
    }
}
