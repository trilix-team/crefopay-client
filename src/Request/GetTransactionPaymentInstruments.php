<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request;

/**
 * Class GetTransactionPaymentInstruments
 *
 * The getTransactionPaymentInstruments call adds the functionality to get a transactions‘s payment instruments.
 * Input data consist of the following:
 * Order information (existing order-id)
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/gettrxpaymentinstruments
 * @package Upg\Library\Request
 */
class GetTransactionPaymentInstruments extends AbstractRequest
{
    /**
     * This is the order number of the shop.
     *
     * This id is created by the shop and is used as identifier for this transaction
     *
     * @var string
     */
    private $orderID;

    /**
     * Set the Order ID
     *
     * @param string $orderID
     *
     * @return $this
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * Get the set order ID
     *
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        return array(
            'orderID' => $this->getOrderID(),
        );
    }

    /**
     * Get the validation
     *
     * @return array
     */
    public function getClassValidationData()
    {
        $validationData = array();

        $validationData['orderID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "orderID is required"
        );

        $validationData['orderID'][] = array(
            'name' => 'MaxLength',
            'value' => '30',
            'message' => "orderID must be between 1 and 30 characters"
        );

        return $validationData;
    }
}
