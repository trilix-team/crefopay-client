<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\PaymentMethods;

use Upg\Library\Validation\Helper\Constants;

/**
 * Class PaymentMethods
 *
 * Contains the payment methods and validator
 *
 * @package Upg\Library\PaymentMethods
 */
class Methods
{
    /**
     * Payment method: Direct Debit
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_DD = "DD";

    /**
     * Payment methods: Credit Card
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_CC = "CC";

    /**
     * Payment method: Credit Card 3D Secure
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_CC3D = "CC3D";

    /**
     * Payment method: Cash in advance
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_PREPAID = "PREPAID";

    /**
     * Payment method: PayPal
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_PAYPAL = "PAYPAL";

    /**
     * Payment method: Sofort
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_SU = "SU";

    /**
     * Payment method: Bill Payment
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_BILL = "BILL";

    /**
     * Payment method: Bill Payment Secure
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_BILL_SECURE = "BILL_SECURE";

    /**
     * Payment method: Cash on delivery
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_COD = "COD";

    /**
     * Payment method: Ideal
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_IDEAL = "IDEAL";

    /**
     * Payment method: Installment
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/paymentmethods
     */
    const PAYMENT_METHOD_TYPE_INSTALLMENT = "INSTALLMENT";

    /**
     * Tag for the constraint validator
     */
    const VALIDATION_TAG_PAYMENT_METHOD = "PAYMENT_METHOD_TYPE";

    /**
     * Validation function
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validate($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::VALIDATION_TAG_PAYMENT_METHOD);
    }
}
