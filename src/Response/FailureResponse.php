<?php
/**
 * FailureResponse class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response;

use Upg\Library\Config;
use Upg\Library\Error\Codes;

/**
 * Class FailureResponse
 *
 * Used for failure and error responses
 *
 * @see     AbstractResponse
 * @package Upg\Library\Response
 */
class FailureResponse extends AbstractResponse
{
    /**
     * Error message
     *
     * @var string
     */
    private $errorStatusMessage;

    /**
     * Constructor
     *
     * @param Config $config
     * @param array  $data
     */
    public function __construct(Config $config, array $data = array())
    {
        if (array_key_exists('resultCode', $data)) {
            $this->errorStatusMessage = Codes::getErrorName($data['resultCode']);
        }
        parent::__construct($config, $data);
    }

    /**
     * Return the error status message from the library
     *
     * Please note the message field will contain the error from CrefoPay
     *
     * @see AbstractResponse::message
     * @return string
     */
    public function getErrorStatusMessage()
    {
        return $this->errorStatusMessage;
    }
}
