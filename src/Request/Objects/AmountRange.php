<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request\Objects;

/**
 * Class AmountRange
 *
 * For the AmountRange json objects
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/getsubscriptionplans
 * @package Upg\Library\Request\Objects
 */
class AmountRange extends AbstractRange
{
    /**
     * Constructor
     *
     * @param Amount $minAmount
     * @param Amount $maxAmount
     */
    public function __construct(Amount $minAmount = null, Amount $maxAmount = null)
    {
        if (!is_null($minAmount)) {
            $this->setMinimum($minAmount);
        }
        if (!is_null($maxAmount)) {
            $this->setMaximum($maxAmount);
        }
    }

    /**
     * Validation meta data
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/getsubscriptionplans
     * @return array
     */
    public function getValidationData()
    {
        $validationData = array();

        $validationData['minimum'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Minimum amount is required"
        );
        $validationData['maximum'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Maximum amount is required"
        );

        return $validationData;
    }
}
