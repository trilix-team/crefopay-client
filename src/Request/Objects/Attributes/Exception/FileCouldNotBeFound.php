<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request\Objects\Attributes\Exception;

/**
 * Class FileCouldNotBeFound
 *
 * Raised if file can not be found
 *
 * @package Upg\Library\Request\Objects\Attributes\Exception
 */
class FileCouldNotBeFound extends AbstractException
{
    /**
     * Constructor
     *
     * @param string $filePath
     */
    public function __construct($filePath = '')
    {
        parent::__construct("File Could not be found: " . $filePath);
    }
}
