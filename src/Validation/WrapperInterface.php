<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Validation;

use Upg\Library\Request\RequestInterface as RequestInterface;

/**
 * Interface WrapperInterface
 *
 * Interface for the validator
 *
 * @package Upg\Library\Validation
 */
interface WrapperInterface
{
    /**
     * Gets the validator for the given requests' class
     *
     * @param RequestInterface $request
     */
    public function getValidator(RequestInterface $request);
}
