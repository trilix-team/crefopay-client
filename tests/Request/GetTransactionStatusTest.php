<?php

namespace Upg\Library\Tests\Request;

use Faker\Factory;
use Upg\Library\Config;
use Upg\Library\Request\GetTransactionStatus;
use Upg\Library\Validation\Validation;

class GetTransactionStatusTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp()
    {
        $faker = Factory::create();

        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;

        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
        ));
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    public function testGetTransactionStatusValidationSuccess()
    {
        $request = new GetTransactionStatus($this->config);
        $request->setOrderID(1);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testGetTransactionStatusValidationOrderID()
    {
        $request = new GetTransactionStatus($this->config);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\GetTransactionStatus',
            'orderID',
            'orderID is required',
            $data,
            "orderID is required failed to trigger"
        );
    }

    public function testGetTransactionStatusValidationReturnRiskData()
    {
        $request = new GetTransactionStatus($this->config);
        $request->setOrderID(1)
            ->setReturnRiskData(true);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testGetTransactionStatusReturnRiskDataSuccess()
    {
        $request = new GetTransactionStatus($this->config);
        $request->setOrderID(1)
            ->setReturnRiskData(true);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }
}