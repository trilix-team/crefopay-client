<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Callback;

use Upg\Library\Callback\Exception\MacValidation;
use Upg\Library\Config;
use Upg\Library\Mac\AbstractCalculator;

/**
 * Class MacCalculator
 *
 * Mac calculator for call backs and MNS notifications
 *
 * @package Upg\Library\Callback
 */
class MacCalculator extends AbstractCalculator
{
    /**
     * Given HMAC
     *
     * @var string
     */
    private $mac;

    /**
     * Raw request
     *
     * @var array
     */
    private $rawRequest;

    /**
     * Constructor
     *
     * @param Config $config
     * @param array  $request
     */
    public function __construct(Config $config, array $request)
    {
        $this->setConfig($config);
        $this->setRequest($request);
    }

    /**
     * Sets the request
     *
     * @param array $request
     *
     * @return $this
     */
    public function setRequest(array $request)
    {
        $this->setCalculationArray($request);

        $this->rawRequest = $request;

        if (array_key_exists('mac', $request)) {
            $this->mac = $request['mac'];
        }
        return $this;
    }

    /**
     * Validation function
     *
     * @return bool
     *
     * @throws MacValidation
     */
    public function validateResponse()
    {
        if (!parent::validate($this->mac, false)) {
            throw new MacValidation($this->calculateMac(), $this->mac, $this->rawRequest);
        }

        return true;
    }
}
