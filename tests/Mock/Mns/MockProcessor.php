<?php

namespace Upg\Library\Tests\Mock\Mns;

class MockProcessor implements \Upg\Library\Mns\ProcessorInterface
{
    public $data = array();
    /**
     * @param $merchantID integer This is the merchantID assigned by CrefoPay.
     * @param $storeID string This is the store ID of a merchant assigned by CrefoPay as a merchant can have more than one store.
     * @param $orderID string This is the order number tyhat the shop has assigned
     * @param $captureID string The confirmation ID of the capture. Only sent for Notifications that belong to captures
     * @param $merchantReference string Reference that can be set by the merchant during the createTransaction call.
     * @param $paymentReference string The reference number of the
     * @param $userID string The unique user id of the customer.
     * @param $amount integer|string This is either the amount of an incoming payment or “0” in case of some status changes
     * @param $currency string Currency code according to ISO4217.
     * @param $transactionStatus string Current status of the transaction. Same values as resultCode
     * @param $orderStatus string Possible values: PAID PAYPENDING PAYMENTFAILED CHARGEBACK CLEARED. Status of order
     * @param $additionalData string Json string with additional data
     * @param $timestamp string Unix timestamp, Notification timestamp
     * @param $version string notification version (currently 1.5)
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/notification-call
     */
    public function sendData(
        $merchantID,
        $storeID,
        $orderID,
        $captureID,
        $merchantReference,
        $paymentReference,
        $userID,
        $amount,
        $currency,
        $transactionStatus,
        $orderStatus,
        $additionalData,
        $timestamp,
        $version
    ) {
        $this->data = array(
            'merchantID' => $merchantID,
            'storeID' => $storeID,
            'orderID' => $orderID,
            'captureID' => $captureID,
            'merchantReference' => $merchantReference,
            'paymentReference' => $paymentReference,
            'userID' => $userID,
            'amount' => $amount,
            'currency' => $currency,
            'transactionStatus' => $transactionStatus,
            'orderStatus' => $orderStatus,
            'additionalData' => $additionalData,
            'timestamp' => $timestamp,
            'version' => $version
        );
    }

    /**
     * The run method used by the processor to run successfully validated MNS notifications.
     * This should not return anything
     */
    public function run()
    {
        return null;
    }
}