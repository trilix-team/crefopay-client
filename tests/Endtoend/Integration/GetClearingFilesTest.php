<?php

namespace Upg\Library\Tests\Endtoend\Integration;

use Faker\Factory;
use Upg\Library\Config;
use Upg\Library\Api\Exception\ApiError;
use Upg\Library\Request\GetClearingFiles as GetClearingFilesRequest;
use Upg\Library\Api\GetClearingFiles as GetClearingFilesApi;

/**
 * @group Endtoend
 */
class GetClearingFilesTest extends \PHPUnit\Framework\TestCase
{
    private $faker;

    /**
     * Config object for tests
     *
     * @var Config
     */
    private $config;

    public function setUp()
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->faker = $faker;

        $merchantPassword = trim(getenv('CREFOPAY_TEST_MERCHANT_PASSWORD'));
        $merchantID = trim(getenv('CREFOPAY_TEST_MERCHANT_ID'));
        $storeID = trim(getenv('CREFOPAY_TEST_STORE_ID'));
        $baseURL = trim(getenv('CREFOPAY_TEST_BASE_URL'));
        $logEnabled = false;

        if (!empty($merchantPassword) && !empty($merchantID) && !empty($storeID) && !empty($baseURL)) {
            $this->config = new Config(array(
                'merchantPassword' => $merchantPassword,
                'merchantID' => $merchantID,
                'storeID' => $storeID,
                'logEnabled' => $logEnabled,
                'sendRequestsWithSalt' => true,
                'baseUrl' => $baseURL
            ));
        } else {
            $this->config = null;
        }
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    /**
     * Make an successful call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');
            return false;
        }

        $clearingFilesRequest = new GetClearingFilesRequest($this->config);
        $clearingFilesRequest->setClearingFileID("8")
            ->setPath("");

        $clearingFiles = new GetClearingFilesApi($this->config, $clearingFilesRequest);
        $result = $clearingFiles->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('filename'));
        $file = $result->getData('filename');
        $this->assertNotEmpty($file);
        unlink($file);
    }

    /**
     * Make an successful call
     */
    public function testFailedApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');
            return false;
        }

        $clearingFilesRequest = new GetClearingFilesRequest($this->config);
        $clearingFilesRequest->setClearingFileID("1a")
            ->setPath("");

        $clearingFiles = new GetClearingFilesApi($this->config, $clearingFilesRequest);
        try {
            $clearingFiles->sendRequest();
        } catch (ApiError $e) {
            $this->assertEquals(6002, $e->getParsedResponse()->getData('resultCode'));
        }
    }
}