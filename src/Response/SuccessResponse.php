<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response;

/**
 * Class SuccessResponse
 *
 * Used for success responses
 *
 * @see     AbstractResponse
 * @package Upg\Library\Response
 */
class SuccessResponse extends AbstractResponse
{
}
