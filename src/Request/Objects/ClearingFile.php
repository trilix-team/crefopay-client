<?php
/**
 * ClearingFile class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Request\Objects;

/**
 * Class ClearingFile
 *
 * For the ClearingFile json objects
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/clearing-file
 * @package Upg\Library\Request\Objects
 */
class ClearingFile extends AbstractObject
{

    /**
     * ID of the clearing file
     *
     * @var string
     */
    private $clearingID;

    /**
     * Start date
     *
     * @var \DateTime
     */
    private $from;

    /**
     * End date
     *
     * @var \DateTime
     */
    private $to;

    /**
     * Set the clearing ID for the object
     *
     * @param string $id
     *
     * @return $this
     */
    public function setClearingID($id)
    {
        $this->clearingID = $id;
        return $this;
    }

    /**
     * Get the clearing ID that has been set
     *
     * @return string
     */
    public function getClearingID()
    {
        return $this->clearingID;
    }

    /**
     * Set the start date
     *
     * @param \DateTime $from
     *
     * @return $this
     */
    public function setFrom(\DateTime $from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * Return the start date
     *
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set the vat rate
     *
     * @param \DateTime $to
     *
     * @return $this
     */
    public function setTo(\DateTime $to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * Get the end date
     *
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Returns objects' data as array
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'clearingID' => $this->clearingID,
        );

        if ($this->from instanceof \DateTime) {
            $return['from'] = $this->getFrom()->format('Y-m-d');
        }

        if ($this->to instanceof \DateTime) {
            $return['to'] = $this->getTo()->format('Y-m-d');
        }

        return $return;
    }

    /**
     * Validation meta data
     *
     * @see https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/clearing-file
     * @return array
     */
    public function getValidationData()
    {
        $validationData = array();

        $validationData['clearingID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "ClearingID is required"
        );
        $validationData['from'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Start date is required"
        );
        $validationData['to'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "End date is required"
        );

        return $validationData;
    }
}
