<?php
/**
 * MacValidation class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api\Exception;

/**
 * Class MacValidation
 * Raised if the MAC in the response is incorrect
 *
 * @package Upg\Library\Api\Exception
 */
class MacValidation extends AbstractException
{
    /**
     * Constructor
     *
     * @param string $calculatedMac
     * @param string $expected
     * @param string $rawJson
     */
    public function __construct($calculatedMac, $expected, $rawJson)
    {
        parent::__construct("Calculated \"$calculatedMac\" but expected \"$expected\" for raw response: $rawJson");
    }
}
