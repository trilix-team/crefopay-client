<?php
/**
 * GetTransactionPaymentInstruments class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class GetTransactionPaymentInstruments
 * Api stub for getTransactionPaymentInstruments call
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/gettrxpaymentinstruments
 * @package Upg\Library\Api
 */
class GetTransactionPaymentInstruments extends AbstractApi
{
    /**
     * URI of the API destination
     */
    const GET_TRANSACTION_PAYMENT_INSTRUMENTS_PATH = 'getTransactionPaymentInstruments';

    /**
     * Construct the API stub
     *
     * @param Config                                                $config  Config for the merchant
     * @param \Upg\Library\Request\GetTransactionPaymentInstruments $request Request to be sent
     */
    public function __construct(Config $config, \Upg\Library\Request\GetTransactionPaymentInstruments $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the url
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::GET_TRANSACTION_PAYMENT_INSTRUMENTS_PATH);
    }
}
