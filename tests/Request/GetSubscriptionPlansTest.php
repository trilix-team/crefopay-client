<?php

namespace Upg\Library\Tests\Request;

use Faker\Factory;
use Upg\Library\Config;
use Upg\Library\Request\GetSubscriptionPlans;
use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Objects\TimeRange;
use Upg\Library\Request\Objects\AmountRange;
use Upg\Library\Validation\Validation;

class GetSubscriptionPlansTest extends AbstractRequestTest
{
    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    /**
     * faker
     * @var
     */
    private $faker;

    public function setUp()
    {
        $this->faker = Factory::create();

        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
        ));
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    public function testGetSubscriptionPlansValidationSuccess()
    {
        $request = new GetSubscriptionPlans($this->config);
        $request->setInterval(GetSubscriptionPlans::INTERVAL_MONTHLY)
            ->setTrialPeriodRange(new TimeRange(1, 30))
            ->setAmount(new AmountRange(new Amount(1), new Amount(1000)))
            ->setPageNumber(1)
            ->setPageSize(25);

        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testGetSubscriptionPlansValidationFailureInterval()
    {
        $request = new GetSubscriptionPlans($this->config);
        $request->setInterval("TEST")
            ->setAmount(new AmountRange(new Amount(1), new Amount(1000)));

        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\GetSubscriptionPlans',
            'interval',
            'Interval is not a valid value',
            $data,
            "Interval validity validation failed to trigger"
        );
    }

    public function testGetSubscriptionPlansValidationFailurePageNumber()
    {
        $request = new GetSubscriptionPlans($this->config);
        $request->setPageNumber("TEST")
            ->setAmount(new AmountRange(new Amount(1), new Amount(1000)));

        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\GetSubscriptionPlans',
            'pageNumber',
            'PageNumber is not an integer',
            $data,
            "Page number integer validation failed to trigger"
        );
    }

    public function testGetSubscriptionPlansValidationFailurePageSize()
    {
        $request = new GetSubscriptionPlans($this->config);
        $request->setPageSize("TEST")
            ->setAmount(new AmountRange(new Amount(1), new Amount(1000)));

        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\GetSubscriptionPlans',
            'pageSize',
            'PageSize is not an integer',
            $data,
            "Page size integer validation failed to trigger"
        );

        $request->setPageSize(200);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\GetSubscriptionPlans',
            'pageSize',
            'PageSize can not exceed the boundaries',
            $data,
            "Page size boundary validation failed to trigger"
        );
    }
}