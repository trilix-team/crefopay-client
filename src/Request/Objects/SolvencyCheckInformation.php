<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request\Objects;

/**
 * Class SolvencyCheckInformation
 *
 * For solvencyCheckInformation json objects
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/solvency-check-information
 * @package Upg\Library\Request\Objects
 */
class SolvencyCheckInformation extends AbstractObject
{
    /**
     * A boniversum result
     *
     * @var Boniversum
     * @deprecated 1.0.7 The field and its functions were renamed to bonimaResult
     */
    private $boniversumResult;

    /**
     * A bonima result
     *
     * @var Bonima
     */
    private $bonimaResult;

    /**
     * A crediconnect result
     *
     * @var Crediconnect
     */
    private $crediConnectResult;

    /**
     * A crefo result
     *
     * @var Creditreform
     */
    private $crefoResult;

    /**
     * Set the boniversumResult field
     *
     * @deprecated 1.0.7 The field and its functions were renamed to bonimaResult
     *
     * @param Boniversum $boniversumResult
     *
     * @return $this
     */
    public function setBoniversumResult(Boniversum $boniversumResult)
    {
        $this->boniversumResult = $boniversumResult;
        return $this;
    }

    /**
     * Get the boniversumResult field
     *
     * @deprecated 1.0.7 The field and its functions were renamed to bonimaResult
     * @return Boniversum
     */
    public function getBoniversumResult()
    {
        return $this->boniversumResult;
    }

    /**
     * Set the bonimaResult field
     *
     * @param Bonima $bonimaResult
     *
     * @return $this
     */
    public function setBonimaResult(Bonima $bonimaResult)
    {
        $this->bonimaResult = $bonimaResult;
        return $this;
    }

    /**
     * Get the bonimaResult field
     *
     * @return Bonima
     */
    public function getBonimaResult()
    {
        return $this->bonimaResult;
    }

    /**
     * Set the crediConnectResult field
     *
     * @param Crediconnect $crediConnectResult
     *
     * @return $this
     */
    public function setCrediConnectResult(Crediconnect $crediConnectResult)
    {
        $this->crediConnectResult = $crediConnectResult;
        return $this;
    }

    /**
     * Get the crediConnectResult field
     *
     * @return Crediconnect
     */
    public function getCrediConnectResult()
    {
        return $this->crediConnectResult;
    }

    /**
     * Set the crefoResult field
     *
     * @param Creditreform $crefoResult
     *
     * @return $this
     */
    public function setCrefoResult(Creditreform $crefoResult)
    {
        $this->crefoResult = $crefoResult;
        return $this;
    }

    /**
     * Get the crefoResult field
     *
     * @return Creditreform
     */
    public function getCrefoResult()
    {
        return $this->crefoResult;
    }

    /**
     * Convert to array for validator
     *
     * @return array
     */
    public function toArray()
    {
        $return = array();

        if ($this->bonimaResult) {
            $return['bonimaResult'] = $this->getBonimaResult();
        }

        if ($this->crediConnectResult) {
            $return['crediConnectResult'] = $this->getCrediConnectResult();
        }

        if ($this->crefoResult) {
            $return['crefoResult'] = $this->getCrefoResult();
        }

        return $return;
    }

    /**
     * Validation meta data
     *
     * @see https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/solvency-check-information
     * @return array
     */
    public function getValidationData()
    {
        $validationData = array();

        return $validationData;
    }
}
