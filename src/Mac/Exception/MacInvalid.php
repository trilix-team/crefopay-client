<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Mac\Exception;

/**
 * Class MacInvalid
 *
 * Raised if MAC is invalid
 *
 * @package Upg\Library\Mac\Exception
 */
class MacInvalid extends \Upg\Library\AbstractException
{
    /**
     * Constructor
     *
     * @param string $expectedMac
     * @param string $calculatedMac
     */
    public function __construct($expectedMac, $calculatedMac)
    {
        parent::__construct("Invalid MAC. Expected MAC: {$expectedMac}; Calculated MAC: {$calculatedMac}");
    }
}
