<?php
/**
 * GetSubscriptionPlans class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class GetSubscriptionPlans
 * API stub for the GetSubscriptionPlans call
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/getsubscriptionplans
 * @package Upg\Library\Api
 */
class GetSubscriptionPlans extends AbstractApi
{
    /**
     * URI for the getSubscriptionPlans call
     */
    const GET_SUBSCRIPTION_PLANS_PATH = 'getSubscriptionPlans';

    /**
     * Construct the API stub class
     *
     * @param Config                                    $config  Config for store owner
     * @param \Upg\Library\Request\GetSubscriptionPlans $request Request to be sent
     */
    public function __construct(Config $config, \Upg\Library\Request\GetSubscriptionPlans $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the url
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::GET_SUBSCRIPTION_PLANS_PATH);
    }
}
