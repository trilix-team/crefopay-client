<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request;

/**
 * Class GetTransactionStatus
 *
 * This is the request class for any getTransactionStatus request object
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/gettransactionstatus
 * @package Upg\Library\Request
 */
class GetTransactionStatus extends AbstractRequest
{
    /**
     * This is the order number of the shop.
     *
     * This id is created by the shop and is used as identifier for this transaction
     *
     * @var string
     */
    private $orderID;

    /**
     * Defines if the risk checks for this transaction should be returned.
     *
     * @var boolean
     */
    private $returnRiskData;

    /**
     * Set the Order ID
     *
     * @see GetTransactionStatus::orderID
     *
     * @param string $orderID
     *
     * @return $this
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * Get the set order ID
     *
     * @see GetTransactionStatus::orderID
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Set the flag to return risk check data
     *
     * @see GetTransactionStatus::returnRiskData
     *
     * @param boolean $returnRiskData
     *
     * @return $this
     */
    public function setReturnRiskData($returnRiskData)
    {
        $this->returnRiskData = $returnRiskData;
        return $this;
    }

    /**
     * Get the flag to return risk check data
     *
     * @see GetTransactionStatus::returnRiskData
     * @return boolean
     */
    public function getReturnRiskData()
    {
        return $this->returnRiskData;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $data = array(
            'orderID' => $this->getOrderID(),
        );
        if (!empty($this->returnRiskData)) {
            $data['returnRiskData'] = $this->getReturnRiskData();
        }
        return $data;
    }

    /**
     * Get the validation
     *
     * @return array
     */
    public function getClassValidationData()
    {
        $validationData = array();

        $validationData['orderID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "orderID is required"
        );

        $validationData['orderID'][] = array(
            'name' => 'MaxLength',
            'value' => '30',
            'message' => "orderID must be between 1 and 30 characters"
        );

        return $validationData;
    }
}
