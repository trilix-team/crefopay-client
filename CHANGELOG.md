# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

## [Unreleased]
- BIG CHANGE: We will amend the library to use the namespace CrefoPay soon. Since this is a big breaking change, we'll release a new major version for this.
- BREAKING CHANGE: Due to recent reports, we will look into validating the set values as soon as they're set to immediately give feedback to the developer if something is wrong.
- We will remove the getToken API in Version 1.0.15 since the PayCoBridge is discontinued and getToken is only necessary for the PayCoBridge Integration.

## [1.0.13] - 2018-09-20
### Fixed
- Added alias for class Address in the new solvency data unserializer

## [1.0.12] - 2018-09-10
### Added
- Added support for the new field returnRiskData in the APIs GetTransactionStatus and GetUser
- Added the object CrediConnect for solvencyCheckInformation in the createTransaction API

### Deprecated
- The getToken API is now deprecated and will be removed in version 1.0.15

## [1.0.11] - 2018-07-25
### Added
- Added support for the new additional field in the address

### Deprecated
- The field billingRecipientAddition in the createTransaction and createSubscription requests was deprecated in favor of the new field mentioned above
- The field shippingRecipientAddition in the createTransaction and createSubscription requests was deprecated in favor of the new field mentioned above

## [1.0.10] - 2018-06-18
### Fixed
- Fixed a critical error, that caused the library to fail in some cases due to class name confusion

## [1.0.9] - 2018-06-15
### Added
- Added support for the updateSubscription API
- Added support for the Pay by Link API

## [1.0.8] - 2018-05-11
### Added
- Added support for the createSubscription API
- Added support for the getSubscriptionPlans API
- Added the new integrationType "Secure Fields"
- Added the new field bonimaResult to the solvencyCheckInformation

### Deprecated
- The Boniversum result class was deprecated

## [1.0.7] - 2018-01-16
### Added
- Added the objects for solvencyCheckInformation in the createTransaction API
- Added the tests for solvencyCheckInformation and its objects

## [1.0.6] - 2017-12-14
### Added
- The company object now allows setting the new email field.

### Changed
- The company object now requires the companyName to be set.
- Added tests for the changes in the company object.

## [1.0.5] - 2017-11-23
### Fixed
- Fixed the persons' salutation to not be required as per API documentation

## [1.0.4] - 2017-11-20
### Added
- Added debug logging to the callback and MNS handler

### Changed
- Changed some tests
- Changed the documentation links in the files

## [1.0.3]
### Added
- Added support for the getClearingFiles API
- Added support for the getToken API

### Changed
- Updated README
- Changed tests
- Changed Config to have some default values

## [1.0.2]
### Added
- Added new function to get the paymentInstruments' unmasked IBAN
- Added support for the getTransactionPaymentInstruments API
- Added support for the getClearingFileList API
- Added support for the verifyCard API
- Added error codes

### Deprecated
- The function "getVailidationResults" is deprecated, use the function "getValidationResults"

### Changed
- The validation library version is now specified and not using the development branch
- Updated README
- Updated error codes
- Updated composer.json

[1.0.1] https://github.com/UPGcarts/clientlibrary/compare/1.0.0...1.0.1
[1.0.0] https://github.com/UPGcarts/clientlibrary/releases/tag/1.0.0