<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Response\Unserializer\Processor;

/**
 * Interface UnserializerInterface
 *
 * Unserializer interface
 *
 * @package Upg\Library\Response\Unserializer\Handler
 */
interface UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * Please note the method can return an array of strings
     *
     * @return string | array
     */
    public function getAttributeNameHandler();

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return \Upg\Library\Request\RequestInterface
     */
    public function unserializeProperty(Processor $processor, $value);
}
