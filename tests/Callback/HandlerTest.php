<?php

namespace Upg\Library\Tests\Callback;

use Upg\Library\Callback\Exception\ParamNotProvided;
use Upg\Library\Callback\Handler;
use Upg\Library\Config;
use Upg\Library\Tests\Mock\Callback\MockProcessor;

class HandlerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Log file location
     * @var string
     */
    protected static $file = './callback.log';

    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public static function tearDownAfterClass()
    {
        unlink(self::$file);
    }

    public function setUp()
    {
        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
            'baseUrl' => "http://www.something.com/",
            'logEnabled' => true,
            'logLevel' => Config::LOG_LEVEL_DEBUG,
            'logLocationCallbacks' => $this->getLogLocation()
        ));
    }

    public function tearDown()
    {
        unset($this->config);
    }

    public function getLogLocation()
    {
        return self::$file;
    }

    public function getNonLoggingConfig()
    {
        return new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
            'baseUrl' => "http://www.something.com/",
            'logEnabled' => true,
            'logLevel' => Config::LOG_LEVEL_DEBUG,
        ));
    }

    /**
     * Test if no log is created if no location is set
     */
    public function testNoLogCreation()
    {
        $data = array(
            'notificationType' => 'PAYMENT_INSTRUMENT_SELECTION',
            'merchantID' => 10,
            'storeID' => 'store',
            'orderID' => 10,
            'paymentMethod' => 'CC',
            'resultCode' => 0,
            'merchantReference' => 'test',
            'additionalInformation' => '{"test":1}',
            'paymentInstrumentID' => 1,
            'message' => 'test',
            'salt' => "randomSalt",
            'mac' => "2429c36de46b23b9f5920de18be90e4a5a58439b"
        );

        $processor = new MockProcessor();

        $handler = new Handler($this->getNonLoggingConfig(), $data, $processor);
        $handler->run();

        $this->assertFileNotExists($this->getLogLocation());
    }

    public function testSuccessfulCallBack()
    {
        $data = array(
            'notificationType' => 'PAYMENT_INSTRUMENT_SELECTION',
            'merchantID' => 10,
            'storeID' => 'store',
            'orderID' => 10,
            'paymentMethod' => 'CC',
            'resultCode' => 0,
            'merchantReference' => 'test',
            'additionalInformation' => '{"test":1}',
            'paymentInstrumentID' => 1,
            'message' => 'test',
            'salt' => "randomSalt",
            'mac' => "2429c36de46b23b9f5920de18be90e4a5a58439b"
        );

        $processor = new MockProcessor();

        $handler = new Handler($this->config, $data, $processor);
        $result = $handler->run();

        $expected = json_encode(array('url'=>'http://something.com/success'));

        $this->assertJsonStringEqualsJsonString($expected, $result);

        $data['additionalInformation'] = array('test'=>1);
        $data['paymentInstrumentsPageUrl'] = '';

        $this->assertArraySubset($processor->data, $data);

        $this->assertFileExists($this->getLogLocation());
    }

    public function testSuccessfulCallBackWithOptionalParam()
    {
        $data = array(
            'notificationType' => 'PAYMENT_INSTRUMENT_SELECTION',
            'merchantID' => 10,
            'storeID' => 'store',
            'orderID' => 10,
            'paymentMethod' => 'CC',
            'resultCode' => 0,
            'merchantReference' => 'test',
            'additionalInformation' => '{"test":1}',
            'paymentInstrumentID' => 1,
            'paymentInstrumentsPageUrl' => "http://something.com",
            'message' => 'test',
            'salt' => "randomSalt",
            'mac' => "5ecb4686ffc2794a6568df85627b965fa773e0ab"
        );

        $processor = new MockProcessor();

        $handler = new Handler($this->config, $data, $processor);
        $result = $handler->run();

        $expected = json_encode(array('url'=>'http://something.com/success'));

        $this->assertJsonStringEqualsJsonString($expected, $result);

        $data['additionalInformation'] = array('test'=>1);

        $this->assertArraySubset($processor->data, $data);

        $this->assertFileExists($this->getLogLocation());
    }

    /**
     * Test if ParamNotProvided exception is thrown
     * @expectedException \Upg\Library\Callback\Exception\ParamNotProvided
     */
    public function testParamException()
    {
        $data = array(
            'storeID' => 'store',
            'orderID' => 10,
            'resultCode' => 0,
            'merchantReference' => 'test',
            'message' => 'test'
        );

        $processor = new MockProcessor();

        try {
            $handler = new Handler($this->config, $data, $processor);
            $handler->run();
        } catch (ParamNotProvided $e) {
            $this->assertFileExists($this->getLogLocation());
            throw $e;
        }
    }

    /**
     * Test if validation exception is thrown
     * @expectedException \Upg\Library\Callback\Exception\MacValidation
     */
    public function testMacValidationException()
    {
        $data = array(
            'notificationType' => 'PAYMENT_INSTRUMENT_SELECTION',
            'merchantID' => 10,
            'storeID' => 'store',
            'orderID' => 10,
            'paymentMethod' => 'CC',
            'resultCode' => 0,
            'merchantReference' => 'test',
            'additionalInformation' => '{test:1}',
            'paymentInstrumentID' => 1,
            'message' => 'test',
            'salt' => "randomSalt",
            'mac' => "TESTMAC"
        );

        $processor = new MockProcessor();

        $handler = new Handler($this->config, $data, $processor);
        $handler->run();
    }
}
