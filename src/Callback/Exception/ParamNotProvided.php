<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Callback\Exception;

/**
 * Class ParamNotProvided
 *
 * @package Upg\Library\Callback\Exception
 */

class ParamNotProvided extends AbstractException
{
    /**
     * Constructor
     *
     * @param string $params
     */
    public function __construct($params)
    {
        parent::__construct("The following parameters were not provided or are empty: " . $params);
    }
}
