<?php

namespace Upg\Library\Tests\Request;

use Faker\Factory;
use Upg\Library\Config;
use Upg\Library\Request\UpdateSubscription;

use Upg\Library\Validation\Validation;

class UpdateSubscriptionTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp()
    {
        $faker = Factory::create();

        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;

        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
        ));
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    public function testCancelValidationSuccess()
    {
        $request = new UpdateSubscription($this->config);
        $request->setSubscriptionID(1)
            ->setAction(UpdateSubscription::ACTION_CANCEL);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");

        $request->setSubscriptionID(1)
            ->setAction(UpdateSubscription::ACTION_CHANGE_RATE)
            ->setRate(1000);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCancelValidationSubscriptionID()
    {
        $request = new UpdateSubscription($this->config);
        $request->setAction(UpdateSubscription::ACTION_CANCEL);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\UpdateSubscription',
            'subscriptionId',
            'subscriptionId is required',
            $data,
            "subscriptionId is required failed to trigger"
        );
    }

    public function testCancelValidationSubscriptionIDLimit()
    {
        $request = new UpdateSubscription($this->config);
        $request->setSubscriptionID($this->veryLongString)
            ->setAction(UpdateSubscription::ACTION_CANCEL);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\UpdateSubscription',
            'subscriptionId',
            'subscriptionId must be between 1 and 30 characters',
            $data,
            "subscriptionId length validation failed to trigger"
        );
    }

    public function testCancelValidationAction()
    {
        $request = new UpdateSubscription($this->config);
        $request->setSubscriptionID(1);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\UpdateSubscription',
            'action',
            'action is required',
            $data,
            "action is required failed to trigger"
        );
    }

    public function testCancelValidationRate()
    {
        $request = new UpdateSubscription($this->config);
        $request->setSubscriptionID(1)
            ->setAction(UpdateSubscription::ACTION_CHANGE_RATE);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\UpdateSubscription',
            'rate',
            'rate is required',
            $data,
            "Rate requirement failed to trigger"
        );

        $request->setRate(10000000000);

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\UpdateSubscription',
            'rate',
            'rate must be between 1 and 9 digits',
            $data,
            "rate length validation failed to trigger"
        );
    }
}