<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request\Objects;

use Upg\Library\Validation\Helper\Constants;

/**
 * Class Boniversum
 *
 * For boniversum json objects
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/boniversum
 * @package Upg\Library\Request\Objects
 */
class Boniversum extends AbstractObject
{

    const IDENTIFICATION_PERSON_HOUSEHOLD_NOT_IDENTIFIED = "PERSON_HOUSEHOLD_NOT_IDENTIFIED";
    const IDENTIFICATION_PERSON_HOUSEHOLD_IDENTIFIED = "PERSON_HOUSEHOLD_IDENTIFIED";
    const IDENTIFICATION_PERSON_IDENTIFIED = "PERSON_IDENTIFIED";
    const IDENTIFICATION_HOUSEHOLD_IDENTIFIED = "HOUSEHOLD_IDENTIFIED";
    const IDENTIFICATION_PERSON_NOT_IDENTIFIED = "PERSON_NOT_IDENTIFIED";
    const IDENTIFICATION_BUILDING_IDENTIFIED = "BUILDING_IDENTIFIED";
    const IDENTIFICATION_PERSON_DECEASED = "PERSON_DECEASED";

    const ADDRESS_SUCCESSFULLY_VALIDATED = "SUCCESSFULLY_VALIDATED";
    const ADDRESS_CORRECTED_AND_VALIDATED = "CORRECTED_AND_VALIDATED";
    const ADDRESS_NOT_CORRECTED_OR_VALIDATED = "NOT_CORRECTED_OR_VALIDATED";

    const TRAFFIC_LIGHT_NONE = "NONE";
    const TRAFFIC_LIGHT_RED = "RED";
    const TRAFFIC_LIGHT_YELLOW = "YELLOW";
    const TRAFFIC_LIGHT_GREEN = "GREEN";

    const TAG_IDENTIFICATION = "IDENTIFICATION";
    const TAG_ADDRESS_VALIDATION = "ADDRESS";
    const TAG_TRAFFIC_LIGHT = "TRAFFIC_LIGHT";

    /**
     * @var \DateTime Request date
     */
    private $requestDate;

    /**
     * @var string Identification result
     */
    private $identification;

    /**
     * @var string Address Validation result
     */
    private $addressValidationStatus;

    /**
     * @var string Traffic Light Result
     */
    private $trafficLightColor;

    /**
     * @var double Score Result
     */
    private $score;

    /**
     * Set the requestDate field
     *
     * @param \DateTime $requestDate
     *
     * @return $this
     */
    public function setRequestDate(\DateTime $requestDate)
    {
        $this->requestDate = $requestDate;
        return $this;
    }

    /**
     * Get the requestDate field
     *
     * @return \DateTime
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * Set the identification field
     *
     * @param string $identification
     *
     * @return $this
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;
        return $this;
    }

    /**
     * Get the identification field
     *
     * @return string
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * Set the addressValidationStatus field
     *
     * @param string $addressValidationStatus
     *
     * @return $this
     */
    public function setAddressValidationStatus($addressValidationStatus)
    {
        $this->addressValidationStatus = $addressValidationStatus;
        return $this;
    }

    /**
     * Get the addressValidationStatus field
     *
     * @return string
     */
    public function getAddressValidationStatus()
    {
        return $this->addressValidationStatus;
    }

    /**
     * Set the trafficLightColor field
     *
     * @param string $trafficLightColor
     *
     * @return $this
     */
    public function setTrafficLightColor($trafficLightColor)
    {
        $this->trafficLightColor = $trafficLightColor;
        return $this;
    }

    /**
     * Get the trafficLightColor field
     *
     * @return string
     */
    public function getTrafficLightColor()
    {
        return $this->trafficLightColor;
    }

    /**
     * Set the score field
     *
     * @param double $score
     *
     * @return $this
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }

    /**
     * Get the score field
     *
     * @return double
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Convert to array for validator
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'identification' => $this->getIdentification(),
            'addressValidationStatus' => $this->getAddressValidationStatus(),
            'trafficLightColor' => $this->getTrafficLightColor(),
            'score' => $this->getScore(),
        );
        if ($this->requestDate && $this->requestDate instanceof \DateTime) {
            $return['requestDate'] = $this->getRequestDate()->format('Y-m-d');
        } else {
            $return['requestDate'] = "";
        }

        return $return;
    }

    /**
     * Validation data
     *
     * @see https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/boniversum
     * @return array
     */
    public function getValidationData()
    {
        $validationData = array();

        $validationData['requestDate'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "RequestDate is required"
        );

        $validationData['identification'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Identification is required"
        );

        $validationData['identification'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateIdentification',
            'message' => "Identification must have a valid value"
        );

        $validationData['addressValidationStatus'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "AddressValidationStatus is required"
        );

        $validationData['addressValidationStatus'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateAddressValidation',
            'message' => "AddressValidationStatus must have a valid value"
        );

        $validationData['trafficLightColor'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "TrafficLightColor is required"
        );

        $validationData['trafficLightColor'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateTrafficLight',
            'message' => "TrafficLightColor must have a valid value"
        );

        $validationData['score'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Score is required"
        );

        $validationData['score'][] = array(
            'name' => 'Number',
            'value' => null,
            'message' => "Score must be a float value"
        );

        return $validationData;
    }

    /**
     * Validate the identification type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateIdentification($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_IDENTIFICATION);
    }

    /**
     * Validate the addressValidation type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateAddressValidation($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_ADDRESS_VALIDATION);
    }

    /**
     * Validate the trafficLight type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateTrafficLight($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_TRAFFIC_LIGHT);
    }
}
