<?php
/**
 * Class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Request\Objects;

use Upg\Library\PaymentMethods\Methods;
use Upg\Library\Validation\Helper\Constants;

/**
 * Class SolvencyData
 *
 * For solvencyData json objects
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/
 * @package Upg\Library\Request\Objects
 */
class SolvencyData extends AbstractObject
{

    const CHECK_INTERFACE_BONIMA = "BONIMA";
    const CHECK_INTERFACE_BUERGEL = "BUERGEL";
    const CHECK_INTERFACE_CREDICONNECT = "CREDICONNECT";
    const CHECK_INTERFACE_CREDITREFORM = "CREDITREFORM";
    const CHECK_INTERFACE_CUBE = "CUBE";
    const CHECK_INTERFACE_FLEXCONNECT = "FLEXCONNECT";
    const CHECK_INTERFACE_VERITA = "VERITA";

    const CHECK_TYPE_FIRSTLEVEL = "FIRST_LEVEL_CHECK";
    const CHECK_TYPE_SECONDLEVEL = "SECOND_LEVEL_CHECK";

    const CHECK_ACTION_DECLINED = "DECLINED";
    const CHECK_ACTION_HIGH = "HIGH";
    const CHECK_ACTION_RESTRICTION = "RESTRICTION";
    const CHECK_ACTION_RESTRICTION_AND_MANUAL_CHECK = "RESTRICTION_AND_MANUAL_CHECK";
    const CHECK_ACTION_MANUAL_CHECK = "MANUAL_CHECK";
    const CHECK_ACTION_NONE = "NONE";

    const CHECK_TRAFFIC_LIGHT_GREEN = "GREEN";
    const CHECK_TRAFFIC_LIGHT_YELLOW = "YELLOW";
    const CHECK_TRAFFIC_LIGHT_RED = "RED";
    const CHECK_TRAFFIC_LIGHT_NONE = "NONE";

    const CHECK_IDENTIFICATION_PERSON_HOUSEHOLD_NOT_IDENTIFIED = "PERSON_HOUSEHOLD_NOT_IDENTIFIED";
    const CHECK_IDENTIFICATION_PERSON_HOUSEHOLD_IDENTIFIED = "PERSON_HOUSEHOLD_IDENTIFIED";
    const CHECK_IDENTIFICATION_PERSON_IDENTIFIED = "PERSON_IDENTIFIED";
    const CHECK_IDENTIFICATION_HOUSEHOLD_IDENTIFIED = "HOUSEHOLD_IDENTIFIED";
    const CHECK_IDENTIFICATION_PERSON_NOT_IDENTIFIED = "PERSON_NOT_IDENTIFIED";
    const CHECK_IDENTIFICATION_BUILDING_IDENTIFIED = "BUILDING_IDENTIFIED";
    const CHECK_IDENTIFICATION_PERSON_DECEASED = "PERSON_DECEASED";

    const CHECK_ADDRESS_VALIDATION_SUCCESSFULLY_VALIDATED = "SUCCESSFULLY_VALIDATED";
    const CHECK_ADDRESS_VALIDATION_CORRECTED_AND_VALIDATED = "CORRECTED_AND_VALIDATED";
    const CHECK_ADDRESS_VALIDATION_NOT_CORRECTED_OR_VALIDATED = "NOT_CORRECTED_OR_VALIDATED";

    const CHECK_RESULT_BLACK = "BLACK";
    const CHECK_RESULT_WHITE = "WHITE";

    const CHECK_PRODUCT_NAME_IDENTIFICATION = "IDENTIFICATION";
    const CHECK_PRODUCT_NAME_RISK_CHECK = "RISK_CHECK";
    const CHECK_PRODUCT_NAME_TRAFFIC_LIGHT_REPORT = "TRAFFIC_LIGHT_REPORT";
    const CHECK_PRODUCT_NAME_E_CREFO = "E_CREFO";
    const CHECK_PRODUCT_NAME_BRIEF_REPORT = "BRIEF_REPORT";
    const CHECK_PRODUCT_NAME_COMPACT_REPORT = "COMPACT_REPORT";

    const CHECK_STATUS_ACTIVE = "ACTIVE";
    const CHECK_STATUS_INSOLVENCY = "INSOLVENCY";
    const CHECK_STATUS_STOP_DISTRIBUTION = "STOP_DISTRIBUTION";
    const CHECK_STATUS_UNKNOWN = "UNKNOWN";
    const CHECK_STATUS_UNDER_DISABILITY = "UNDER_DISABILITY";
    const CHECK_STATUS_DEAD = "DEAD";

    const CHECK_SPECIAL_ADDRESS_S1 = "S1";
    const CHECK_SPECIAL_ADDRESS_S2 = "S2";
    const CHECK_SPECIAL_ADDRESS_S3 = "S3";
    const CHECK_SPECIAL_ADDRESS_S4 = "S4";
    const CHECK_SPECIAL_ADDRESS_S5 = "S5";
    const CHECK_SPECIAL_ADDRESS_S6 = "S6";
    const CHECK_SPECIAL_ADDRESS_S7 = "S7";
    const CHECK_SPECIAL_ADDRESS_S8 = "S8";

    const TAG_SOLVENCY_INTERFACE = "CHECK_INTERFACE";
    const TAG_CHECK_TYPE = "CHECK_TYPE";
    const TAG_ACTION = "CHECK_ACTION";
    const TAG_TRAFFIC_LIGHT = "CHECK_TRAFFIC_LIGHT";
    const TAG_IDENTIFICATION = "CHECK_IDENTIFICATION";
    const TAG_ADDRESS_VALIDATION = "CHECK_ADDRESS_VALIDATION";
    const TAG_CHECK_RESULT = "CHECK_RESULT";
    const TAG_PRODUCT_NAME = "CHECK_PRODUCT_NAME";
    const TAG_STATUS = "CHECK_STATUS";
    const TAG_SPECIAL_ADDRESS = "CHECK_SPECIAL_ADDRESS";

    /**
     * Definition which check interface delivered this information
     *
     * @var string
     */
    private $solvencyInterface;

    /**
     * Last request date
     *
     * @var \DateTime
     */
    private $lastRequestDate;

    /**
     * Date of the check
     *
     * @var \DateTime
     */
    private $checkDate;

    /**
     * Type of the check
     *
     * @var string
     */
    private $checkType;

    /**
     * Payment method for which the check was done
     *
     * @var string
     */
    private $paymentMethod;

    /**
     * Actions that were done after the check
     *
     * @var array
     */
    private $actions;

    /**
     * Error message
     *
     * @var string
     */
    private $message;

    /**
     * If any third party was requested for the check
     *
     * @var boolean
     */
    private $thirdPartyRequested;

    /**
     * Order ID of the transaction this check was done for
     *
     * @var string
     */
    private $orderID;

    /**
     * Score result
     *
     * double in case of BONIMA and BUERGEL checks
     * integer in case of CUBE and VERITA checks
     *
     * @var double|integer
     */
    private $score;

    /**
     * Traffic light result
     *
     * Doesn't exist in BUERGEL checks
     *
     * @var string
     */
    private $trafficLight;

    /**
     * Identification result
     *
     * Only for BONIMA and VERITA
     *
     * @var string
     */
    private $identification;

    /**
     * Address validation result
     *
     * Only for BONIMA and VERITA
     *
     * @var string
     */
    private $addressValidation;

    /**
     * Corrected address result
     *
     * Only for BONIMA, CUBE and VERITA
     * Format follows:
     * "<street> <houseNo>, <zip> <city>, <countryCode>"
     *
     * @var string
     */
    private $correctedAddress;

    /**
     * Address origin
     *
     * Only for BUERGEL
     *
     * @var string
     */
    private $addressOrigin;

    /**
     * Address origin code
     *
     * Only for BUERGEL
     *
     * @var integer
     */
    private $addressOriginCode;

    /**
     * Decision message
     *
     * Only for BUERGEL
     *
     * @var string
     */
    private $decisionMessage;

    /**
     * Status code
     *
     * Only for BUERGEL
     *
     * @var string
     */
    private $statusCode;

    /**
     * Black and white check result
     *
     * @var string
     */
    private $checkResult;

    /**
     * Product used for the check
     *
     * Only for CREDITREFORM
     *
     * @var string
     */
    private $productName;

    /**
     * Identification number of the customer/company
     *
     * Only for CREDITREFORM
     *
     * @var string
     */
    private $identificationNumber;

    /**
     * Negative indicator
     *
     * Only for CUBE
     *
     * @var boolean
     */
    private $negativeIndicator;

    /**
     * Status
     *
     * Only for CUBE
     *
     * @var string
     */
    private $status;

    /**
     * Special address
     *
     * Only for VERITA
     *
     * @var string
     */
    private $specialAddress;

    /**
     * @return string
     */
    public function getSolvencyInterface()
    {
        return $this->solvencyInterface;
    }

    /**
     * @param string $solvencyInterface
     *
     * @return SolvencyData
     */
    public function setSolvencyInterface($solvencyInterface)
    {
        $this->solvencyInterface = $solvencyInterface;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastRequestDate()
    {
        return $this->lastRequestDate;
    }

    /**
     * @param \DateTime $lastRequestDate
     *
     * @return SolvencyData
     */
    public function setLastRequestDate($lastRequestDate)
    {
        $this->lastRequestDate = $lastRequestDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCheckDate()
    {
        return $this->checkDate;
    }

    /**
     * @param \DateTime $checkDate
     *
     * @return SolvencyData
     */
    public function setCheckDate($checkDate)
    {
        $this->checkDate = $checkDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckType()
    {
        return $this->checkType;
    }

    /**
     * @param string $checkType
     *
     * @return SolvencyData
     */
    public function setCheckType($checkType)
    {
        $this->checkType = $checkType;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     *
     * @return SolvencyData
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    /**
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param array $actions
     *
     * @return SolvencyData
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return SolvencyData
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool
     */
    public function isThirdPartyRequested()
    {
        return $this->thirdPartyRequested;
    }

    /**
     * @param bool $thirdPartyRequested
     *
     * @return SolvencyData
     */
    public function setThirdPartyRequested($thirdPartyRequested)
    {
        $this->thirdPartyRequested = $thirdPartyRequested;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * @param string $orderID
     *
     * @return SolvencyData
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * @return float|int
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param float|int $score
     *
     * @return SolvencyData
     */
    public function setScore($score)
    {
        $this->score = $score;
        return $this;
    }

    /**
     * @return string
     */
    public function getTrafficLight()
    {
        return $this->trafficLight;
    }

    /**
     * @param string $trafficLight
     *
     * @return SolvencyData
     */
    public function setTrafficLight($trafficLight)
    {
        $this->trafficLight = $trafficLight;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * @param string $identification
     *
     * @return SolvencyData
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressValidation()
    {
        return $this->addressValidation;
    }

    /**
     * @param string $addressValidation
     *
     * @return SolvencyData
     */
    public function setAddressValidation($addressValidation)
    {
        $this->addressValidation = $addressValidation;
        return $this;
    }

    /**
     * @return string|Address
     */
    public function getCorrectedAddress()
    {
        return $this->correctedAddress;
    }

    /**
     * @param string|Address $correctedAddress
     *
     * @return SolvencyData
     */
    public function setCorrectedAddress($correctedAddress)
    {
        $this->correctedAddress = $correctedAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressOrigin()
    {
        return $this->addressOrigin;
    }

    /**
     * @param string $addressOrigin
     *
     * @return SolvencyData
     */
    public function setAddressOrigin($addressOrigin)
    {
        $this->addressOrigin = $addressOrigin;
        return $this;
    }

    /**
     * @return int
     */
    public function getAddressOriginCode()
    {
        return $this->addressOriginCode;
    }

    /**
     * @param int $addressOriginCode
     *
     * @return SolvencyData
     */
    public function setAddressOriginCode($addressOriginCode)
    {
        $this->addressOriginCode = $addressOriginCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getDecisionMessage()
    {
        return $this->decisionMessage;
    }

    /**
     * @param string $decisionMessage
     *
     * @return SolvencyData
     */
    public function setDecisionMessage($decisionMessage)
    {
        $this->decisionMessage = $decisionMessage;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param string $statusCode
     *
     * @return SolvencyData
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckResult()
    {
        return $this->checkResult;
    }

    /**
     * @param string $checkResult
     *
     * @return SolvencyData
     */
    public function setCheckResult($checkResult)
    {
        $this->checkResult = $checkResult;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     *
     * @return SolvencyData
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdentificationNumber()
    {
        return $this->identificationNumber;
    }

    /**
     * @param string $identificationNumber
     *
     * @return SolvencyData
     */
    public function setIdentificationNumber($identificationNumber)
    {
        $this->identificationNumber = $identificationNumber;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNegativeIndicator()
    {
        return $this->negativeIndicator;
    }

    /**
     * @param bool $negativeIndicator
     *
     * @return SolvencyData
     */
    public function setNegativeIndicator($negativeIndicator)
    {
        $this->negativeIndicator = $negativeIndicator;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return SolvencyData
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecialAddress()
    {
        return $this->specialAddress;
    }

    /**
     * @param string $specialAddress
     *
     * @return SolvencyData
     */
    public function setSpecialAddress($specialAddress)
    {
        $this->specialAddress = $specialAddress;
        return $this;
    }

    /**
     * Convert to array for validator
     *
     * @return array
     */
    public function toArray()
    {
        $data = array(
            'solvencyInterface' => $this->getSolvencyInterface(),
            'lastRequestDate' => $this->getLastRequestDate(),
            'checkDate' => $this->getCheckDate(),
            'checkType' => $this->getCheckType(),
            'actions' => $this->getActions(),
            'thirdPartyRequested' => $this->isThirdPartyRequested(),
        );

        if ($this->paymentMethod) {
            $data['paymentMethod'] = $this->getPaymentMethod();
        }

        if ($this->message) {
            $data['message'] = $this->getMessage();
        }

        if ($this->orderID) {
            $data['orderID'] = $this->getOrderID();
        }

        if ($this->score) {
            $data['score'] = $this->getScore();
        }

        if ($this->trafficLight) {
            $data['trafficLight'] = $this->getTrafficLight();
        }

        if ($this->identification) {
            $data['identification'] = $this->getIdentification();
        }

        if ($this->addressValidation) {
            $data['addressValidation'] = $this->getAddressValidation();
        }

        if ($this->correctedAddress) {
            $data['correctedAddress'] = $this->getCorrectedAddress();
        }

        if ($this->addressOrigin) {
            $data['addressOrigin'] = $this->getAddressOrigin();
        }

        if ($this->addressOriginCode) {
            $data['addressOriginCode'] = $this->getAddressOriginCode();
        }

        if ($this->decisionMessage) {
            $data['decisionMessage'] = $this->getDecisionMessage();
        }

        if ($this->statusCode) {
            $data['statusCode'] = $this->getStatusCode();
        }

        if ($this->productName) {
            $data['productName'] = $this->getProductName();
        }

        if ($this->identificationNumber) {
            $data['identificationNumber'] = $this->getIdentificationNumber();
        }

        if ($this->negativeIndicator) {
            $data['negativeIndicator'] = $this->isNegativeIndicator();
        }

        if ($this->status) {
            $data['status'] = $this->getStatus();
        }

        if ($this->specialAddress) {
            $data['specialAddress'] = $this->getSpecialAddress();
        }

        return $data;
    }

    /**
     * Validation meta data
     *
     * @see TBD
     * @return array
     */
    public function getValidationData()
    {
        $validationData = array();

        $validationData['solvencyInterface'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "SolvencyInterface is required"
        );

        $validationData['solvencyInterface'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateSolvencyInterface',
            'message' => "SolvencyInterface must have a valid value"
        );

        $validationData['lastRequestDate'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "LastRequestDate is required"
        );

        $validationData['checkDate'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "CheckDate is required"
        );

        $validationData['checkType'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "CheckType is required"
        );

        $validationData['checkType'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateCheckType',
            'message' => "CheckType must have a valid value"
        );

        $validationData['actions'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Actions is required"
        );

        $validationData['actions'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateActions',
            'message' => "Actions must have valid values"
        );

        $validationData['thirdPartyRequested'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "ThirdPartyRequested is required"
        );

        if ($this->checkType === $this::CHECK_TYPE_SECONDLEVEL) {
            $validationData['paymentMethod'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validatePaymentMethod',
                'message' => "PaymentMethod must have a valid value"
            );
        }

        if ($this->trafficLight) {
            $validationData['trafficLight'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateTrafficLight',
                'message' => "TrafficLight must have a valid value"
            );
        }

        if ($this->identification) {
            $validationData['identification'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateIdentification',
                'message' => "Identification must have a valid value"
            );
        }

        if ($this->addressValidation) {
            $validationData['addressValidation'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateAddressValidation',
                'message' => "AddressValidation must have a valid value"
            );
        }

        if ($this->checkResult) {
            $validationData['checkResult'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateCheckResult',
                'message' => "CheckResult must have a valid value"
            );
        }

        if ($this->productName) {
            $validationData['productName'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateProductName',
                'message' => "ProductName must have a valid value"
            );
        }

        if ($this->status) {
            $validationData['status'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateStatus',
                'message' => "Status must have a valid value"
            );
        }

        if ($this->specialAddress) {
            $validationData['specialAddress'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateSpecialAddress',
                'message' => "SpecialAddress must have a valid value"
            );
        }

        return $validationData;
    }

    /**
     * Validate the solvency interface type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateSolvencyInterface($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_SOLVENCY_INTERFACE);
    }

    /**
     * Validate the check type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateCheckType($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_CHECK_TYPE);
    }

    /**
     * Validate all actions in the array of actions
     *
     * @param array $value
     *
     * @return mixed
     */
    public static function validateActions($value)
    {
        foreach ($value as $action) {
            $resultForAll = Constants::validateConstant(__CLASS__, $action, static::TAG_ACTION);
            if (!$resultForAll) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validate the traffic light
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateTrafficLight($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_TRAFFIC_LIGHT);
    }

    /**
     * Validate the identification
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateIdentification($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_IDENTIFICATION);
    }

    /**
     * Validate the address validation value
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateAddressValidation($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_ADDRESS_VALIDATION);
    }

    /**
     * Validate the product name
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateProductName($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_PRODUCT_NAME);
    }

    /**
     * Validate the check result
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateCheckResult($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_CHECK_RESULT);
    }

    /**
     * Validate the status
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateStatus($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_STATUS);
    }

    /**
     * Validate the special address
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateSpecialAddress($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_SPECIAL_ADDRESS);
    }

    /**
     * Validate the payment method
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validatePaymentMethod($value)
    {
        return Methods::validate($value);
    }
}
