<?php

namespace Upg\Library\Tests\Request\Objects;

use Upg\Library\Request\Objects\ClearingFile as ClearingFile;
use Upg\Library\Validation\Validation;

class ClearingFileTest extends \PHPUnit\Framework\TestCase
{

    public function testClearingFileTestValidationSuccess()
    {
        $clearingFile = new ClearingFile();
        $clearingFile->setClearingID(100)->setFrom(new \DateTime("2017-01-01"))->setTo(new \DateTime("2017-02-01"));

        $validation = new Validation();
        $validation->getValidator($clearingFile);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testClearingFileTestValidationClearingIDFailure()
    {
        /** Test the required validation */
        $clearingFile = new ClearingFile();

        $validation = new Validation();
        $validation->getValidator($clearingFile);
        $data = $validation->performValidation();

        $expected = array(
            'Upg\\Library\\Request\\Objects\\ClearingFile' =>
            array(
                'clearingID' =>
                array(
                    0 => 'ClearingID is required',
                ),
                'from' =>
                array(
                    0 => 'Start date is required',
                ),
                'to' =>
                array(
                    0 => 'End date is required',
                ),
            ),
        );

        $this->assertEquals($expected, $data, 'Validation not triggered when clearingID was not set');

    }

    public function testClearingFileTestValidationFromFailure()
    {
        /** Test required validation */
        $clearingFile = new ClearingFile();
        $clearingFile->setClearingID(99);

        $validation = new Validation();
        $validation->getValidator($clearingFile);
        $data = $validation->performValidation();

        $expected = array(
            'Upg\\Library\\Request\\Objects\\ClearingFile' =>
            array(
                'from' =>
                array(
                    0 => 'Start date is required',
                ),
                'to' =>
                array(
                    0 => 'End date is required',
                ),
            ),
        );

        $this->assertEquals($expected, $data, 'Validation not triggered when start date was not set');

    }

    public function testClearingFileTestValidationToFailure()
    {
        /** Test required validation */
        $clearingFile = new ClearingFile();
        $clearingFile->setClearingID(99)
            ->setFrom(new \DateTime("2017-01-01"));

        $validation = new Validation();
        $validation->getValidator($clearingFile);
        $data = $validation->performValidation();

        $expected = array(
            'Upg\\Library\\Request\\Objects\\ClearingFile' =>
            array(
                'to' =>
                array(
                    0 => 'End date is required',
                ),
            ),
        );

        $this->assertEquals($expected, $data, 'Validation not triggered when end date was not set');

    }
}
