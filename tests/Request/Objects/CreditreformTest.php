<?php
namespace Upg\Library\Tests\Request\Objects;

use Upg\Library\Request\Objects\Creditreform;
use Faker\Factory as Factory;
use Upg\Library\Tests\Request\AbstractRequestTest;
use Upg\Library\Validation\Validation;

class CreditreformTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function setUp()
    {
        $faker = Factory::create();


        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown()
    {
        unset($this->faker);
    }

    public function testRequestDate()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setCrefoProductName(Creditreform::PRODUCT_IDENTIFICATION)
            ->setBlackWhiteResult(true)
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Creditreform',
            'requestDate',
            'RequestDate is required',
            $data,
            "RequestDate requirement did not trigger"
        );
    }

    public function testCrefoProductNameFailure()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new \DateTime())
            ->setBlackWhiteResult(true)
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Creditreform',
            'crefoProductName',
            'CrefoProductName is required',
            $data,
            "CrefoProductName requirement did not trigger"
        );

        $crefoResult->setCrefoProductName("BONIVERSUM");
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Creditreform',
            'crefoProductName',
            'CrefoProductName must have a valid value',
            $data,
            "CrefoProductName constant validation did not trigger"
        );
    }

    public function testBlackWhiteResultFailure()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new \DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_IDENTIFICATION)
            ->setBlackWhiteResult("TEST123")
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Creditreform',
            'blackWhiteResult',
            'BlackWhiteResult is not a valid value',
            $data,
            "BlackWhiteResult boolean check did not trigger"
        );
    }

    public function testTrafficLightResultFailure()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new \DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_IDENTIFICATION)
            ->setBlackWhiteResult(true);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Creditreform',
            'trafficLightResult',
            'TrafficLightResult is required',
            $data,
            "TrafficLightResult requirement did not trigger"
        );

        $crefoResult->setTrafficLightResult("BONIVERSUM");
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Creditreform',
            'trafficLightResult',
            'TrafficLightResult must have a valid value',
            $data,
            "TrafficLightResult constant validation did not trigger"
        );
    }

    public function testCompleteObject()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new \DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_IDENTIFICATION)
            ->setBlackWhiteResult(true)
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }
}
