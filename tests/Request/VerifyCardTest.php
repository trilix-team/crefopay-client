<?php
namespace Upg\Library\Tests\Request;

use Faker\Factory;
use Upg\Library\Config;
use Upg\Library\Request\VerifyCard;
use Upg\Library\Validation\Validation;

class VerifyCardTest extends AbstractRequestTest
{

    /**
     * Config object for tests
     *
     * @var Config
     */
    private $config;

    /**
     * A very long string
     *
     * @var string
     */
    private $veryLongString;

    /**
     * Faker instance
     *
     * @var \Faker\Generator
     */
    private $faker;

    public function setUp()
    {
        $faker = Factory::create();
        
        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
        
        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true
        ));
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    /**
     * Test successful validation of all properties
     */
    public function testVerifyCardValidationSuccess()
    {
        $request = new VerifyCard($this->config);
        $request->setCardNumber($this->faker->creditCardNumber)
            ->setValidity(new \DateTime("now"))
            ->setCvv(123)
            ->setContext(VerifyCard::CONTEXT_ONLINE);
        
        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();
        
        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    /**
     * Test cvv formatting validation (too long / wrong characters)
     */
    public function testVerifyCardValidationCVV()
    {
        $request = new VerifyCard($this->config);
        $request->setCardNumber($this->faker->creditCardNumber)
            ->setValidity(new \DateTime("now"))
            ->setContext(VerifyCard::CONTEXT_ONLINE)
            ->setCvv("1234567890");
        
        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();
        
        $this->assertValidationReturned('Upg\\Library\\Request\\VerifyCard', 'cvv', 'cvv has an invalid format', $data, "cvv validation failed to trigger");
       
        $request->setCvv("AAA");
        
        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();
        
        $this->assertValidationReturned('Upg\\Library\\Request\\VerifyCard', 'cvv', 'cvv has an invalid format', $data, "cvv validation failed to trigger");
    }

    /**
     * Test validity validation
     */
    public function testVerifyCardValidationValidity()
    {
        $request = new VerifyCard($this->config);
        $request->setCardNumber($this->faker->creditCardNumber)
            ->setCvv("123")
            ->setContext(VerifyCard::CONTEXT_ONLINE);
        
        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();
        
        $this->assertValidationReturned('Upg\\Library\\Request\\VerifyCard', 'validity', 'validity is required', $data, "validity validation failed to trigger");
    }

    /**
     * Test context requirement and value check
     */
    public function testVerifyCardValidationContext()
    {
        $request = new VerifyCard($this->config);
        $request->setCardNumber($this->faker->creditCardNumber)
            ->setValidity(new \DateTime("now"))
            ->setCvv("123");
        
        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();
        
        $this->assertValidationReturned('Upg\\Library\\Request\\VerifyCard', 'context', 'context is required', $data, "context required validation failed to trigger");
        
        $request->setContext('TERMINAL');
        
        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();
        
        $this->assertValidationReturned('Upg\\Library\\Request\\VerifyCard', 'context', 'context must one of 3 values', $data, "context value validation failed to trigger");
    }

    /**
     * Test card number requirement and length
     */
    public function testVerifyCardValidationNumber()
    {
        $request = new VerifyCard($this->config);
        $request->setValidity(new \DateTime("now"))
            ->setCvv("1234567890")
            ->setContext(VerifyCard::CONTEXT_ONLINE);
        
        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();
        
        $this->assertValidationReturned('Upg\\Library\\Request\\VerifyCard', 'cardNumber', 'cardNumber is required', $data, "cardNumber requirement validation failed to trigger");
        
        $request->setCardNumber($this->veryLongString);
        
        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();
        
        $this->assertValidationReturned('Upg\\Library\\Request\\VerifyCard', 'cardNumber', 'cardNumber must be between 1 and 30 characters', $data, "cardNumber length validation failed to trigger");
    }
}