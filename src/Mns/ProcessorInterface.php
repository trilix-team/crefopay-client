<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Mns;

/**
 * Interface ProcessorInterface
 *
 * For the integrator to use to process incoming MNS notices.
 * Please note for this script please do not do any processing beyond saving the MNS to the database.
 * We recommend that the order actions should be processed using a cron job and this interface should be used for the
 * implementation of the database saving code
 *
 * @package Upg\Library\Mns
 */
interface ProcessorInterface
{
    /**
     * Send data to the processor that will be used in the run method
     *
     * @param integer $merchantID        This is the merchantID assigned by CrefoPay.
     * @param string  $storeID           This is the store ID of a merchant assigned by CrefoPay as a merchant can have
     *                                   more than one store.
     * @param string  $orderID           This is the order number that the shop has assigned
     * @param string  $captureID         The confirmation ID of the capture. Only sent for Notifications that belong to
     *                                   captures
     * @param string  $merchantReference Reference that can be set by the merchant during the createTransaction call.
     * @param string  $paymentReference  The reference number of the
     * @param string  $userID            The unique user id of the customer.
     * @param string  $amount            This is either the amount of an incoming payment or “0” in case of some status
     *                                   changes
     * @param string  $currency          Currency code according to ISO4217.
     * @param string  $transactionStatus Current status of the transaction. Same values as resultCode
     * @param string  $orderStatus       Current status of the order. Possible values: PAID, PAYPENDING, PAYMENTFAILED,
     *                                   CHARGEBACK, CLEARED.
     * @param string  $additionalData    JSON string with additional data
     * @param string  $timestamp         Unix timestamp, Notification timestamp
     * @param string  $version           notification version (currently 1.5)
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/notification-call
     */
    public function sendData(
        $merchantID,
        $storeID,
        $orderID,
        $captureID,
        $merchantReference,
        $paymentReference,
        $userID,
        $amount,
        $currency,
        $transactionStatus,
        $orderStatus,
        $additionalData,
        $timestamp,
        $version
    );

    /**
     * The run method used by the processor to run successfully validated MNS notifications.
     *
     * This should not return anything
     */
    public function run();
}
