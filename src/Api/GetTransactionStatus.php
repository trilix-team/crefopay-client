<?php
/**
 * GetTransactionStatus class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class GetTransactionStatus
 * Api stub for getTransactionStatus
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/getstatus
 * @package Upg\Library\Api
 */
class GetTransactionStatus extends AbstractApi
{
    const UPDATE_TRANSACTION_STATUS_PATH = 'getTransactionStatus';

    /**
     * Constructor
     *
     * @param Config                                    $config
     * @param \Upg\Library\Request\GetTransactionStatus $request
     */
    public function __construct(Config $config, \Upg\Library\Request\GetTransactionStatus $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Returns API endpoint URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::UPDATE_TRANSACTION_STATUS_PATH);
    }
}
