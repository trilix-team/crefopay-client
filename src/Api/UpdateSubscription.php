<?php
/**
 * UpdateSubscription class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class UpdateSubscription
 * Api stub for updateSubscription
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/getstatus
 * @package Upg\Library\Api
 */
class UpdateSubscription extends AbstractApi
{
    const UPDATE_SUBSCRIPTION_STATUS_PATH = 'updateSubscription';

    /**
     * Constructor
     *
     * @param Config                                  $config
     * @param \Upg\Library\Request\UpdateSubscription $request
     */
    public function __construct(Config $config, \Upg\Library\Request\UpdateSubscription $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Returns API endpoint URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::UPDATE_SUBSCRIPTION_STATUS_PATH);
    }
}
