<?php
/**
 * ProcessorInterface interface file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Callback;

/**
 * Interface ProcessorInterface
 *
 * Processor interface which needs to be implemented in the integration for the handler
 * To call once request passes validation
 *
 * @package Upg\Library\Callback
 */
interface ProcessorInterface
{
    /**
     * Send data to the processor that will be used in the run method
     *
     * Unless specified most parameters will not be blank
     * Which may or may not be given depending on user flow and integration mode
     *
     * @param string  $notificationType          This is the notification type which can be PAYMENT_STATUS,
     *                                           PAYMENT_INSTRUMENT_SELECTION
     * @param integer $merchantID                This is the merchantID assigned by CrefoPay.
     * @param string  $storeID                   This is the store ID of a merchant assigned by CrefoPay as a merchant
     *                                           can have more than one store.
     * @param string  $orderID                   This is the order number of the shop.
     * @param string  $paymentMethod             This is the selected payment method
     * @param integer $resultCode                0 means OK, any other code means error
     * @param string  $merchantReference         Reference that was set by the merchant during the createTransaction
     *                                           call. Optional
     * @param string  $paymentInstrumentID       This is the payment instrument Id that was used
     * @param string  $paymentInstrumentsPageUrl This is the payment instruments page url.
     * @param array   $additionalInformation     Optional additional info in an associative array
     * @param string  $message                   Details about an error, otherwise not present. Optional
     */
    public function sendData(
        $notificationType,
        $merchantID,
        $storeID,
        $orderID,
        $paymentMethod,
        $resultCode,
        $merchantReference,
        $paymentInstrumentID,
        $paymentInstrumentsPageUrl,
        array $additionalInformation,
        $message
    );

    /**
     * The run method.
     *
     * This should return the appropriate url as shown in the manual under the Callback on the link provided for this
     * method.
     * The implementation must return either a successful url or an error url to CrefoPay.
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/callback
     * @return string The url as a raw string
     */
    public function run();
}
