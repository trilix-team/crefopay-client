<?php
namespace Upg\Library\Tests\Request\Objects;

use Upg\Library\Request\Objects\Bonima;
use Faker\Factory as Factory;
use Upg\Library\Tests\Request\AbstractRequestTest;
use Upg\Library\Validation\Validation;

class BonimaTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function setUp()
    {
        $faker = Factory::create();


        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown()
    {
        unset($this->faker);
    }

    /*public function testRequestDate()
    {
        $bonima = new Bonima();
        $bonima->setIdentification(Bonima::IDENTIFICATION_PERSON_HOUSEHOLD_IDENTIFIED)
            ->setAddressValidationStatus(Bonima::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setTrafficLightColor(Bonima::TRAFFIC_LIGHT_YELLOW)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'requestDate',
            'RequestDate is required',
            $data,
            "RequestDate requirement did not trigger"
        );
    }*/

    public function testIdentificationFailure()
    {
        $bonima = new Bonima();
        $bonima->setRequestDate(new \DateTime())
            ->setAddressValidationStatus(Bonima::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setTrafficLightColor(Bonima::TRAFFIC_LIGHT_YELLOW)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'identification',
            'Identification is required',
            $data,
            "Identification requirement did not trigger"
        );

        $bonima->setIdentification("BONIVERSUM");
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'identification',
            'Identification must have a valid value',
            $data,
            "Identification constant validation did not trigger"
        );
    }

    public function testAddressValidationStatusFailure()
    {
        $bonima = new Bonima();
        $bonima->setRequestDate(new \DateTime())
            ->setIdentification(Bonima::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setTrafficLightColor(Bonima::TRAFFIC_LIGHT_YELLOW)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'addressValidationStatus',
            'AddressValidationStatus is required',
            $data,
            "AddressValidationStatus requirement did not trigger"
        );

        $bonima->setAddressValidationStatus("BONIVERSUM");
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'addressValidationStatus',
            'AddressValidationStatus must have a valid value',
            $data,
            "AddressValidationStatus constant validation did not trigger"
        );
    }

    public function testTrafficLightColorFailure()
    {
        $bonima = new Bonima();
        $bonima->setRequestDate(new \DateTime())
            ->setAddressValidationStatus(Bonima::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setIdentification(Bonima::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'trafficLightColor',
            'TrafficLightColor is required',
            $data,
            "TrafficLightColor requirement did not trigger"
        );

        $bonima->setTrafficLightColor("BONIVERSUM");
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'trafficLightColor',
            'TrafficLightColor must have a valid value',
            $data,
            "TrafficLightColor constant validation did not trigger"
        );
    }

    public function testScoreFailure()
    {
        $bonima = new Bonima();
        $bonima->setRequestDate(new \DateTime())
            ->setAddressValidationStatus(Bonima::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setIdentification(Bonima::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setTrafficLightColor(Bonima::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'score',
            'Score is required',
            $data,
            "Score requirement did not trigger"
        );

        $bonima->setScore("BONIVERSUM");
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\Bonima',
            'score',
            'Score must be a float value',
            $data,
            "Score float check did not trigger"
        );
    }

    public function testCompleteObject()
    {
        $bonima = new Bonima();
        $bonima->setRequestDate(new \DateTime())
            ->setAddressValidationStatus(Bonima::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setIdentification(Bonima::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setTrafficLightColor(Bonima::TRAFFIC_LIGHT_YELLOW)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($bonima);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }
}
