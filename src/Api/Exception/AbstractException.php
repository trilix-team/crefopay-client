<?php
/**
 * AbstractException class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api\Exception;

use Upg\Library\Response\FailureResponse;

/**
 * Class AbstractException
 *
 * Abstract Exception for the API
 *
 * @package Upg\Library\Api\Exception
 */
abstract class AbstractException extends \Exception
{
    /**
     * Raw response string
     *
     * @var string
     */
    private $rawResponse;

    /**
     * Parsed response, usually a FailureResponse object
     *
     * @var \Upg\Library\Response\FailureResponse|string
     */
    private $parsedResponse;

    /**
     * HTTP Code that was received
     *
     * @var integer
     */
    private $httpCode;

    /**
     * Constructor
     *
     * @param string                                       $message
     * @param integer                                      $code
     * @param string                                       $rawResponse
     * @param \Upg\Library\Response\FailureResponse|string $parsedResponse
     * @param integer                                      $httpCode
     */
    public function __construct($message, $code = 0, $rawResponse = '', $parsedResponse = '', $httpCode = 0)
    {
        $this->rawResponse = $rawResponse;
        $this->parsedResponse = $parsedResponse;
        $this->httpCode = $httpCode;

        parent::__construct($message, $code);
    }

    /**
     * Returns the raw response
     *
     * @return string
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    /**
     * Returns the parsed response
     *
     * @return FailureResponse|string
     */
    public function getParsedResponse()
    {
        return $this->parsedResponse;
    }

    /**
     * Returns the received http code
     *
     * @return integer
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }
}
