<?php
/**
 * InvalidHttpResponseCode class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api\Exception;

/**
 * Class InvalidHttpResponseCode
 * Raised if an invalid http status code is received
 *
 * @package Upg\Library\Api\Exception
 */
class InvalidHttpResponseCode extends AbstractException
{
    /**
     * Constructor
     *
     * @param int    $httpCode
     * @param string $rawResponse
     */
    public function __construct($httpCode, $rawResponse)
    {
        parent::__construct(
            "Non expected http code: " . $httpCode,
            $httpCode,
            $rawResponse,
            null,
            $httpCode
        );
    }
}
