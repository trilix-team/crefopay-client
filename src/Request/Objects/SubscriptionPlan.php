<?php
/**
 * SubscriptionPlan class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Request\Objects;

use Upg\Library\Validation\Helper\Constants;

/**
 * Class SubscriptionPlan
 *
 * For subscription plan json objects
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/subscription-plan
 * @package Upg\Library\Request\Objects
 */
class SubscriptionPlan extends AbstractObject
{
    /**
     * Daily subscription plan
     */
    const INTERVAL_DAILY = "DAILY";

    /**
     * Weekly subscription plan
     */
    const INTERVAL_WEEKLY = "WEEKLY";

    /**
     * 4 weekly subscription plan
     */
    const INTERVAL_FOUR_WEEKLY = "FOUR_WEEKLY";

    /**
     * Monthly subscription plan
     */
    const INTERVAL_MONTHLY = "MONTHLY";

    /**
     * Quarterly subscription plan
     */
    const INTERVAL_QUARTERLY = "QUARTERLY";

    /**
     * Biannually subscription plan
     */
    const INTERVAL_BIANNUALLY = "BIANNUALLY";

    /**
     * Yearly subscription plan
     */
    const INTERVAL_YEARLY = "YEARLY";

    /**
     * Tag for the context validation
     */
    const TAG_INTERVAL = "INTERVAL";

    /**
     * The plans' reference
     *
     * @var string
     */
    private $planReference;

    /**
     * The plans' name
     *
     * @var string
     */
    private $name;

    /**
     * The plans' description
     *
     * @var string
     */
    private $description;

    /**
     * The plans' amount
     *
     * @var Amount
     */
    private $amount;

    /**
     * The plans' interval
     *
     * @see SubscriptionPlan::INTERVAL_DAILY
     * @see SubscriptionPlan::INTERVAL_WEEKLY
     * @see SubscriptionPlan::INTERVAL_FOUR_WEEKLY
     * @see SubscriptionPlan::INTERVAL_MONTHLY
     * @see SubscriptionPlan::INTERVAL_QUARTERLY
     * @see SubscriptionPlan::INTERVAL_BIANNUALLY
     * @see SubscriptionPlan::INTERVAL_YEARLY
     * @var string
     */
    private $interval;

    /**
     * The plans' trialPeriod
     *
     * @var integer
     */
    private $trialPeriod;

    /**
     * The limit of payments that will be done with this plan. If not set, it will be unlimited
     *
     * @var integer
     */
    private $basicPaymentsCount;

    /**
     * The plans' set contact details
     *
     * @var string
     */
    private $contactDetails;

    /**
     * If the plan has subscribers
     *
     * @var string
     */
    private $hasSubscribers;

    /**
     * Set the planReference field
     *
     * @param string $planReference
     *
     * @return SubscriptionPlan
     */
    public function setPlanReference($planReference)
    {
        $this->planReference = $planReference;
        return $this;
    }

    /**
     * Get the planReference field
     *
     * @return string
     */
    public function getPlanReference()
    {
        return $this->planReference;
    }

    /**
     * Set the name field
     *
     * @param string $name
     *
     * @return SubscriptionPlan
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get the name field
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the description field
     *
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get the description field
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the amount field
     *
     * @param Amount $amount
     *
     * @return $this
     */
    public function setAmount(Amount $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get the amount field
     *
     * @return Amount
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the interval field
     *
     * @param string $interval
     *
     * @return $this
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;
        return $this;
    }

    /**
     * Get the interval field
     *
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * Set the trial period field
     *
     * @param integer $trialPeriod
     *
     * @return $this
     */
    public function setTrialPeriod($trialPeriod)
    {
        $this->trialPeriod = $trialPeriod;
        return $this;
    }

    /**
     * Get the trial period field
     *
     * @return integer
     */
    public function getTrialPeriod()
    {
        return $this->trialPeriod;
    }

    /**
     * Set the trial period field
     *
     * @param integer $basicPaymentsCount
     *
     * @return $this
     */
    public function setBasicPaymentsCount($basicPaymentsCount)
    {
        $this->basicPaymentsCount = $basicPaymentsCount;
        return $this;
    }

    /**
     * Get the trial period field
     *
     * @return integer
     */
    public function getBasicPaymentsCount()
    {
        return $this->basicPaymentsCount;
    }

    /**
     * Set the contact details field
     *
     * @param integer $contactDetails
     *
     * @return $this
     */
    public function setContactDetails($contactDetails)
    {
        $this->contactDetails = $contactDetails;
        return $this;
    }

    /**
     * Get the contact details field
     *
     * @return integer
     */
    public function getContactDetails()
    {
        return $this->contactDetails;
    }

    /**
     * Set the hasSubscribers field
     *
     * @param boolean $hasSubscribers
     *
     * @return $this
     */
    public function setHasSubscribers($hasSubscribers)
    {
        $this->hasSubscribers = $hasSubscribers;
        return $this;
    }

    /**
     * Get the hasSubscribers field
     *
     * @return boolean
     */
    public function getHasSubscribers()
    {
        return $this->hasSubscribers;
    }

    /**
     * Generates a parameter array from this instances' set values
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'planReference' => $this->getPlanReference(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'amount' => $this->getAmount(),
            'interval' => $this->getInterval(),
            'hasSubscribers' => $this->getHasSubscribers(),
        );

        if ($this->trialPeriod) {
            $return['trialPeriod'] = $this->getTrialPeriod();
        }

        if ($this->basicPaymentsCount) {
            $return['basicPaymentsCount'] = $this->getBasicPaymentsCount();
        }

        if ($this->contactDetails) {
            $return['contactDetails'] = $this->getContactDetails();
        }

        return $return;
    }

    /**
     * Validation data
     *
     * @see https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/subscription-plan
     *
     * @return array
     */
    public function getValidationData()
    {
        $validationData = array();

        $validationData['planReference'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "PlanReference is required"
        );

        $validationData['planReference'][] = array(
            'name' => 'MaxLength',
            'value' => '15',
            'message' => "PlanReference must be no more than 15 characters long"
        );

        $validationData['name'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Name is required"
        );

        $validationData['name'][] = array(
            'name' => 'MaxLength',
            'value' => '25',
            'message' => "Name must be no more than 25 characters long"
        );

        $validationData['description'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Description is required"
        );

        $validationData['description'][] = array(
            'name' => 'MaxLength',
            'value' => '100',
            'message' => "Description must be no more than 100 characters long"
        );

        $validationData['amount'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Amount is required"
        );

        $validationData['interval'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Interval is required"
        );

        $validationData['interval'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateInterval',
            'message' => "Interval is not a valid value"
        );

        $validationData['trialPeriod'][] = array(
            'name' => 'Between',
            'value' => array('min' => 1, 'max' => 366),
            'message' => "TrialPeriod time is not in the allowed range"
        );

        if ($this->basicPaymentsCount) {
            $validationData['basicPaymentsCount'][] = array(
                'name' => 'Between',
                'value' => array('min' => 1, 'max' => 999),
                'message' => "BasicPaymentsCount value is not in the allowed range"
            );
        }

        $validationData['contactDetails'][] = array(
            'name' => 'MaxLength',
            'value' => '100',
            'message' => "ContactDetails must be no more than 100 characters long"
        );

        return $validationData;
    }

    /**
     * Validate the interval
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateInterval($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_INTERVAL);
    }
}
