<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request;

use Upg\Library\Request\Objects\Company;
use Upg\Library\Request\Objects\Person;
use Upg\Library\Request\Objects\Address;
use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Objects\BasketItem;

use Upg\Library\Request\Attributes\ObjectArray;
use Upg\Library\Validation\Helper\Constants;

/**
 * Class CreateSubscription
 *
 * This is the request class for any CreateSubscription request
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/createsubscription
 * @package Upg\Library\Request
 */
class CreateSubscription extends AbstractRequest
{
    /**
     * Value for the userType field for b2c transactions
     */
    const USER_TYPE_PRIVATE = "PRIVATE";

    /**
     * Value for the userType field for b2b transactions
     */
    const USER_TYPE_BUSINESS = "BUSINESS";

    /**
     * Tag for the userType validation
     */
    const TAG_USER_TYPE = "USER_TYPE";

    /**
     * A customizable reference to this plan
     *
     * @var string
     */
    private $planReference;

    /**
     * The trial period in which the plan costs nothing. (1-366)
     *
     * @var integer
     */
    private $trialPeriod;

    /**
     * This is the order number of the shop. This id is created by the shop and
     * is used as identifier for this transaction
     *
     * @var string
     */
    private $orderID;

    /**
     * This is the subscription id of the subscription. This id is created by the shop and
     * is used as identifier for the subscriptions
     *
     * @var string
     */
    private $subscriptionID;

    /**
     * The unique user id of the customer.
     *
     * @var string
     */
    private $userID;

    /**
     * Reference that can be set by the merchant. This parameter is sent back with
     * every call from CrefoPay to the merchant
     *
     * @var string
     */
    private $merchantReference;

    /**
     * This parameter is used to differentiate b2b and b2c customers
     *
     * @see CreateSubscription::USER_TYPE_PRIVATE
     * @see CreateSubscription::USER_TYPE_BUSINESS
     * @var string
     */
    private $userType;

    /**
     * Defines the risk assessment of a user from merchants perspective.
     *
     * Possible values are: [0,1,2].
     * 0 -> trusted user, 1 -> default risk user, 2 -> high risk user.
     * Either the useRiskClass or the basketItemRiskCass in the basket items has to be set.
     *
     * @see \Upg\Library\Risk\RiskClass
     * @var string
     */
    private $userRiskClass = null;

    /**
     * The IP address of the customer
     *
     * @var string
     */
    private $userIpAddress;

    /**
     * Contact data of the users company
     *
     * @see Company
     * @var Company
     */
    private $companyData;

    /**
     * Contact data of the user.
     *
     * The field “date of birth” is not mandatory.
     * It’s needed for solvency checks and the payment method “bill secure”.
     * An absent “date of birth” could cause less payment methods to be offered to the user.
     *
     * @var Person
     */
    private $userData;

    /**
     * Recipient for the bill
     *
     * @var string
     */
    private $billingRecipient;

    /**
     * Additional information like c/o.
     *
     * @deprecated 1.0.11 Deprecated by the API, use the field "additional" in the {@link $billingAddress}
     * @var string
     */
    private $billingRecipientAddition;

    /**
     * The customers billing address
     *
     * Only required if user was not registered before with this userID
     *
     * @var Address
     */
    private $billingAddress;

    /**
     * Recipient for shipping
     *
     * @var string
     */
    private $shippingRecipient;

    /**
     * Additional information like c/o or “PostNummer”.
     *
     * @deprecated 1.0.11 Deprecated by the API, use the field "additional" in the {@link $shippingAddress}
     * @var string
     */
    private $shippingRecipientAddition;

    /**
     * The customers shipping address
     *
     * @var Address
     */
    private $shippingAddress;

    /**
     * The amount of the basket
     *
     * @var Amount
     */
    private $amount;

    /**
     * A detailed list of all basket items
     *
     * @var ObjectArray
     */
    private $basketItems;

    /**
     * m -> minutes, h -> hours, d -> days; Example: one hour: 1h, ten minutes: 10m, two days: 2d
     *
     * @var string
     */
    private $basketValidity;

    /**
     * Locale determines the user’s communication language e.g. for e-mails which will be send to the user or for
     * payment pages.
     *
     * @see \Upg\Library\Locale\Codes
     * @var string
     */
    private $locale;

    /**
     * Set the plans' unique reference
     *
     * @see CreateSubscription::planReference
     *
     * @param string $planReference
     *
     * @return $this
     */
    public function setPlanReference($planReference)
    {
        $this->planReference = $planReference;
        return $this;
    }

    /**
     * Get the plans' unique reference
     *
     * @see CreateSubscription::planReference
     * @return string
     */
    public function getPlanReference()
    {
        return $this->planReference;
    }

    /**
     * Set the trialPeriod
     *
     * @see CreateSubscription::trialPeriod
     *
     * @param integer $trialPeriod
     *
     * @return $this
     */
    public function setTrialPeriod($trialPeriod)
    {
        $this->trialPeriod = $trialPeriod;
        return $this;
    }

    /**
     * Get the set trialPeriod
     *
     * @see CreateSubscription::trialPeriod
     * @return integer
     */
    public function getTrialPeriod()
    {
        return $this->trialPeriod;
    }

    /**
     * Set the Order ID
     *
     * @deprecated
     * @see CreateSubscription::orderID
     *
     * @param string $orderID
     *
     * @return $this
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * Get the set order ID
     *
     * @deprecated
     * @see CreateSubscription::orderID
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Set the Subscription ID
     *
     * @see CreateSubscription::subscriptionID
     *
     * @param string $subscriptionID
     *
     * @return $this
     */
    public function setSubscriptionID($subscriptionID)
    {
        $this->subscriptionID = $subscriptionID;
        return $this;
    }

    /**
     * Get the set order ID
     *
     * @see CreateSubscription::subscriptionID
     * @return string
     */
    public function getSubscriptionID()
    {
        return $this->subscriptionID;
    }


    /**
     * Set the user ID
     *
     * @see CreateSubscription::userID
     *
     * @param string $userID
     *
     * @return $this
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
        return $this;
    }

    /**
     * Get the user id
     *
     * @see CreateSubscription::userID
     * @return string
     */
    public function getUserId()
    {
        return $this->userID;
    }

    /**
     * Set the merchantReference
     *
     * @see CreateSubscription::merchantReference
     *
     * @param $merchantReference
     *
     * @return $this
     */
    public function setMerchantReference($merchantReference)
    {
        $this->merchantReference = $merchantReference;
        return $this;
    }

    /**
     * Get the merchantReference
     *
     * @see CreateSubscription::merchantReference
     * @return string
     */
    public function getMerchantReference()
    {
        return $this->merchantReference;
    }

    /**
     * Set the userType field
     *
     * @see CreateSubscription::userType
     *
     * @param $userType
     *
     * @return $this
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
        return $this;
    }

    /**
     * Get the userType field
     *
     * @see CreateSubscription::userType
     * @return string
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set the user risk class
     *
     * @see CreateSubscription::userRiskClass
     *
     * @param string $userRiskClass
     *
     * @return $this
     */
    public function setUserRiskClass($userRiskClass)
    {
        $this->userRiskClass = $userRiskClass;
        return $this;
    }

    /**
     * Get the user risk class
     *
     * @see CreateSubscription::userRiskClass
     * @return string
     */
    public function getUserRiskClass()
    {
        return $this->userRiskClass;
    }

    /**
     * Set the userIpAddress field
     *
     * @see CreateSubscription::userIpAddress
     *
     * @param $userIpAddress
     *
     * @return $this
     */
    public function setUserIpAddress($userIpAddress)
    {
        $this->userIpAddress = $userIpAddress;
        return $this;
    }

    /**
     * Get the userIpAddress field
     *
     * @see CreateSubscription::userIpAddress
     * @return string
     */
    public function getUserIpAddress()
    {
        return $this->userIpAddress;
    }

    /**
     * Set the companyData field
     *
     * @see CreateSubscription::companyData
     *
     * @param Company $company
     *
     * @return $this
     */
    public function setCompanyData(Company $company)
    {
        $this->companyData = $company;
        return $this;
    }

    /**
     * Get the companyData field
     *
     * @see CreateSubscription::companyData
     * @return Company
     */
    public function getCompanyData()
    {
        return $this->companyData;
    }

    /**
     * Set the userData field
     *
     * @see CreateSubscription::userData
     *
     * @param Person $userData
     *
     * @return $this
     */
    public function setUserData(Person $userData)
    {
        $this->userData = $userData;
        return $this;
    }

    /**
     * Get the userData field
     *
     * @see CreateSubscription::userData
     * @return Person
     */
    public function getUserData()
    {
        return $this->userData;
    }

    /**
     * Set the billingRecipient field
     *
     * @see CreateSubscription::billingRecipient
     *
     * @param $billingRecipient
     *
     * @return $this
     */
    public function setBillingRecipient($billingRecipient)
    {
        $this->billingRecipient = $billingRecipient;
        return $this;
    }

    /**
     * Get the billingRecipient field
     *
     * @see CreateSubscription::billingRecipient
     * @return string
     */
    public function getBillingRecipient()
    {
        return $this->billingRecipient;
    }

    /**
     * Set the billingRecipientAddition field
     *
     * @deprecated 1.0.11 Deprecated by the API, use the function "setAdditional" in the {@link $billingAddress}
     * @see        CreateSubscription::billingRecipientAddition
     *
     * @param $billingRecipientAddition
     *
     * @return $this
     */
    public function setBillingRecipientAddition($billingRecipientAddition)
    {
        $this->billingRecipientAddition = $billingRecipientAddition;
        return $this;
    }

    /**
     * Get the billingRecipientAddition field
     *
     * @deprecated 1.0.11 Deprecated by the API, use the function "getAdditional" in the {@link $billingAddress}
     * @see        CreateSubscription::billingRecipientAddition
     * @return string
     */
    public function getBillingRecipientAddition()
    {
        return $this->billingRecipientAddition;
    }

    /**
     * Set billingAddress field
     *
     * @see CreateSubscription::billingAddress
     *
     * @param Address $billingAddress
     *
     * @return $this
     */
    public function setBillingAddress(Address $billingAddress)
    {
        $this->billingAddress = $billingAddress;
        return $this;
    }

    /**
     * Get billingAddress field
     *
     * @see CreateSubscription::billingAddress
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set the shippingRecipient field
     *
     * @param string $shippingRecipient
     *
     * @see CreateSubscription::shippingRecipient
     * @return $this
     */
    public function setShippingRecipient($shippingRecipient)
    {
        $this->shippingRecipient = $shippingRecipient;
        return $this;
    }

    /**
     * Get the shippingRecipient field
     *
     * @see CreateSubscription::shippingRecipient
     * @return string
     */
    public function getShippingRecipient()
    {
        return $this->shippingRecipient;
    }

    /**
     * Get the shippingRecipientAddition field
     *
     * @deprecated 1.0.11 Deprecated by the API, use the function "setAdditional" in the {@link $shippingAddress}
     * @see        CreateSubscription::shippingRecipientAddition
     *
     * @param $shippingRecipientAddition
     *
     * @return $this
     */
    public function setShippingRecipientAddition($shippingRecipientAddition)
    {
        $this->shippingRecipientAddition = $shippingRecipientAddition;
        return $this;
    }

    /**
     * Get the shippingRecipientAddition field
     *
     * @deprecated 1.0.11 Deprecated by the API, use the function "getAdditional" in the {@link $shippingAddress}
     * @see        CreateSubscription::shippingRecipientAddition
     * @return string
     */
    public function getShippingRecipientAddition()
    {
        return $this->shippingRecipientAddition;
    }

    /**
     * Set the shippingAddress field
     *
     * @see CreateSubscription::shippingAddress
     *
     * @param Address $shippingAddress
     *
     * @return $this
     */
    public function setShippingAddress(Address $shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;
        return $this;
    }

    /**
     * Get the shippingAddress field
     *
     * @see CreateSubscription::shippingAddress
     * @return Address
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * Set the amount field
     *
     * @see CreateSubscription::amount
     *
     * @param Amount $amount
     *
     * @return $this
     */
    public function setAmount(Amount $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get the amount field
     *
     * @see CreateSubscription::amount
     * @return Amount
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Add Basket Item
     *
     * @param BasketItem $item
     *
     * @return $this
     */
    public function addBasketItem(BasketItem $item)
    {
        if (empty($this->basketItems)) {
            $this->basketItems = new ObjectArray();
        }

        $this->basketItems->append($item);
        return $this;
    }

    /**
     * Get the basket items which is an array object with BasketItems objects
     *
     * @see CreateSubscription::basketItems
     * @see Basket
     * @return ObjectArray
     */
    public function getBasketItems()
    {
        return $this->basketItems;
    }

    /**
     * Set the basketValidity field
     *
     * @see CreateSubscription::basketValidity
     *
     * @param $basketValidity
     *
     * @return $this
     */
    public function setBasketValidity($basketValidity)
    {
        $this->basketValidity = $basketValidity;
        return $this;
    }

    /**
     * Get the basketValidity field
     *
     * @see CreateSubscription::basketValidity
     * @return string
     */
    public function getBasketValidity()
    {
        return $this->basketValidity;
    }

    /**
     * Set the locale field
     *
     * @see CreateSubscription::locale
     *
     * @param $locale
     *
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * Get the locale field
     *
     * @see CreateSubscription::locale
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Getting data for serialization
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $data = array(
            'planReference' => $this->getPlanReference(),
            'subscriptionID' => $this->getSubscriptionID(),
            'userID' => $this->getUserId(),
            'userType' => $this->getUserType(),
            'amount' => $this->getAmount(),
            'basketItems' => $this->getBasketItems(),
            'locale' => $this->getLocale(),
        );

        if (!empty($this->trialPeriod)) {
            $data['trialPeriod'] = $this->getTrialPeriod();
        }

        if (!empty($this->merchantReference)) {
            $data['merchantReference'] = $this->getMerchantReference();
        }

        if ($this->userRiskClass !== null) {
            $data['userRiskClass'] = $this->getUserRiskClass();
        }

        if (!empty($this->userIpAddress)) {
            $data['userIpAddress'] = $this->getUserIpAddress();
        }

        if (!empty($this->companyData)) {
            $data['companyData'] = $this->getCompanyData();
        }

        if (!empty($this->userData)) {
            $data['userData'] = $this->getUserData();
        }

        if (!empty($this->billingRecipient)) {
            $data['billingRecipient'] = $this->getBillingRecipient();
        }

        if (!empty($this->billingRecipientAddition)) {
            $data['billingRecipientAddition'] = $this->getBillingRecipientAddition();
        }

        if (!empty($this->billingAddress)) {
            $data['billingAddress'] = $this->getBillingAddress();
        }

        if (!empty($this->shippingRecipient)) {
            $data['shippingRecipient'] = $this->getShippingRecipient();
        }

        if (!empty($this->shippingRecipientAddition)) {
            $data['shippingRecipientAddition'] = $this->getShippingRecipientAddition();
        }

        if (!empty($this->shippingAddress)) {
            $data['shippingAddress'] = $this->getShippingAddress();
        }

        if (!empty($this->basketValidity)) {
            $data['basketValidity'] = $this->getBasketValidity();
        }

        return $data;
    }

    /**
     * Validation meta data
     *
     * @return array
     */
    public function getClassValidationData()
    {
        $validationData = array();

        $validationData['planReference'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "planReference is required"
        );

        $validationData['planReference'][] = array(
            'name' => 'MaxLength',
            'value' => '15',
            'message' => "planReference must be between 1 and 15 characters"
        );

        $validationData['trialPeriod'][] = array(
            'name' => 'Between',
            'value' => array('min' => 1, 'max' => 366),
            'message' => "trialPeriod is not in the allowed range"
        );

        $validationData['subscriptionID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "subscriptionID is required"
        );

        $validationData['subscriptionID'][] = array(
            'name' => 'MaxLength',
            'value' => '30',
            'message' => "subscriptionID must be between 1 and 30 characters"
        );

        $validationData['userID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "userID is required"
        );

        $validationData['userID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "userID must be between 1 and 50 characters"
        );

        $validationData['merchantReference'][] = array(
            'name' => 'MaxLength',
            'value' => '30',
            'message' => "merchantReference must be between 1 and 30 characters"
        );

        $validationData['userType'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "userType must be certain values"
        );

        $validationData['userType'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateUserType',
            'message' => "userType must be certain values"
        );

        $validationData['userRiskClass'][] = array(
            'name' => 'Callback',
            'value' => 'Upg\Library\Risk\RiskClass::validateRiskClass',
            'message' => "userRiskClass must contain certain values or be empty"
        );

        $validationData['userIpAddress'][] = array(
            'name' => 'MaxLength',
            'value' => '15',
            'message' => "userIpAddress must be between 1 and 15 characters"
        );

        $validationData['billingRecipient'][] = array(
            'name' => 'MaxLength',
            'value' => '80',
            'message' => "billingRecipient must be between 1 and 80 characters"
        );

        $validationData['billingRecipientAddition'][] = array(
            'name' => 'MaxLength',
            'value' => '80',
            'message' => "billingRecipientAddition must be between 1 and 80 characters"
        );

        $validationData['shippingRecipient'][] = array(
            'name' => 'MaxLength',
            'value' => '80',
            'message' => "shippingRecipient must be between 1 and 80 characters"
        );

        $validationData['shippingRecipientAddition'][] = array(
            'name' => 'MaxLength',
            'value' => '80',
            'message' => "shippingRecipientAddition must be between 1 and 80 characters"
        );

        $validationData['amount'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "amount must be set for the transaction"
        );

        $validationData['basketItems'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "basketItems must be added to the transaction"
        );

        $validationData['locale'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "locale must be set for the transaction"
        );

        $validationData['locale'][] = array(
            'name' => 'Callback',
            'value' => 'Upg\Library\Locale\Codes::validateLocale',
            'message' => "locale must be certain values"
        );

        return $validationData;
    }

    /**
     * Validate the user type
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validateUserType($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_USER_TYPE);
    }
}
