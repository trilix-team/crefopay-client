<?php

namespace Upg\Library\Tests\Endtoend\Integration;

use Faker\Factory;
use Upg\Library\Api\Exception\ApiError;
use Upg\Library\Config;
use Upg\Library\Api\VerifyCard as VerifyCardApi;
use Upg\Library\Request\VerifyCard as VerifyCardRequest;

/**
 * @group Endtoend
 */
class VerifyCardTest extends \PHPUnit\Framework\TestCase
{
    private $faker;

    /**
     * Config object for tests
     *
     * @var Config
     */
    private $config;

    public function setUp()
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->faker = $faker;

        $merchantPassword = trim(getenv('CREFOPAY_TEST_MERCHANT_PASSWORD'));
        $merchantID = trim(getenv('CREFOPAY_TEST_MERCHANT_ID'));
        $storeID = trim(getenv('CREFOPAY_TEST_STORE_ID'));
        $baseURL = trim(getenv('CREFOPAY_TEST_BASE_URL'));
        $logEnabled = false;

        if (!empty($merchantPassword) && !empty($merchantID) && !empty($storeID) && !empty($baseURL)) {
            $this->config = new Config(array(
                'merchantPassword' => $merchantPassword,
                'merchantID' => $merchantID,
                'storeID' => $storeID,
                'logEnabled' => $logEnabled,
                'sendRequestsWithSalt' => true,
                'baseUrl' => $baseURL
            ));
        } else {
            $this->config = null;
        }
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    /**
     * Test successful sending of the request
     * Depending on the configured API-Data this call might produce different results.
     */
    public function testSuccessfulApiCall()
    {
        $request = new VerifyCardRequest($this->config);
        $request->setCardNumber($this->faker->creditCardNumber)
            ->setValidity(new \DateTime('2020-01-01'))
            ->setContext(VerifyCardRequest::CONTEXT_ONLINE)
            ->setCvv('123');

        $api = new VerifyCardApi($this->config, $request);
        try {
            $result = $api->sendRequest();
            $this->assertEquals(0, $result->getData('resultCode'));
            $this->assertEmpty($result->getData('message'));
        } catch (ApiError $e) {
            $result = $e->getParsedResponse();
            $this->assertEquals(1000, $result->getData('resultCode'));
            $this->assertEquals('Internal error.', $result->getData('message'));
        }

        $this->assertNotEmpty($result->getData('salt'));
    }
}