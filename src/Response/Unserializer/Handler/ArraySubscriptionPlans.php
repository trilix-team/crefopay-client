<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\Amount as PlanAmount;
use Upg\Library\Request\Objects\SubscriptionPlan;
use Upg\Library\Response\Unserializer\Processor;

/**
 * Class ArrayClearingFiles
 *
 * Unserializer for clearingFiles data
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/company
 * @package Upg\Library\Response\Unserializer\Handler
 */
class ArraySubscriptionPlans implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'subscriptionPlans',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return array
     */
    public function unserializeProperty(Processor $processor, $value)
    {
        $data = array();

        foreach ($value as $subscriptionPlanData) {
            $subscriptionPlan = new SubscriptionPlan();
            $subscriptionPlan->setUnserializedData($subscriptionPlanData);

            $amount = new PlanAmount();
            $amount->setUnserializedData($subscriptionPlanData['amount']);
            $subscriptionPlan->setAmount($amount);

            $data[] = $subscriptionPlan;
        }

        return $data;
    }
}
