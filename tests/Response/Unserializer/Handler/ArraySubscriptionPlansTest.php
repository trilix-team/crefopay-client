<?php

namespace Upg\Library\Tests\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Objects\SubscriptionPlan;
use Upg\Library\Response\Unserializer\Handler\ArraySubscriptionPlans;
use Upg\Library\Response\Unserializer\Processor;

class ArraySubscriptionPlansTest extends \PHPUnit\Framework\TestCase
{

    protected $testCases = 2;

    /**
     * Test if array of SubscriptionPlan is returned
     */
    public function testSerialization()
    {
        $path = realpath(dirname(__FILE__));

        $json = file_get_contents("$path/ArraySubscriptionPlansTest.json");

        $value = json_decode($json, true);

        $arraySubscriptionPlans = new ArraySubscriptionPlans();

        $data = $arraySubscriptionPlans->unserializeProperty(new Processor(), $value);

        $this->assertEquals(count($data), $this->testCases, "array does not contain two elements");

        $objValidationRan = 0;

        foreach ($data as $plan) {
            /**
             * @var SubscriptionPlan $plan
             */
            if ($objValidationRan === 0) {
                $this->assertEquals("D3MO", $plan->getPlanReference());
                $this->assertEquals("Demoabo", $plan->getName());
                $this->assertEquals("Ein Testabonnement", $plan->getDescription());
                $this->assertEquals(new Amount(1000), $plan->getAmount());
                $this->assertEquals(SubscriptionPlan::INTERVAL_MONTHLY, $plan->getInterval());
                $this->assertEquals(30, $plan->getTrialPeriod());
                $this->assertEquals(48, $plan->getBasicPaymentsCount());
                $this->assertEquals("Feurigstr. 59, 12103 Berlin, Deutschland", $plan->getContactDetails());
                $this->assertEquals(true, $plan->getHasSubscribers());
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 1) {
                $this->assertEquals("ORIGIN", $plan->getPlanReference());
                $this->assertEquals("Origin - Abo", $plan->getName());
                $this->assertEquals("Ein Origin Abonnement", $plan->getDescription());
                $this->assertEquals(new Amount(1500), $plan->getAmount());
                $this->assertEquals(SubscriptionPlan::INTERVAL_MONTHLY, $plan->getInterval());
                $this->assertEquals(12, $plan->getBasicPaymentsCount());
                $this->assertEquals("Feurigstr. 59, 12103 Berlin, Deutschland", $plan->getContactDetails());
                $this->assertEquals(true, $plan->getHasSubscribers());
                $objValidationRan++;
                break;
            }

        }

        $this->assertEquals($this->testCases, $objValidationRan, "Not all object validation has ran in this test");

    }
}
