<?php

namespace Upg\Library\Tests\Response\Unserializer\Handler;

use Upg\Library\Response\Unserializer\Handler\RiskData as RiskDataUnserializer;
use Upg\Library\Response\Unserializer\Handler\RiskData;
use Upg\Library\Response\Unserializer\Processor;

class RiskDataTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test if array of PaymentInstrument is returned
     */
    public function testSerialization()
    {
        $jsonString = '{"solvencyData": [{"checktype":"BONIMA"}]}';

        $jsonObj = json_decode($jsonString, true);

        $riskDataProcessor = new RiskDataUnserializer();

        $riskData = $riskDataProcessor->unserializeProperty(new Processor(), $jsonObj);

        $expectedArray = array(
            "solvencyData" => array(
                array(
                    "checktype" => "BONIMA",
                ),
            ),
        );

        /**
         * @var string $riskData
         */
        $this->assertEquals($expectedArray, $riskData);

    }
}
