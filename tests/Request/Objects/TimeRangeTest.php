<?php

namespace Upg\Library\Tests\Request\Objects;

use Upg\Library\Request\Objects\TimeRange as TimeRange;

use Upg\Library\Tests\Request\AbstractRequestTest;
use Upg\Library\Validation\Validation;

class TimeRangeTest extends AbstractRequestTest
{

    public function testTimeRangeTestValidationSuccess()
    {
        $timeRange = new TimeRange();
        $timeRange->setMinimum(1)
            ->setMaximum(30);

        $validation = new Validation();
        $validation->getValidator($timeRange);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testTimeRangeTestValidationMinimumFailure()
    {
        $timeRange = new TimeRange();

        $validation = new Validation();

        /** test the invalid timeRange logic */
        $timeRange->setMaximum(30);
        $validation->getValidator($timeRange);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\TimeRange',
            'minimum',
            'Minimum time is required',
            $data,
            "Minimum requirement was not validated"
        );

        $timeRange->setMinimum(1000);
        $validation->getValidator($timeRange);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\TimeRange',
            'minimum',
            'Minimum time is not in the allowed range',
            $data,
            "Minimum time range was not validated"
        );
    }

    public function testTimeRangeTestValidationMaximumFailure()
    {
        $timeRange = new TimeRange();

        $validation = new Validation();

        /** test the invalid timeRange logic */
        $timeRange->setMinimum(1);
        $validation->getValidator($timeRange);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\TimeRange',
            'maximum',
            'Maximum time is required',
            $data,
            "Maximum requirement was not validated"
        );

        $timeRange->setMaximum(1000);
        $validation->getValidator($timeRange);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\TimeRange',
            'maximum',
            'Maximum time is not in the allowed range',
            $data,
            "Maximum time range was not validated"
        );
    }
}
