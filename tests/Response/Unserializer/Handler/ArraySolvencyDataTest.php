<?php

namespace Upg\Library\Tests\Response\Unserializer\Handler;

use Upg\Library\PaymentMethods\Methods;
use Upg\Library\Request\Objects\Address;
use Upg\Library\Request\Objects\SolvencyData;
use Upg\Library\Response\Unserializer\Handler\ArraySolvencyData;
use Upg\Library\Response\Unserializer\Processor;

class ArraySolvencyDataTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test if array of PaymentInstrument is returned
     */
    public function testSerialization()
    {
        $path = realpath(dirname(__FILE__));

        $json = file_get_contents("$path/ArraySolvencyDataTest.json");

        $value = json_decode($json, true);

        $arraySolvencyData = new ArraySolvencyData();

        $data = $arraySolvencyData->unserializeProperty(new Processor(), $value);

        $elementCount = count($data);

        $this->assertEquals(7, $elementCount, "Array does not contain seven elements");

        $objValidationRan = 0;
        date_default_timezone_set('Etc/UTC');

        foreach ($data as $solvencyCheck) {
            /**
             * @var SolvencyData $solvencyCheck
             */
            $this->assertEquals($solvencyCheck->getLastRequestDate(),
                \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', "2018-08-01T00:00:00Z"));
            $this->assertEquals($solvencyCheck->getCheckDate(),
                \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', "2018-08-08T00:00:00Z"));
            if ($objValidationRan === 0) {
                $this->assertEquals($solvencyCheck->getSolvencyInterface(), SolvencyData::CHECK_INTERFACE_BONIMA);
                $this->assertEquals($solvencyCheck->getCheckType(), SolvencyData::CHECK_TYPE_SECONDLEVEL);
                $this->assertEquals($solvencyCheck->getPaymentMethod(), Methods::PAYMENT_METHOD_TYPE_BILL);
                $this->assertEquals($solvencyCheck->getActions(),
                    array(SolvencyData::CHECK_ACTION_HIGH, SolvencyData::CHECK_ACTION_RESTRICTION));
                $this->assertEquals($solvencyCheck->isThirdPartyRequested(), true);
                $this->assertEquals($solvencyCheck->getScore(), 5.45);
                $this->assertEquals($solvencyCheck->getTrafficLight(), SolvencyData::CHECK_TRAFFIC_LIGHT_YELLOW);
                $this->assertEquals($solvencyCheck->getIdentification(),
                    SolvencyData::CHECK_IDENTIFICATION_BUILDING_IDENTIFIED);
                $this->assertEquals($solvencyCheck->getAddressValidation(),
                    SolvencyData::CHECK_ADDRESS_VALIDATION_CORRECTED_AND_VALIDATED);
                $this->assertEquals($solvencyCheck->getCorrectedAddress(), $this->getAddress());
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 1) {
                $this->assertEquals($solvencyCheck->getSolvencyInterface(), SolvencyData::CHECK_INTERFACE_BUERGEL);
                $this->assertEquals($solvencyCheck->getActions(), array(SolvencyData::CHECK_ACTION_HIGH));
                $this->assertEquals($solvencyCheck->isThirdPartyRequested(), true);
                $this->assertEquals($solvencyCheck->getScore(), 25.45);
                $this->assertEquals($solvencyCheck->getAddressOrigin(), "What");
                $this->assertEquals($solvencyCheck->getAddressOriginCode(), "is");
                $this->assertEquals($solvencyCheck->getDecisionMessage(), "buergel?");
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 2) {
                $this->assertEquals($solvencyCheck->getSolvencyInterface(), SolvencyData::CHECK_INTERFACE_CREDICONNECT);
                $this->assertEquals($solvencyCheck->getCheckType(), SolvencyData::CHECK_TYPE_SECONDLEVEL);
                $this->assertEquals($solvencyCheck->getPaymentMethod(), Methods::PAYMENT_METHOD_TYPE_DD);
                $this->assertEquals($solvencyCheck->getActions(), array(SolvencyData::CHECK_ACTION_DECLINED));
                $this->assertEquals($solvencyCheck->isThirdPartyRequested(), false);
                $this->assertEquals($solvencyCheck->getOrderID(), "169");
                $this->assertEquals($solvencyCheck->getTrafficLight(), SolvencyData::CHECK_TRAFFIC_LIGHT_RED);
                $this->assertEquals($solvencyCheck->getStatusCode(), "ALIVE");
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 3) {
                $this->assertEquals($solvencyCheck->getSolvencyInterface(), SolvencyData::CHECK_INTERFACE_CREDITREFORM);
                $this->assertEquals($solvencyCheck->getCheckType(), SolvencyData::CHECK_TYPE_SECONDLEVEL);
                $this->assertEquals($solvencyCheck->getPaymentMethod(), Methods::PAYMENT_METHOD_TYPE_BILL);
                $this->assertEquals($solvencyCheck->getActions(), array(SolvencyData::CHECK_ACTION_NONE));
                $this->assertEquals($solvencyCheck->isThirdPartyRequested(), true);
                $this->assertEquals($solvencyCheck->getTrafficLight(), SolvencyData::CHECK_TRAFFIC_LIGHT_GREEN);
                $this->assertEquals($solvencyCheck->getCheckResult(), SolvencyData::CHECK_RESULT_BLACK);
                $this->assertEquals($solvencyCheck->getProductName(),
                    SolvencyData::CHECK_PRODUCT_NAME_TRAFFIC_LIGHT_REPORT);
                $this->assertEquals($solvencyCheck->getIdentificationNumber(), "CREFO12345678");
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 4) {
                $this->assertEquals($solvencyCheck->getSolvencyInterface(), SolvencyData::CHECK_INTERFACE_CUBE);
                $this->assertEquals($solvencyCheck->getActions(), array(SolvencyData::CHECK_ACTION_HIGH));
                $this->assertEquals($solvencyCheck->isThirdPartyRequested(), true);
                $this->assertEquals($solvencyCheck->getScore(), 200);
                $this->assertEquals($solvencyCheck->isNegativeIndicator(), false);
                $this->assertEquals($solvencyCheck->getStatus(), SolvencyData::CHECK_STATUS_DEAD);
                $this->assertEquals($solvencyCheck->getCorrectedAddress(), $this->getAddress());
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 5) {
                $this->assertEquals($solvencyCheck->getSolvencyInterface(), SolvencyData::CHECK_INTERFACE_FLEXCONNECT);
                $this->assertEquals($solvencyCheck->getCheckType(), SolvencyData::CHECK_TYPE_SECONDLEVEL);
                $this->assertEquals($solvencyCheck->getPaymentMethod(), Methods::PAYMENT_METHOD_TYPE_BILL);
                $this->assertEquals($solvencyCheck->getActions(), array(SolvencyData::CHECK_ACTION_HIGH));
                $this->assertEquals($solvencyCheck->isThirdPartyRequested(), true);
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 6) {
                $this->assertEquals($solvencyCheck->getSolvencyInterface(), SolvencyData::CHECK_INTERFACE_VERITA);
                $this->assertEquals($solvencyCheck->getCheckType(), SolvencyData::CHECK_TYPE_SECONDLEVEL);
                $this->assertEquals($solvencyCheck->getPaymentMethod(), Methods::PAYMENT_METHOD_TYPE_DD);
                $this->assertEquals($solvencyCheck->getActions(), array(SolvencyData::CHECK_ACTION_MANUAL_CHECK));
                $this->assertEquals($solvencyCheck->isThirdPartyRequested(), true);
                $this->assertEquals($solvencyCheck->getScore(), 2545);
                $this->assertEquals($solvencyCheck->getTrafficLight(), SolvencyData::CHECK_TRAFFIC_LIGHT_RED);
                $this->assertEquals($solvencyCheck->getIdentification(),
                    SolvencyData::CHECK_IDENTIFICATION_BUILDING_IDENTIFIED);
                $this->assertEquals($solvencyCheck->getAddressValidation(),
                    SolvencyData::CHECK_ADDRESS_VALIDATION_CORRECTED_AND_VALIDATED);
                $this->assertEquals($solvencyCheck->getCorrectedAddress(), $this->getAddress());
                $objValidationRan++;
                break;
            }
        }
        date_default_timezone_set('Europe/Berlin');

        $this->assertEquals($elementCount, $objValidationRan, "Not all object validations have run in this test");
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Teststrasse")
            ->setNo("1")
            ->setZip("10000")
            ->setCity("Berlin")
            ->setCountry("DE");
        return $address;
    }
}
