<?php
/**
 * Class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Request;

use Upg\Library\Validation\Helper\Constants;

/**
 * Class UpdateSubscription
 *
 * This is the request class for any updateSubscription request object
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/updatesubscription
 * @package Upg\Library\Request
 */
class UpdateSubscription extends AbstractRequest
{
    const ACTION_CHANGE_RATE = "CHANGE_RATE";
    const ACTION_CANCEL = "CANCEL";

    /**
     * Tag for the context validation
     */
    const TAG_ACTION = "ACTION";

    /**
     * This is the order number of the shop.
     *
     * This id is created by the shop and is used as identifier for this transaction
     *
     * @var string
     */
    private $subscriptionId;

    /**
     * Action to be performed by the call. Options: CHANGE_RATE, CANCEL
     *
     * @var string
     */
    private $action;

    /**
     * Amount to which the subscription rate should be updated to. Only required if the action is CHANGE_RATE.
     *
     * @var Integer
     */
    private $rate;

    /**
     * Set the subscriptionId
     *
     * @see UpdateSubscription::subscriptionId
     *
     * @param string $subscriptionId
     *
     * @return $this
     */
    public function setSubscriptionID($subscriptionId)
    {
        $this->subscriptionId = $subscriptionId;
        return $this;
    }

    /**
     * Get the set subscriptionId
     *
     * @see UpdateSubscription::subscriptionId
     * @return string
     */
    public function getSubscriptionID()
    {
        return $this->subscriptionId;
    }

    /**
     * Set the action
     *
     * @see UpdateSubscription::ACTION_CHANGE_RATE
     * @see UpdateSubscription::ACTION_CANCEL
     *
     * @param string $action
     *
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * Get the set action
     *
     * @see UpdateSubscription::ACTION_CHANGE_RATE
     * @see UpdateSubscription::ACTION_CANCEL
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set the rate
     *
     * @see UpdateSubscription::rate
     *
     * @param Integer $rate
     *
     * @return $this
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * Get the set rate
     *
     * @see UpdateSubscription::rate
     *
     * @return Integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $return = array(
            'subscriptionId' => $this->getSubscriptionID(),
            'action' => $this->getAction(),
        );

        if ($this->action === self::ACTION_CHANGE_RATE) {
            $return['rate'] = $this->getRate();
        }

        return $return;
    }

    /**
     * Get the validation
     *
     * @return array
     */
    public function getClassValidationData()
    {
        $validationData = array();

        $validationData['subscriptionId'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "subscriptionId is required",
        );

        $validationData['subscriptionId'][] = array(
            'name' => 'MaxLength',
            'value' => '30',
            'message' => "subscriptionId must be between 1 and 30 characters",
        );

        $validationData['action'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "action is required",
        );

        $validationData['action'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateAction',
            'message' => "action must be certain values",
        );

        if ($this->action === self::ACTION_CHANGE_RATE) {
            $validationData['rate'][] = array(
                'name' => 'required',
                'value' => null,
                'message' => "rate is required",
            );
            $validationData['rate'][] = array(
                'name' => 'Regex',
                'value' => '/^\d{0,9}$/',
                'message' => "rate must be between 1 and 9 digits"
            );
        }

        return $validationData;
    }

    /**
     * Validate the action
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateAction($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_ACTION);
    }
}
