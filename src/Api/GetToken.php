<?php
/**
 * GetToken class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class GetToken
 *
 * Api stub for GetToken
 *
 * @deprecated 1.0.11 Will be removed in version 1.0.15
 * @link       https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/paycobridge-js
 * @package    Upg\Library\Api
 */
class GetToken extends AbstractApi
{
    const GET_TOKEN_PATH = 'getToken';

    /**
     * Constructor
     *
     * @param Config                        $config
     * @param \Upg\Library\Request\GetToken $request
     */
    public function __construct(Config $config, \Upg\Library\Request\GetToken $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Returns API endpoint URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::GET_TOKEN_PATH);
    }
}
