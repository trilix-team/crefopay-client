<?php
/**
 * VerifyCard class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class VerifyCard
 *
 * Api stub for verifyCard
 *
 * @link    https://documentation.upgplc.com/hostedpagesdraft/en/topic/verifycard
 * @package Upg\Library\Api
 */
class VerifyCard extends AbstractApi
{
    const VERIFY_CARD_PATH = 'verifyCard';

    /**
     * Constructor
     *
     * @param Config                          $config
     * @param \Upg\Library\Request\VerifyCard $request
     */
    public function __construct(Config $config, \Upg\Library\Request\VerifyCard $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Returns API endpoint URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::VERIFY_CARD_PATH);
    }
}
