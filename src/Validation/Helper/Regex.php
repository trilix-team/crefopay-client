<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Validation\Helper;

/**
 * Class Regex
 *
 * @link
 * @package Upg\Library\Validation\Helper
 */

class Regex
{
    const REGEX_FULL_ALPHANUMERIC = '/[\p{Cyrillic}\p{Latin}\p{Greek}\s0-9]*/';
    const REGEX_FULL_ALPHA = '/[\p{Cyrillic}\p{Latin}\p{Greek}\s]*/';

    const REGEX_PARTIAL_ALPHANUMERIC = '[\p{Cyrillic}\p{Latin}\p{Greek}\s0-9]';
    const REGEX_PARTIAL_ALPHA = '[\p{Cyrillic}\p{Latin}\p{Greek}\s]';
}
