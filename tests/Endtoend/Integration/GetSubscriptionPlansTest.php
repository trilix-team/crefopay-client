<?php

namespace Upg\Library\Tests\Endtoend\Integration;

use Faker\Factory;
use Upg\Library\Config;
use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Objects\AmountRange;
use Upg\Library\Request\GetSubscriptionPlans as GetSubscriptionPlansRequest;
use Upg\Library\Api\GetSubscriptionPlans as GetSubscriptionPlansApi;

/**
 * @group Endtoend
 */
class GetSubscriptionPlansTest extends \PHPUnit\Framework\TestCase
{
    private $faker;

    /**
     * Config object for tests
     *
     * @var Config
     */
    private $config;

    public function setUp()
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->faker = $faker;

        $merchantPassword = trim(getenv('CREFOPAY_TEST_MERCHANT_PASSWORD'));
        $merchantID = trim(getenv('CREFOPAY_TEST_MERCHANT_ID'));
        $storeID = trim(getenv('CREFOPAY_TEST_STORE_ID'));
        $baseURL = trim(getenv('CREFOPAY_TEST_BASE_URL'));
        $logEnabled = false;

        if (!empty($merchantPassword) && !empty($merchantID) && !empty($storeID) && !empty($baseURL)) {
            $this->config = new Config(array(
                'merchantPassword' => $merchantPassword,
                'merchantID' => $merchantID,
                'storeID' => $storeID,
                'logEnabled' => $logEnabled,
                'sendRequestsWithSalt' => true,
                'baseUrl' => $baseURL
            ));
        } else {
            $this->config = null;
        }
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    private function getAmountRange()
    {
        return new AmountRange(new Amount(1), new Amount(1000));
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        $pageNumber = 1;
        $pageSize = 25;
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');
            return false;
        }

        $getSubscriptionPlansRequest = new GetSubscriptionPlansRequest($this->config);
        $getSubscriptionPlansRequest->setAmount($this->getAmountRange())
            ->setPageNumber($pageNumber)
            ->setPageSize($pageSize);

        /**
         * Now test the getSubscriptionPlans call
         */
        $getSubscriptionPlansApi = new GetSubscriptionPlansApi($this->config, $getSubscriptionPlansRequest);
        $result = $getSubscriptionPlansApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('salt'));
        $this->assertNotEmpty($result->getData('subscriptionPlans'));
        $this->assertNotEmpty($result->getData('totalEntries'));
        $this->assertEquals($pageSize, $result->getData('pageSize'));
        $this->assertEquals($pageNumber, $result->getData('pageNumber'));
    }
}
