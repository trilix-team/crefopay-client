<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Locale;

use Upg\Library\Validation\Helper\Constants;

/**
 * Class Codes
 *
 * @link
 * @package Upg\Library\Locale
 */
class Codes
{
    /**
     * Locale : German - Deutsch
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_DE = "DE";

    /**
     * Locale : English
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_EN = "EN";

    /**
     * Locale : Spanish - Español
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_ES = "ES";

    /**
     * Locale : Finnish - Suomi
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_FI = "FI";

    /**
     * Locale : French - Français
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_FR = "FR";

    /**
     * Locale : Italian - Italiano
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_IT = "IT";

    /**
     * Locale : Dutch - Nederlands
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_NL = "NL";

    /**
     * Locale : Turkish - Türkçe
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_TU = "TU";

    /**
     * Locale : Russian - Русский
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_RU = "RU";

    /**
     * Locale : Portuguese - Português
     *
     * @link https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/supported-languages
     */
    const LOCALE_PT = "PT";

    /**
     * Tag for the constraint validator
     */
    const TAG_LOCALE = "LOCALE";

    /**
     * Validation function
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validateLocale($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_LOCALE);
    }
}
