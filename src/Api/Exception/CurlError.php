<?php
/**
 * CurlError class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api\Exception;

/**
 * Class CurlError
 * Raised if there is a curl error
 *
 * @package Upg\Library\Api\Exception
 */
class CurlError extends AbstractException
{
    /**
     * Constructor
     *
     * @param string $curlErrorMessage
     * @param int    $curlErrorCode
     * @param string $rawResponse
     */
    public function __construct($curlErrorMessage, $curlErrorCode, $rawResponse)
    {
        parent::__construct($curlErrorMessage, $curlErrorCode, $rawResponse);
    }
}
