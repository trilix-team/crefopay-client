<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request\Objects;

use Upg\Library\Validation\Helper\Constants;

/**
 * Class Creditreform
 *
 * For creditreform json objects
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/creditreform
 * @package Upg\Library\Request\Objects
 */
class Creditreform extends AbstractObject
{

    const PRODUCT_IDENTIFICATION = "IDENTIFICATION";
    const PRODUCT_RISK_CHECK = "RISK_CHECK";
    const PRODUCT_TRAFFIC_LIGHT_REPORT = "TRAFFIC_LIGHT_REPORT";
    const PRODUCT_BRIEF_REPORT = "BRIEF_REPORT";
    const PRODUCT_COMPACT_REPORT = "COMPACT_REPORT";
    const PRODUCT_E_CREFO = "E_CREFO";

    const TRAFFIC_LIGHT_NONE = "NONE";
    const TRAFFIC_LIGHT_RED = "RED";
    const TRAFFIC_LIGHT_YELLOW = "YELLOW";
    const TRAFFIC_LIGHT_GREEN = "GREEN";

    const TAG_PRODUCT_NAME = "PRODUCT";
    const TAG_TRAFFIC_LIGHT = "TRAFFIC_LIGHT";


    /**
     * Date of request
     *
     * @var \DateTime Request date
     */
    private $requestDate;

    /**
     * Result of identification
     *
     * @var string Identification result
     */
    private $crefoProductName;

    /**
     * Result of address validation check
     *
     * @var boolean Address validation result
     */
    private $blackWhiteResult = null;

    /**
     * Result of traffic light check
     *
     * @var string Traffic Light Result
     */
    private $trafficLightResult;

    /**
     * Set the requestDate field
     *
     * @param \DateTime $requestDate
     *
     * @return $this
     */
    public function setRequestDate(\DateTime $requestDate)
    {
        $this->requestDate = $requestDate;
        return $this;
    }

    /**
     * Get the requestDate field
     *
     * @return \DateTime
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * Set the crefoProductName field
     *
     * @param string $crefoProductName
     *
     * @return $this
     */
    public function setCrefoProductName($crefoProductName)
    {
        $this->crefoProductName = $crefoProductName;
        return $this;
    }

    /**
     * Get the crefoProductName field
     *
     * @return string
     */
    public function getCrefoProductName()
    {
        return $this->crefoProductName;
    }

    /**
     * Set the blackWhiteResult field
     *
     * @param boolean $blackWhiteResult
     *
     * @return $this
     */
    public function setBlackWhiteResult($blackWhiteResult)
    {
        $this->blackWhiteResult = $blackWhiteResult;
        return $this;
    }

    /**
     * Get the blackWhiteResult field
     *
     * @return boolean
     */
    public function getBlackWhiteResult()
    {
        return $this->blackWhiteResult;
    }

    /**
     * Set the trafficLightResult field
     *
     * @param string $trafficLightResult
     *
     * @return $this
     */
    public function setTrafficLightResult($trafficLightResult)
    {
        $this->trafficLightResult = $trafficLightResult;
        return $this;
    }

    /**
     * Get the trafficLightResult field
     *
     * @return string
     */
    public function getTrafficLightResult()
    {
        return $this->trafficLightResult;
    }

    /**
     * Convert to array for validator
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'crefoProductName' => $this->getCrefoProductName(),
            'trafficLightResult' => $this->getTrafficLightResult(),
        );
        if ($this->requestDate && $this->requestDate instanceof \DateTime) {
            $return['requestDate'] = $this->getRequestDate()->format('Y-m-d');
        } else {
            $return['requestDate'] = "";
        }

        if (isset($this->blackWhiteResult)) {
            $return['blackWhiteResult'] = $this->getBlackWhiteResult();
        }

        return $return;
    }

    /**
     * Validation meta data
     *
     * @see https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/creditreform
     * @return array
     */
    public function getValidationData()
    {
        $validationData = array();

        $validationData['requestDate'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "RequestDate is required"
        );

        $validationData['crefoProductName'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "CrefoProductName is required"
        );

        $validationData['crefoProductName'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateCrefoProductName',
            'message' => "CrefoProductName must have a valid value"
        );

        if (isset($this->blackWhiteResult)) {
            $validationData['blackWhiteResult'][] = array(
                'name' => 'Callback',
                'value' => get_class($this) . '::validateBlackWhiteResult',
                'message' => "BlackWhiteResult is not a valid value"
            );
        }

        $validationData['trafficLightResult'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "TrafficLightResult is required"
        );

        $validationData['trafficLightResult'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateTrafficLight',
            'message' => "TrafficLightResult must have a valid value"
        );

        return $validationData;
    }

    /**
     * Validate the crefoProductName type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateCrefoProductName($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_PRODUCT_NAME);
    }

    /**
     * Validate the trafficLight type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateTrafficLight($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_TRAFFIC_LIGHT);
    }

    /**
     * Validate the blackWhite type
     *
     * @param $value
     *
     * @return boolean
     */
    public static function validateBlackWhiteResult($value)
    {
        return $value === true || $value === false;
    }
}
