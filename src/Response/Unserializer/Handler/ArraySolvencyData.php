<?php
/**
 * Class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\Address as CorrectedAddress;
use Upg\Library\Request\Objects\SolvencyData;
use Upg\Library\Response\Unserializer\Processor;

/**
 * Class ArraySolvencyData
 *
 * Unserializer for solvency data
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/
 * @package Upg\Library\Response\Unserializer\Handler
 */
class ArraySolvencyData implements UnserializerInterface
{
    /**
     * Return the string of the property that the deserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'solvencyData',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return array
     */
    public function unserializeProperty(Processor $processor, $value)
    {
        $data = array();

        foreach ($value as $solvencyDataResult) {
            $solvencyCheck = new SolvencyData();
            $solvencyCheck->setUnserializedData($solvencyDataResult);
            $lastRequestConvertedDate = new \DateTime($solvencyDataResult['lastRequestDate']);
            $solvencyCheck->setLastRequestDate($lastRequestConvertedDate);
            $checkConvertedDate = new \DateTime($solvencyDataResult['checkDate']);
            $solvencyCheck->setCheckDate($checkConvertedDate);
            if (isset($solvencyDataResult['score'])) {
                if ($solvencyDataResult['solvencyInterface'] === 'BONIMA' || $solvencyDataResult['solvencyInterface'] === 'BUERGEL') {
                    $solvencyCheck->setScore(doubleval($solvencyDataResult['score']));
                } else {
                    $solvencyCheck->setScore(intval($solvencyDataResult['score']));
                }
            }
            if (isset($solvencyDataResult['correctedAddress'])) {
                preg_match_all("/(:?.*)\ (:?.*)\,\ (:?.*)\ (:?.*)\,\ (:?.*)/m", $solvencyDataResult['correctedAddress'],
                    $matches, PREG_SET_ORDER, 0);
                // Match might not work in case of failed BONIMA checks since those don't have an address set
                if (!empty($matches)) {
                    list(, $street, $no, $zip, $city, $country) = $matches[0];
                    $correctedAddress = new CorrectedAddress();
                    $correctedAddress->setStreet($street)
                        ->setNo($no)
                        ->setZip($zip)
                        ->setCity($city)
                        ->setCountry($country);
                    $solvencyCheck->setCorrectedAddress($correctedAddress);
                }
            }
            $data[] = $solvencyCheck;
        }

        return $data;
    }
}
