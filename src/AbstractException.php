<?php
/**
 * AbstractException global class
 *
 * @author Michael Fisher
 */

namespace Upg\Library;

/**
 * Class AbstractException
 *
 * Exception class for all exceptions thrown by the library
 *
 * @package Upg\Library
 */
abstract class AbstractException extends \Exception
{
}
