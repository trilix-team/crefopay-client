<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Basket;

use Upg\Library\Validation\Helper\Constants;

/**
 * Class BasketItemType
 *
 * Basket Item Type class definitions
 *
 * @package Upg\Library\Basket
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/basket
 */
class BasketItemType
{
    /**
     * Default basket item type
     */
    const BASKET_ITEM_TYPE_DEFAULT = 'DEFAULT';

    /**
     * Type for basket item which represents the shipping cost
     */
    const BASKET_ITEM_TYPE_SHIPPINGCOST = 'SHIPPINGCOSTS';

    /**
     * Type for an basket item which represents any coupons applied to the order
     */
    const BASKET_ITEM_TYPE_COUPON = 'COUPON';

    /**
     * Validation function for this class
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validateBasketItemType($value)
    {
        return Constants::validateConstant(__CLASS__, $value, 'BASKET_ITEM_TYPE');
    }
}
