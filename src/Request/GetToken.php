<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request;

/**
 * Class GetToken
 *
 * This is the request class for any GetToken request object
 *
 * @deprecated 1.0.11 Will be removed in version 1.0.15
 * @link       https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/paycobridge-js
 * @package    Upg\Library\Request
 */
class GetToken extends AbstractRequest
{
    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        return array();
    }

    /**
     * Get the validation
     *
     * @return array
     */
    public function getClassValidationData()
    {
        return array();
    }
}
