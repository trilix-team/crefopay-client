<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Logging;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Upg\Library\Config;

/**
 * Class Factory
 *
 * Provides an Logger
 *
 * @package Upg\Library\Logging
 */
class Factory
{
    /**
     * Array of initialized loggers
     *
     * @var array
     */
    private static $loggers = array();

    /**
     * Gets a logger based on the logLocation
     *
     * @param Config $config
     * @param        $logLocation
     *
     * @return LoggerInterface
     */
    public static function getLogger(Config $config, $logLocation)
    {
        $log = new Blank();
        if (!array_key_exists($logLocation, self::$loggers)) {
            if ($config->getLogEnabled()) {
                $log = new Logger('crefopay');
                try {
                    $log->pushHandler(new StreamHandler($logLocation, $config->getLogLevel()));
                } catch (\Exception $e) {
                }
            }
        }
        self::$loggers[$logLocation] = $log;
        return self::$loggers[$logLocation];
    }
}
