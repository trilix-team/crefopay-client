<?php

namespace Upg\Library\Tests\Endtoend\Integration;

use Faker\Factory;
use Upg\Library\Api\Exception\ApiError;
use Upg\Library\Config;
use Upg\Library\Locale\Codes;
use Upg\Library\Request\Objects\SolvencyData;
use Upg\Library\Risk\RiskClass;
use Upg\Library\PaymentMethods\Methods;
use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Objects\Person;
use Upg\Library\Request\Objects\BasketItem;
use Upg\Library\Api\CreateTransaction;
use Upg\Library\Request\CreateTransaction as CreateTransactionRequest;
use Upg\Library\Api\Reserve as ReserveApi;
use Upg\Library\Request\Reserve as ReserveRequest;
use Upg\Library\Api\RegisterUser as RegisterUserApi;
use Upg\Library\Request\RegisterUser as RegisterUserRequest;
use Upg\Library\Api\GetUser as GetUserApi;
use Upg\Library\Request\GetUser as GetUserRequest;

/**
 * @group Endtoend
 */
class GetUserTest extends \PHPUnit\Framework\TestCase
{
    private $faker;

    /**
     * Config object for tests
     *
     * @var Config
     */
    private $config = null;

    private $user;

    private $address;

    public static $userId;

    public static function setUpBeforeClass()
    {
        self::$userId = "REGISTERED:" . hash('md5', microtime());
    }

    public function setUp()
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->faker = $faker;

        $merchantPassword = trim(getenv('CREFOPAY_TEST_MERCHANT_PASSWORD'));
        $merchantID = trim(getenv('CREFOPAY_TEST_MERCHANT_ID'));
        $storeID = trim(getenv('CREFOPAY_TEST_STORE_ID'));
        $baseURL = trim(getenv('CREFOPAY_TEST_BASE_URL'));
        $logEnabled = false;

        if (!empty($merchantPassword) && !empty($merchantID) && !empty($storeID) && !empty($baseURL)) {
            $this->config = new Config(array(
                'merchantPassword' => $merchantPassword,
                'merchantID' => $merchantID,
                'storeID' => $storeID,
                'logEnabled' => $logEnabled,
                'sendRequestsWithSalt' => true,
                'baseUrl' => $baseURL
            ));
        } else {
            $this->config = null;
        }
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    private function getUser()
    {
        if (is_null($this->user)) {
            $date = new \DateTime();
            $date->setDate(1980, 1, 1);

            $this->user = new Person();
            $this->user->setSalutation(Person::SALUTATIONMALE)
                ->setName($this->faker->name)
                ->setSurname($this->faker->name)
                ->setDateOfBirth($date)
                ->setEmail(strtolower($this->faker->email))
                ->setPhoneNumber('03452696645')
                ->setFaxNumber('03452696645');
        }

        return $this->user;
    }

    private function getAddress()
    {
        if (is_null($this->address)) {
            $this->address = new \Upg\Library\Request\Objects\Address();
            $this->address->setStreet("Test")
                ->setNo(45)
                ->setZip("12100")
                ->setCity("City")
                ->setState("State")
                ->setCountry("DE");
        }

        return $this->address;
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item CALL")
            ->setBasketItemCount(1)
            ->setBasketItemAmount(new Amount(101));

        return $item;
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');
            return false;
        }
        $billingRecipient = $this->faker->name;
        $shippingRecipient = $this->faker->name;

        $registerRequest = new RegisterUserRequest($this->config);
        $registerRequest->setUserID(self::$userId)
            ->setUserType(\Upg\Library\User\Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setBillingRecipient($billingRecipient)
            ->setShippingAddress($this->getAddress())
            ->setShippingRecipient($shippingRecipient)
            ->setLocale(Codes::LOCALE_EN);

        $registerUserApi = new RegisterUserApi($this->config, $registerRequest);
        $registerUserApi->sendRequest();

        /**
         * ok now get the user
         */
        $getUserRequest = new GetUserRequest($this->config);
        $getUserRequest->setUserID(self::$userId);

        $getUserApi = new GetUserApi($this->config, $getUserRequest);
        $result = $getUserApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));

        /**
         * @var Person $returnedPerson
         */
        $returnedPerson = $result->getData('userData');
        $this->assertEquals(Person::SALUTATIONMALE, $returnedPerson->getSalutation());
        $this->assertEquals($this->getUser()->getName(), $returnedPerson->getName());
        $this->assertEquals($this->getUser()->getSurname(), $returnedPerson->getSurname());
        $this->assertEquals(
            $this->getUser()->getDateOfBirth()->format('Y-m-d'),
            $returnedPerson->getDateOfBirth()->format('Y-m-d')
        );

        $this->assertEquals($this->getUser()->getEmail(), $returnedPerson->getEmail());
        $this->assertEquals($this->getUser()->getPhoneNumber(), $returnedPerson->getPhoneNumber());

        $this->assertEquals($billingRecipient, $result->getData('billingRecipient'));

        /**
         * billingAddress and shippingAddress should be converted to an address object
         */
        $this->assertEquals($this->getAddress()->getNo(), $result->getData('billingAddress')->getNo());
        $this->assertEquals($this->getAddress()->getStreet(), $result->getData('billingAddress')->getStreet());
        $this->assertEquals($this->getAddress()->getZip(), $result->getData('billingAddress')->getZip());
        $this->assertEquals($this->getAddress()->getCity(), $result->getData('billingAddress')->getCity());
        $this->assertEquals($this->getAddress()->getState(), $result->getData('billingAddress')->getState());
        $this->assertEquals($this->getAddress()->getCountry(), $result->getData('billingAddress')->getCountry());

        $this->assertEquals($this->getAddress()->getNo(), $result->getData('shippingAddress')->getNo());
        $this->assertEquals($this->getAddress()->getStreet(), $result->getData('shippingAddress')->getStreet());
        $this->assertEquals($this->getAddress()->getZip(), $result->getData('shippingAddress')->getZip());
        $this->assertEquals($this->getAddress()->getCity(), $result->getData('shippingAddress')->getCity());
        $this->assertEquals($this->getAddress()->getState(), $result->getData('shippingAddress')->getState());
        $this->assertEquals($this->getAddress()->getCountry(), $result->getData('shippingAddress')->getCountry());
    }

    /**
     * Make an successful call with no risk data set
     * Create a transaction then request the risk data
     *
     * @depends testSuccessfulApiCall
     */
    public function testSuccessfulRiskDataEmptyApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');
            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        // Unique order ID for the tests
        $orderId = hash('crc32b', microtime());

        $request->setOrderID($orderId)
            ->setUserID(self::$userId)
            ->setIntegrationType(CreateTransactionRequest::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(CreateTransactionRequest::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setAmount(new Amount(101))
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        if (!in_array('PREPAID', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks PREPAID can not continue");
        }

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_PREPAID);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        $reserveApi->sendRequest();

        $getUserRequest = new GetUserRequest($this->config);
        $getUserRequest->setUserID(self::$userId)
            ->setReturnRiskData(true);

        /**
         * Now test the getTransactionStatus call
         */
        $getUserApi = new GetUserApi($this->config, $getUserRequest);
        $result = $getUserApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));

        $riskData = $result->getData('riskData');
        $this->assertNotEmpty($riskData);
        $this->assertEmpty($riskData['solvencyData']);
    }

    /**
     * Make an successful call with risk data set
     * Create a transaction then request the risk data
     *
     * @depends testSuccessfulApiCall
     */
    public function testSuccessfulRiskDataSetApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');
            return false;
        }
        $request = new CreateTransactionRequest($this->config);

        // Unique order ID for the tests
        $orderId = hash('crc32b', microtime());

        $request->setOrderID($orderId)
            ->setUserID(self::$userId)
            ->setIntegrationType(CreateTransactionRequest::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(CreateTransactionRequest::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setAmount(new Amount(101))
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        if (!in_array('BILL', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks BILL can not continue");
        }

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        try {
            $reserveApi->sendRequest();
        } catch (ApiError $ae) {}

        $getUserRequest = new GetUserRequest($this->config);
        $getUserRequest->setUserID(self::$userId)
            ->setReturnRiskData(true);

        /**
         * Now test the getTransactionStatus call
         */
        $getUserApi = new GetUserApi($this->config, $getUserRequest);
        $result = $getUserApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));

        $riskData = $result->getData('riskData');
        $this->assertNotEmpty($riskData);
        $this->assertNotEmpty($riskData['solvencyData']);
        $this->assertEquals(1, count($riskData['solvencyData']));
        $this->assertInstanceOf('\Upg\Library\Request\Objects\SolvencyData', $riskData['solvencyData'][0]);
        /** @var SolvencyData $solvencyResult */
        $solvencyResult = $riskData['solvencyData'][0];
        $this->assertEquals(SolvencyData::CHECK_INTERFACE_BONIMA, $solvencyResult->getSolvencyInterface());
        $this->assertEquals(SolvencyData::CHECK_TYPE_SECONDLEVEL, $solvencyResult->getCheckType());
        $this->assertEquals(Methods::PAYMENT_METHOD_TYPE_BILL, $solvencyResult->getPaymentMethod());
        $this->assertEquals("Anmeldung Fehler", $solvencyResult->getMessage());
        $this->assertEquals(true, $solvencyResult->isThirdPartyRequested());
    }

    public static function tearDownAfterClass()
    {
        self::$userId = null;
    }
}
