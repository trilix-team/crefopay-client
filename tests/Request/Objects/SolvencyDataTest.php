<?php
namespace Upg\Library\Tests\Request\Objects;

use Upg\Library\PaymentMethods\Methods;
use Upg\Library\Request\Objects\Address;
use Faker\Factory as Factory;
use Upg\Library\Request\Objects\SolvencyData;
use Upg\Library\Tests\Request\AbstractRequestTest;
use Upg\Library\Validation\Validation;

class SolvencyDataTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function setUp()
    {
        $faker = Factory::create();


        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown()
    {
        unset($this->faker);
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Teststrasse")
            ->setNo("1")
            ->setZip("10000")
            ->setCity("Berlin")
            ->setCountry("DE");
        return $address;
    }

    public function testCompleteObjectBonima()
    {
        $lastRequest = new \DateTime('now');
        $checkDate = new \DateTime('now');
        $bonimaSolvencyData = new SolvencyData();
        $bonimaSolvencyData->setSolvencyInterface(SolvencyData::CHECK_INTERFACE_BONIMA)
            ->setLastRequestDate($lastRequest)
            ->setCheckDate($checkDate)
            ->setCheckType(SolvencyData::CHECK_TYPE_SECONDLEVEL)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_DD)
            ->setActions(array(SolvencyData::CHECK_ACTION_HIGH, SolvencyData::CHECK_ACTION_RESTRICTION))
            ->setThirdPartyRequested(true)
            ->setOrderID("169")
            ->setScore(5.46)
            ->setTrafficLight(SolvencyData::CHECK_TRAFFIC_LIGHT_YELLOW)
            ->setIdentification(SolvencyData::CHECK_IDENTIFICATION_PERSON_HOUSEHOLD_IDENTIFIED)
            ->setAddressValidation(SolvencyData::CHECK_ADDRESS_VALIDATION_CORRECTED_AND_VALIDATED)
            ->setCorrectedAddress($this->getAddress());

        $validation = new Validation();
        $validation->getValidator($bonimaSolvencyData);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCompleteObjectBuergel()
    {
        $lastRequest = new \DateTime('now');
        $checkDate = new \DateTime('now');
        $buergelSolvencyCheck = new SolvencyData();
        $buergelSolvencyCheck->setSolvencyInterface(SolvencyData::CHECK_INTERFACE_BUERGEL)
            ->setLastRequestDate($lastRequest)
            ->setCheckDate($checkDate)
            ->setCheckType(SolvencyData::CHECK_TYPE_FIRSTLEVEL)
            ->setActions(array(SolvencyData::CHECK_ACTION_DECLINED))
            ->setThirdPartyRequested(false)
            ->setOrderID("169")
            ->setScore(45.46)
            ->setAddressOrigin("What")
            ->setAddressOriginCode("is")
            ->setDecisionMessage("buergel?");

        $validation = new Validation();
        $validation->getValidator($buergelSolvencyCheck);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCompleteObjectCrediconnect()
    {
        $lastRequest = new \DateTime('now');
        $checkDate = new \DateTime('now');
        $crediconnectSolvencyCheck = new SolvencyData();
        $crediconnectSolvencyCheck->setSolvencyInterface(SolvencyData::CHECK_INTERFACE_CREDICONNECT)
            ->setLastRequestDate($lastRequest)
            ->setCheckDate($checkDate)
            ->setCheckType(SolvencyData::CHECK_TYPE_SECONDLEVEL)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL)
            ->setActions(array(SolvencyData::CHECK_ACTION_MANUAL_CHECK))
            ->setThirdPartyRequested(true)
            ->setTrafficLight(SolvencyData::CHECK_TRAFFIC_LIGHT_GREEN)
            ->setStatusCode("ALIVE");

        $validation = new Validation();
        $validation->getValidator($crediconnectSolvencyCheck);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCompleteObjectCreditreform()
    {
        $lastRequest = new \DateTime('now');
        $checkDate = new \DateTime('now');
        $creditreformSolvencyCheck = new SolvencyData();
        $creditreformSolvencyCheck->setSolvencyInterface(SolvencyData::CHECK_INTERFACE_CREDITREFORM)
            ->setLastRequestDate($lastRequest)
            ->setCheckDate($checkDate)
            ->setCheckType(SolvencyData::CHECK_TYPE_SECONDLEVEL)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL)
            ->setActions(array(SolvencyData::CHECK_ACTION_MANUAL_CHECK))
            ->setThirdPartyRequested(true)
            ->setTrafficLight(SolvencyData::CHECK_TRAFFIC_LIGHT_YELLOW)
            ->setCheckResult(SolvencyData::CHECK_RESULT_BLACK)
            ->setProductName(SolvencyData::CHECK_PRODUCT_NAME_IDENTIFICATION)
            ->setIdentificationNumber("421662250");

        $validation = new Validation();
        $validation->getValidator($creditreformSolvencyCheck);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCompleteObjectCube()
    {
        $lastRequest = new \DateTime('now');
        $checkDate = new \DateTime('now');
        $cubeSolvencyCheck = new SolvencyData();
        $cubeSolvencyCheck->setSolvencyInterface(SolvencyData::CHECK_INTERFACE_CUBE)
            ->setLastRequestDate($lastRequest)
            ->setCheckDate($checkDate)
            ->setCheckType(SolvencyData::CHECK_TYPE_SECONDLEVEL)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL)
            ->setActions(array(SolvencyData::CHECK_ACTION_MANUAL_CHECK))
            ->setThirdPartyRequested(true)
            ->setScore(200)
            ->setTrafficLight(SolvencyData::CHECK_TRAFFIC_LIGHT_YELLOW)
            ->setNegativeIndicator(false)
            ->setStatus(SolvencyData::CHECK_STATUS_DEAD)
            ->setCorrectedAddress($this->getAddress());

        $validation = new Validation();
        $validation->getValidator($cubeSolvencyCheck);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCompleteObjectFlexConnect()
    {
        $lastRequest = new \DateTime('now');
        $checkDate = new \DateTime('now');
        $flexconnectSolvencyCheck = new SolvencyData();
        $flexconnectSolvencyCheck->setSolvencyInterface(SolvencyData::CHECK_INTERFACE_FLEXCONNECT)
            ->setLastRequestDate($lastRequest)
            ->setCheckDate($checkDate)
            ->setCheckType(SolvencyData::CHECK_TYPE_SECONDLEVEL)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL)
            ->setActions(array(SolvencyData::CHECK_ACTION_MANUAL_CHECK))
            ->setThirdPartyRequested(true)
            ->setTrafficLight(SolvencyData::CHECK_TRAFFIC_LIGHT_RED);

        $validation = new Validation();
        $validation->getValidator($flexconnectSolvencyCheck);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCompleteObjectVerita()
    {
        $lastRequest = new \DateTime('now');
        $checkDate = new \DateTime('now');
        $veritaSolvencyCheck = new SolvencyData();
        $veritaSolvencyCheck->setSolvencyInterface(SolvencyData::CHECK_INTERFACE_VERITA)
            ->setLastRequestDate($lastRequest)
            ->setCheckDate($checkDate)
            ->setCheckType(SolvencyData::CHECK_TYPE_SECONDLEVEL)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL)
            ->setActions(array(SolvencyData::CHECK_ACTION_MANUAL_CHECK))
            ->setThirdPartyRequested(true)
            ->setScore(5000)
            ->setTrafficLight(SolvencyData::CHECK_TRAFFIC_LIGHT_RED)
            ->setIdentification(SolvencyData::CHECK_IDENTIFICATION_PERSON_IDENTIFIED)
            ->setAddressValidation(SolvencyData::CHECK_ADDRESS_VALIDATION_SUCCESSFULLY_VALIDATED)
            ->setSpecialAddress(SolvencyData::CHECK_SPECIAL_ADDRESS_S2)
            ->setCorrectedAddress($this->getAddress());

        $validation = new Validation();
        $validation->getValidator($veritaSolvencyCheck);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }
}
