<?php

namespace Upg\Library\Tests\Endtoend\Integration;

use Faker\Factory;
use Upg\Library\Config;
use Upg\Library\Request\GetClearingFileList as GetClearingFileListRequest;
use Upg\Library\Api\GetClearingFileList as GetClearingFileListApi;

/**
 * @group Endtoend
 */
class GetClearingFileListTest extends \PHPUnit\Framework\TestCase
{
    private $faker;

    /**
     * Config object for tests
     *
     * @var Config
     */
    private $config;

    public function setUp()
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->faker = $faker;

        $merchantPassword = trim(getenv('CREFOPAY_TEST_MERCHANT_PASSWORD'));
        $merchantID = trim(getenv('CREFOPAY_TEST_MERCHANT_ID'));
        $storeID = trim(getenv('CREFOPAY_TEST_STORE_ID'));
        $baseURL = trim(getenv('CREFOPAY_TEST_BASE_URL'));
        $logEnabled = false;

        if (!empty($merchantPassword) && !empty($merchantID) && !empty($storeID) && !empty($baseURL)) {
            $this->config = new Config(array(
                'merchantPassword' => $merchantPassword,
                'merchantID' => $merchantID,
                'storeID' => $storeID,
                'logEnabled' => $logEnabled,
                'sendRequestsWithSalt' => true,
                'baseUrl' => $baseURL
            ));
        } else {
            $this->config = null;
        }
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    /**
     * Make an successful call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');
            return false;
        }

        $clearingFileListRequest = new GetClearingFileListRequest($this->config);
        $clearingFileListRequest->setFrom(new \DateTime('2017-01-01'))
            ->setTo(new \DateTime('2017-02-01'));

        $clearingFileList = new GetClearingFileListApi($this->config, $clearingFileListRequest);
        $result = $clearingFileList->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));
    }

    /**
     * Make a call which receives no clearing file information
     */
    public function testEmptyApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');
            return false;
        }

        $clearingFileListRequest = new GetClearingFileListRequest($this->config);
        $clearingFileListRequest->setFrom(new \DateTime('2015-01-01'))
            ->setTo(new \DateTime('2015-02-01'));

        $clearingFileList = new GetClearingFileListApi($this->config, $clearingFileListRequest);
        $result = $clearingFileList->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertEmpty($result->getData('clearingFiles'));
        $this->assertNotEmpty($result->getData('salt'));
    }
}