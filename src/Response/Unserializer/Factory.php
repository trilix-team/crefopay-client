<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer;

/**
 * Class Factory
 *
 * @link
 * @package Upg\Library\Response\Unserializer
 */

class Factory
{
    /**
     * Get the processor with all default deserializer handlers in the API
     *
     * @return Processor
     */
    public static function getProcessor()
    {
        $instance = new Processor();
        $instance->addUnserializerHandler(new Handler\Address());
        $instance->addUnserializerHandler(new Handler\Amount());
        $instance->addUnserializerHandler(new Handler\ArrayPaymentInstruments());
        $instance->addUnserializerHandler(new Handler\ArrayClearingFiles());
        $instance->addUnserializerHandler(new Handler\RiskData());
        $instance->addUnserializerHandler(new Handler\ArraySolvencyData());
        $instance->addUnserializerHandler(new Handler\ArraySubscriptionPlans());
        $instance->addUnserializerHandler(new Handler\Company());
        $instance->addUnserializerHandler(new Handler\PaymentInstruments());
        $instance->addUnserializerHandler(new Handler\Person());

        return $instance;
    }
}
