<?php

namespace Upg\Library\Tests\Request;

use Faker\Factory;
use Upg\Library\Config;
use Upg\Library\Locale\Codes;
use Upg\Library\Request\CreateSubscription;
use Upg\Library\Request\Objects\Address;
use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Objects\BasketItem;
use Upg\Library\Request\Objects\Person;
use Upg\Library\Request\Objects\Company;
use Upg\Library\Risk\RiskClass;
use Upg\Library\Validation\Validation;

class CreateSubscriptionTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var \Faker\Generator
     */
    private $faker;

    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp()
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;

        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
        ));
    }

    public function tearDown()
    {
        unset($this->faker);
        unset($this->config);
    }

    /**
     * Get the person
     * @return Person
     */
    private function getUser()
    {
        $user = new Person();
        $user->setSalutation(PERSON::SALUTATIONMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth(new \DateTime())
            ->setEmail($this->faker->email)
            ->setPhoneNumber('555666')
            ->setFaxNumber('555454');

        return $user;
    }

    /**
     * Get the company
     * @return Company
     */
    private function getCompany()
    {
        $company = new Company();
        $company->setEmail($this->faker->email)
            ->setCompanyName($this->faker->company)
            ->setCompanyRegistrationID('123456')
            ->setCompanyVatID('123456')
            ->setCompanyTaxID('123456')
            ->setCompanyRegisterType(Company::COMPANY_TYPE_HRB);

        return $company;
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setNo(45)
            ->setZip("LS12 4TN")
            ->setCity("City")
            ->setState("State")
            ->setCountry("GB");

        return $address;
    }

    private function getAmount()
    {
        return new Amount(100, 0, 0);
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item")
            ->setBasketItemCount(1)
            ->setBasketItemAmount($this->getAmount());

        return $item;
    }


    public function testCreateSubscriptionValidationB2CSuccess()
    {
        $request = new CreateSubscription($this->config);
        $request->setPlanReference("D3M0")
            ->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCreateSubscriptionValidationB2BSuccess()
    {
        $request = new CreateSubscription($this->config);
        $request->setPlanReference("D3M0")
            ->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST")
            ->setUserType(CreateSubscription::USER_TYPE_BUSINESS)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setCompanyData($this->getCompany())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testCreateSubscriptionValidationPlanReference()
    {
        $validation = new Validation();
        /**
         * Test required
         */

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST")
            ->setUserType(CreateSubscription::USER_TYPE_BUSINESS)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setCompanyData($this->getCompany())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'planReference',
            'planReference is required',
            $data,
            "planReference requirement validation failed"
        );

        /**
         * Test length validation
         */
        $request->setPlanReference($this->veryLongString);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'planReference',
            'planReference must be between 1 and 15 characters',
            $data,
            "PlanReference character limit check failed"
        );
    }

    public function testCreateSubscriptionValidationSubscriptionID()
    {
        $validation = new Validation();

        // Testing requirement
        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setUserID(1)
            ->setMerchantReference("TEST")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem());

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'subscriptionID',
            'subscriptionID is required',
            $data,
            "subscriptionID requirement validation failed"
        );

        // Testing length validation
        $request->setSubscriptionID($this->veryLongString);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'subscriptionID',
            'subscriptionID must be between 1 and 30 characters',
            $data,
            "subscriptionID limit validation failed"
        );
    }

    public function testCreateSubscriptionValidationUserID()
    {
        $validation = new Validation();
        /**
         * Test required
         */

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setMerchantReference("TEST")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem());

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'userID',
            'userID is required',
            $data,
            "userID is required validation failed"
        );

        /**
         * Test length validation
         */
        $request->setUserID($this->veryLongString);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'userID',
            'userID must be between 1 and 50 characters',
            $data,
            "userID must be between 1 and 50 characters failed"
        );
    }

    public function testCreateSubscriptionValidationMerchantReference()
    {
        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference($this->veryLongString)
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem());
        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'merchantReference',
            'merchantReference must be between 1 and 30 characters',
            $data,
            "merchantReference must be between 1 and 30 characters failed"
        );
    }

    public function testCreateSubscriptionValidationUserType()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(null)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem());

        $validation->getValidator($request);
        $data = $validation->performValidation();

        /**
         * Test required
         */
        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'userType',
            'userType must be certain values',
            $data,
            "userType must be certain values failed"
        );

        /**
         * Test certain values validation
         */
        $request->setUserType($this->faker->name);

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'userType',
            'userType must be certain values',
            $data,
            "userType must be certain values failed"
        );

    }

    public function testCreateSubscriptionValidationUserRiskClass()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass($this->faker->name)
            ->setUserIpAddress("192.168.1.2")
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem());


        $request->setUserType($this->faker->name);

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'userRiskClass',
            'userRiskClass must contain certain values or be empty',
            $data,
            "userRiskClass must certain values or be empty failed"
        );

    }

    public function testCreateSubscriptionValidationUserIpAddress()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress($this->veryLongString)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem());

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'userIpAddress',
            'userIpAddress must be between 1 and 15 characters',
            $data,
            "userIpAddress must be between 1 and 15 characters failed"
        );
    }

    public function testCreateSubscriptionValidationBillingRecipient()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress('192.168.0.1')
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setBillingRecipient($this->veryLongString);

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'billingRecipient',
            'billingRecipient must be between 1 and 80 characters',
            $data,
            "billingRecipient must be between 1 and 80 characters failed"
        );
    }

    public function testCreateSubscriptionValidationBillingRecipientAddition()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress('192.168.0.1')
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setBillingRecipientAddition($this->veryLongString);

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'billingRecipientAddition',
            'billingRecipientAddition must be between 1 and 80 characters',
            $data,
            "billingRecipientAddition must be between 1 and 80 characters failed"
        );
    }

    public function testCreateSubscriptionValidationShippingRecipient()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress('192.168.0.1')
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setShippingRecipient($this->veryLongString);

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'shippingRecipient',
            'shippingRecipient must be between 1 and 80 characters',
            $data,
            "shippingRecipient must be between 1 and 80 character failed"
        );
    }

    public function testCreateSubscriptionValidationShippingRecipientAddition()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress('192.168.0.1')
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setShippingRecipientAddition($this->veryLongString);

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'shippingRecipientAddition',
            'shippingRecipientAddition must be between 1 and 80 characters',
            $data,
            "shippingRecipientAddition must be between 1 and 80 characters failed"
        );
    }

    public function testCreateSubscriptionValidationAmount()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress('192.168.0.1')
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->addBasketItem($this->getBasketItem());

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'amount',
            'amount must be set for the transaction',
            $data,
            "amount must be set for the transaction failed"
        );
    }

    public function testCreateSubscriptionValidationBasketItems()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST FIELD")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress('192.168.0.1')
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount());

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'basketItems',
            'basketItems must be added to the transaction',
            $data,
            "basketItems must be added to the transaction failed"
        );
    }

    public function testCreateSubscriptionValidationLocale()
    {
        $validation = new Validation();

        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem());

        /**
         * Test required
         */
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'locale',
            'locale must be set for the transaction',
            $data,
            "locale must be set for the transaction failed"
        );

        /**
         * Test set values validation
         */
        $request->setLocale($this->faker->name);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\CreateSubscription',
            'locale',
            'locale must be certain values',
            $data,
            "locale must be certain values failed"
        );
    }

    public function testCreateSubscriptionSaltGeneration()
    {
        $request = new CreateSubscription($this->config);
        $request->setTrialPeriod(36)
            ->setSubscriptionID(1)
            ->setUserID(1)
            ->setMerchantReference("TEST")
            ->setUserType(CreateSubscription::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserIpAddress("192.168.1.2")
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $data = $request->getSerializerData();
        $this->assertArrayHasKey("salt", $data, "Salt was not set");
    }
}
