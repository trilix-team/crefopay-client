<?php
/**
 * Amount class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Request\Objects;

/**
 * Class Amount
 *
 * For the Amount json objects
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/amounts
 * @package Upg\Library\Request\Objects
 */
class Amount extends AbstractObject
{

    /**
     * Amount in smallest possible denomination ie cents, pence etc
     *
     * @var int
     */
    private $amount;

    /**
     * VAT\Tax amount in smallest possible denomination ie cents, pence etc
     *
     * @var int
     */
    private $vatAmount;

    /**
     * VAT\Tax rate in percentage to two decimal places
     *
     * @var float
     */
    private $vatRate;

    /**
     * Constructor
     *
     * @param int $amount
     * @param int $vatAmount
     * @param int $vatRate
     */
    public function __construct($amount = 0, $vatAmount = 0, $vatRate = 0)
    {
        if ($amount >= 0) {
            $this->setAmount($amount);
            $this->setVatAmount($vatAmount);
            $this->setVatRate($vatRate);
        }
    }

    /**
     * Set the amount for the object
     *
     * @param int $amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get the amount that has been set
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the vat amount
     *
     * @param int $vatAmount
     *
     * @return $this
     */
    public function setVatAmount($vatAmount)
    {
        $this->vatAmount = $vatAmount;
        return $this;
    }

    /**
     * Return the vat amount that has been set
     *
     * @return int
     */
    public function getVatAmount()
    {
        return $this->vatAmount;
    }

    /**
     * Set the vat rate
     *
     * @param float $vatRate
     *
     * @return $this
     */
    public function setVatRate($vatRate)
    {
        $this->vatRate = $vatRate;
        return $this;
    }

    /**
     * Returns the set VAT rate
     *
     * @return int
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * Returns an array of all set values in this instance
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'amount' => $this->amount,
        );

        if ($this->vatAmount) {
            $return['vatAmount'] = $this->vatAmount;
        }

        if ($this->vatRate) {
            $return['vatRate'] = $this->vatRate;
        }

        return $return;
    }

    /**
     * Validation meta data
     *
     * @see https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/amounts
     *
     * @return array
     */
    public function getValidationData()
    {
        $validationData = array();

        $validationData['amount'][] = array('name' => 'required', 'value' => null, 'message' => "Amount is required");
        $validationData['amount'][] = array(
            'name' => 'Integer',
            'value' => null,
            'message' => "Amount must be an integer"
        );
        $validationData['amount'][] = array(
            'name' => 'Regex',
            'value' => '/^\d{0,16}$/',
            'message' => "Amount must be between 1 and 16 digits"
        );

        $validationData['vatAmount'][] = array(
            'name' => 'Integer',
            'value' => null,
            'message' => "VatAmount must be an integer"
        );
        $validationData['vatAmount'][] = array(
            'name' => 'Regex',
            'value' => '/^\d{0,16}$/',
            'message' => "VatAmount must be between 1 and 16 digits"
        );

        $validationData['vatRate'][] = array(
            'name' => 'Number',
            'value' => null,
            'message' => "VatRate must be an number"
        );
        $validationData['vatRate'][] = array(
            'name' => 'Regex',
            'value' => '/^\d{1,2}.{0,1}\d{0,2}$/',
            'message' => "VatRate must be 1 to 2 digits followed by 1 to 2 decimal place"
        );

        return $validationData;
    }
}
