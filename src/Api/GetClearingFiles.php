<?php
/**
 * GetClearingFiles class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class GetClearingFiles
 * The API stub method for the getClearingFiles call
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/getclearingfile
 * @package Upg\Library\Api
 */
class GetClearingFiles extends AbstractApi
{
    /**
     * URI for the call
     */
    const GET_CLEARING_FILES_PATH = 'getClearingFiles';

    /**
     * Construct the stub
     *
     * @param Config                                $config  Merchant config
     * @param \Upg\Library\Request\GetClearingFiles $request Request to be sent
     */
    public function __construct(Config $config, \Upg\Library\Request\GetClearingFiles $request)
    {
        $this->request = $request;
        $this->awaitedResponse = self::RESPONSE_TYPE_OCTET_STREAM;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::GET_CLEARING_FILES_PATH);
    }
}
