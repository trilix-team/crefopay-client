<?php

namespace Upg\Library\Error\Serializer;

use Upg\Library\Error\Codes;

class CodesTest extends \PHPUnit\Framework\TestCase
{
    public function setUp()
    {}

    public function tearDown()
    {}

    /**
     * Test the correct range of non-error codes and error codes
     */
    public function testErrorStatusRange()
    {
        $this->assertEquals(false, Codes::checkCodeIsError(0));
        $this->assertEquals(true, Codes::checkCodeIsError(1000));
    }

    /**
     * Test technical error codes
     */
    public function testTechnicalErrors()
    {
        $this->assertEquals('An unknown error occurred, please contact CrefoPay support to find out details.', Codes::getErrorName(1000));
        $this->assertEquals('The calculated MAC is invalid.', Codes::getErrorName(1001));
        $this->assertEquals('The request is invalid. Please refer to the message for further explanation of the cause.', Codes::getErrorName(1002));
    }

    /**
     * Test notification callback error codes
     */
    public function testCallbackErrors()
    {
        $this->assertEquals('Unknown Error Code', Codes::getErrorName(6000));
        $this->assertNotEmpty(Codes::getErrorName(6001));
        $this->assertNotEmpty(Codes::getErrorName(6002));
        $this->assertNotEmpty(Codes::getErrorName(6003));
        $this->assertNotEmpty(Codes::getErrorName(6004));
        $this->assertNotEmpty(Codes::getErrorName(6005));
        $this->assertNotEmpty(Codes::getErrorName(6006));
        $this->assertNotEmpty(Codes::getErrorName(6007));
        $this->assertNotEmpty(Codes::getErrorName(6008));
        $this->assertNotEmpty(Codes::getErrorName(6009));
        $this->assertNotEmpty(Codes::getErrorName(6010));
        $this->assertNotEmpty(Codes::getErrorName(6011));
        $this->assertNotEmpty(Codes::getErrorName(6012));
        $this->assertNotEmpty(Codes::getErrorName(6013));
        $this->assertNotEmpty(Codes::getErrorName(6014));
        $this->assertNotEmpty(Codes::getErrorName(6015));
        $this->assertNotEmpty(Codes::getErrorName(6016));
        $this->assertNotEmpty(Codes::getErrorName(6017));
        $this->assertNotEmpty(Codes::getErrorName(6018));
    }

    /**
     * Test rejection error codes
     */
    public function testRejectErrors()
    {
        $this->assertNotEmpty(Codes::getErrorName(2000));
        $this->assertNotEmpty(Codes::getErrorName(2001));
        $this->assertNotEmpty(Codes::getErrorName(2002));
        $this->assertNotEmpty(Codes::getErrorName(2003));
        $this->assertNotEmpty(Codes::getErrorName(2004));
        $this->assertNotEmpty(Codes::getErrorName(2005));
        $this->assertNotEmpty(Codes::getErrorName(2006));
        $this->assertNotEmpty(Codes::getErrorName(2007));
        $this->assertNotEmpty(Codes::getErrorName(2008));
        $this->assertNotEmpty(Codes::getErrorName(2009));
        $this->assertNotEmpty(Codes::getErrorName(2010));
        $this->assertNotEmpty(Codes::getErrorName(2011));
        $this->assertNotEmpty(Codes::getErrorName(2012));
        $this->assertNotEmpty(Codes::getErrorName(2013));
        $this->assertNotEmpty(Codes::getErrorName(2014));
        $this->assertNotEmpty(Codes::getErrorName(2015));
        $this->assertNotEmpty(Codes::getErrorName(2016));
        $this->assertNotEmpty(Codes::getErrorName(2017));
        $this->assertNotEmpty(Codes::getErrorName(2018));
        $this->assertNotEmpty(Codes::getErrorName(2019));
        $this->assertNotEmpty(Codes::getErrorName(2020));
        $this->assertNotEmpty(Codes::getErrorName(2021));
        $this->assertNotEmpty(Codes::getErrorName(2022));
        $this->assertNotEmpty(Codes::getErrorName(2023));
        $this->assertNotEmpty(Codes::getErrorName(2024));
        $this->assertNotEmpty(Codes::getErrorName(2025));
        $this->assertNotEmpty(Codes::getErrorName(2026));
        $this->assertNotEmpty(Codes::getErrorName(2027));
        $this->assertNotEmpty(Codes::getErrorName(2028));
        $this->assertNotEmpty(Codes::getErrorName(2029));
        $this->assertNotEmpty(Codes::getErrorName(2030));
        $this->assertNotEmpty(Codes::getErrorName(2031));
        $this->assertNotEmpty(Codes::getErrorName(2032));
        $this->assertNotEmpty(Codes::getErrorName(2033));
        $this->assertNotEmpty(Codes::getErrorName(2034));
        $this->assertNotEmpty(Codes::getErrorName(2035));
    }
}