<?php
/**
 * InvalidUrl class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api\Exception;

/**
 * Class InvalidUrl
 * Raised if the URL for the API is invalid
 *
 * @package Upg\Library\Api\Exception
 */
class InvalidUrl extends AbstractException
{
    /**
     * Constructor
     *
     * @param string $url
     */
    public function __construct($url)
    {
        parent::__construct("URL is invalid: " . $url);
    }
}
