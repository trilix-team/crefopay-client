<?php
/**
 * CreateSubscription class file
 *
 * @author Vincent Mrose
 */

namespace Upg\Library\Api;

use Upg\Library\Config;

/**
 * Class CreateSubscription
 * API stub for the CreateSubscription call
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/en/topic/createsubscription
 * @package Upg\Library\Api
 */
class CreateSubscription extends AbstractApi
{
    /**
     * URI for the createSubscription call
     */
    const CREATE_SUBSCRIPTION_PATH = 'createSubscription';

    /**
     * Construct the API stub class
     *
     * @param Config                                  $config  Config for store owner
     * @param \Upg\Library\Request\CreateSubscription $request Request to be sent
     */
    public function __construct(Config $config, \Upg\Library\Request\CreateSubscription $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the url
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::CREATE_SUBSCRIPTION_PATH);
    }
}
