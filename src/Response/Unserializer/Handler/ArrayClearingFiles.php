<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\ClearingFile;
use Upg\Library\Response\Unserializer\Processor;

/**
 * Class ArrayClearingFiles
 *
 * Unserializer for clearingFiles data
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/
 * @package Upg\Library\Response\Unserializer\Handler
 */
class ArrayClearingFiles implements UnserializerInterface
{
    /**
     * Return the string of the property that the deserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'clearingFiles',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return array
     */
    public function unserializeProperty(Processor $processor, $value)
    {
        $data = array();

        foreach ($value as $clearingFileData) {
            $clearingFile = new ClearingFile();
            $clearingFile->setUnserializedData($clearingFileData);

            list($year, $month, $day) = explode('-', $clearingFileData['from']);
            $dateTime = new \DateTime();
            $dateTime->setDate($year, $month, $day);
            $clearingFile->setFrom($dateTime);

            list($year, $month, $day) = explode('-', $clearingFileData['to']);
            $dateTime = new \DateTime();
            $dateTime->setDate($year, $month, $day);
            $clearingFile->setTo($dateTime);

            $data[] = $clearingFile;
        }

        return $data;
    }
}
