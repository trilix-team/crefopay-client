<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Callback\Exception;

/**
 * Abstract class AbstractException
 *
 * @package Upg\Library\Callback\Exception
 */
abstract class AbstractException extends \Upg\Library\AbstractException
{
    /**
     * Constructor
     *
     * @param string $message
     */
    public function __construct($message = 'Callback Exception')
    {
        parent::__construct($message);
    }
}
