<?php
/**
 * ApiError class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Api\Exception;

use Upg\Library\Response\FailureResponse;

/**
 * Class ApiError
 *
 * Raised when API responds with an error for a call
 *
 * @package Upg\Library\Api\Exception
 */
class ApiError extends AbstractException
{
    /**
     * Parsed FailureResponse
     *
     * @var \Upg\Library\Response\FailureResponse
     */
    private $response;

    /**
     * Takes a failure response, the raw response and the HTTP code
     *
     * @param \Upg\Library\Response\FailureResponse $response
     * @param string                                $rawResponse
     * @param integer                               $httpCode
     */
    public function __construct(FailureResponse $response, $rawResponse, $httpCode)
    {
        $this->response = $response;
        parent::__construct(
            $response->getData('message'),
            $response->getData('resultCode'),
            $rawResponse,
            $response,
            $httpCode
        );
    }
}
