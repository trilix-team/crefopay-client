<?php

namespace Upg\Library\Tests\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\ClearingFile;
use Upg\Library\Response\Unserializer\Handler\ArrayClearingFiles;
use Upg\Library\Response\Unserializer\Processor;

class ArrayClearingFilesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Test if array of PaymentInstrument is returned
     */
    public function testSerialization()
    {
        $path = realpath(dirname(__FILE__));

        $json = file_get_contents("$path/ArrayClearingFilesTest.json");

        $value = json_decode($json, true);

        $arrayClearingFiles = new ArrayClearingFiles();

        $data = $arrayClearingFiles->unserializeProperty(new Processor(), $value);
        
        $elementCount = count($data);
        
        $this->assertEquals(2, $elementCount, "Array does not contain two elements");

        $objValidationRan = 0;

        foreach ($data as $clearingFile) {
            /**
             * @var ClearingFile $clearingFile
             */
            if ($objValidationRan === 0) {
                $this->assertEquals(113, $clearingFile->getClearingID());
                $this->assertEquals("2017-01-22", $clearingFile->getFrom()->format("Y-m-d"));
                $this->assertEquals("2017-01-29", $clearingFile->getTo()->format("Y-m-d"));
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 1) {
                $this->assertEquals(112, $clearingFile->getClearingID());
                $this->assertEquals("2017-01-15", $clearingFile->getFrom()->format("Y-m-d"));
                $this->assertEquals("2017-01-22", $clearingFile->getTo()->format("Y-m-d"));
                $objValidationRan++;
                break;
            }
        }

        $this->assertEquals($elementCount, $objValidationRan, "Not all object validations have run in this test");

    }
}
