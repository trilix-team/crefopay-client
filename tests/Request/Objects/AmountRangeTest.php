<?php

namespace Upg\Library\Tests\Request\Objects;

use Upg\Library\Request\Objects\Amount;
use Upg\Library\Request\Objects\AmountRange as AmountRange;

use Upg\Library\Tests\Request\AbstractRequestTest;
use Upg\Library\Validation\Validation;

class AmountRangeTest extends AbstractRequestTest
{

    public function testAmountRangeTestValidationSuccess()
    {
        $amountRange = new AmountRange();
        $amountRange->setMinimum(new Amount(1))
            ->setMaximum(new Amount(10000));

        $validation = new Validation();
        $validation->getValidator($amountRange);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testAmountRangeTestValidationMinimumFailure()
    {
        $amountRange = new AmountRange();

        $validation = new Validation();

        /** test the invalid amountRange logic */
        $amountRange->setMaximum(new Amount(1000000));
        $validation->getValidator($amountRange);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\AmountRange',
            'minimum',
            'Minimum amount is required',
            $data,
            "Minimum requirement was not validated"
        );
    }

    public function testAmountRangeTestValidationMaximumFailure()
    {
        $amountRange = new AmountRange();

        $validation = new Validation();

        /** test the invalid amountRange logic */
        $amountRange->setMinimum(new Amount(1000000));
        $validation->getValidator($amountRange);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'Upg\\Library\\Request\\Objects\\AmountRange',
            'maximum',
            'Maximum amount is required',
            $data,
            "Maximum requirement was not validated"
        );
    }
}
