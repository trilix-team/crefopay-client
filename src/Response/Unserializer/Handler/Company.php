<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\Company as CompanyClass;
use Upg\Library\Response\Unserializer\Processor;

/**
 * Class Company
 *
 * Unserializer for company data
 *
 * @link    https://www.manula.com/manuals/crefopayment/crefopay/1.2/de/topic/company
 * @package Upg\Library\Response\Unserializer\Handler
 */
class Company implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'companyData',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param mixed     $value
     *
     * @return CompanyClass
     */
    public function unserializeProperty(Processor $processor, $value)
    {

        $company = new CompanyClass();
        $company->setUnserializedData($value);

        return $company;
    }
}
