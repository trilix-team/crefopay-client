<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Serializer\Visitors\Exception;

/**
 * Class AbstractException
 *
 * Abstract exception for serializer errors
 *
 * @package Upg\Library\Serializer\Visitors\Exception
 */
abstract class AbstractException extends \Upg\Library\AbstractException
{
}
