<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Response\Unserializer\Handler;

use Upg\Library\Request\Objects\PaymentInstrument;
use Upg\Library\Response\Unserializer\Processor;

/**
 * Class ArrayPaymentInstruments
 *
 * @link
 * @package Upg\Library\Response\Unserializer\Handler
 */
class ArrayPaymentInstruments implements UnserializerInterface
{
    /**
     * Return the string of the property that the deserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'allowedPaymentInstruments',
            'paymentInstruments',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return array
     */
    public function unserializeProperty(Processor $processor, $value)
    {
        $data = array();

        foreach ($value as $paymentInstrumentData) {
            $paymentInstrument = new PaymentInstrument();
            $paymentInstrument->setUnserializedData($paymentInstrumentData);

            if ($paymentInstrument->getPaymentInstrumentType() == PaymentInstrument::PAYMENT_INSTRUMENT_TYPE_CARD) {
                list($year, $month) = explode('-', $paymentInstrumentData['validity']);
                $dateTime = new \DateTime();
                $dateTime->setDate($year, $month, 1);
                $paymentInstrument->setValidity($dateTime);
            }

            $data[] = $paymentInstrument;
        }

        return $data;
    }
}
