<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace Upg\Library\Callback\Exception;

/**
 * Class MacValidation
 *
 * @package Upg\Library\Callback\Exception
 */

class MacValidation extends AbstractException
{
    /**
     * Constructor
     *
     * @param string $calculated
     * @param string $sentMac
     * @param array  $data
     */
    public function __construct($calculated, $sentMac, array $data)
    {
        $string = json_encode($data);
        parent::__construct("MAC Validation failed. Received: $sentMac; Calculated: $calculated; Data: " . $string);
    }
}
