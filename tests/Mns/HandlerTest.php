<?php

namespace Upg\Library\Tests\Mns;

use Upg\Library\Mns\Handler;
use Upg\Library\Mns\Exception\ParamNotProvided;
use Upg\Library\Config;

class HandlerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * Log file location
     * @var string
     */
    protected static $file = './mns.log';

    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public static function tearDownAfterClass()
    {
        unlink(self::$file);
    }

    public function setUp()
    {
        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
            'baseUrl' => "http://www.something.com/",
            'logEnabled' => true,
            'logLevel' => Config::LOG_LEVEL_DEBUG,
            'logLocationMNS' => $this->getLogLocation()
        ));
    }

    public function tearDown()
    {
        unset($this->config);
    }

    public function getLogLocation()
    {
        return self::$file;
    }

    public function getNonLoggingConfig()
    {
        return new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
            'baseUrl' => "http://www.something.com/",
            'logEnabled' => true,
            'logLevel' => Config::LOG_LEVEL_DEBUG,
        ));
    }

    /**
     * Test if no log is created if no location is set
     */
    public function testNoLogCreation()
    {
        $data = array(
            'merchantID' => 1,
            'storeID' => 2,
            'orderID' => 3,
            'captureID' => 4,
            'merchantReference' => 5,
            'paymentReference' => 6,
            'userID' => 7,
            'amount' => 9,
            'currency' => 10,
            'transactionStatus' => 12,
            'orderStatus' => 13,
            'additionalData' => 15,
            'timestamp' => 16,
            'version' => 1.5,
            'mac' => 'fbdc46ef7ab1ccf195781983caf60782a81bd0f1',
        );

        $processor = new \Upg\Library\Tests\Mock\Mns\MockProcessor();

        $handler = new Handler($this->getNonLoggingConfig(), $data, $processor);
        $handler->run();

        $this->assertFileNotExists($this->getLogLocation());
    }

    public function testSuccessfulProcess()
    {
        $data = array(
            'merchantID' => 1,
            'storeID' => 2,
            'orderID' => 3,
            'captureID' => 4,
            'merchantReference' => 5,
            'paymentReference' => 6,
            'userID' => 7,
            'amount' => 9,
            'currency' => 10,
            'transactionStatus' => 12,
            'orderStatus' => 13,
            'additionalData' => 15,
            'timestamp' => 16,
            'version' => 1.5,
            'mac' => 'fbdc46ef7ab1ccf195781983caf60782a81bd0f1',
        );

        $processor = new \Upg\Library\Tests\Mock\Mns\MockProcessor();

        $handler = new Handler($this->config, $data, $processor);
        $handler->run();

        $this->assertArraySubset($processor->data, $data);

        $this->assertFileExists($this->getLogLocation());
    }

    /**
     * Test if ParamNotProvided exception is thrown
     *
     * @expectedException \Upg\Library\Mns\Exception\ParamNotProvided
     * @throws ParamNotProvided
     * @throws \Upg\Library\Callback\Exception\MacValidation
     */
    public function testParamNotProvidedException()
    {
        $data = array(
            'merchantID' => 1,
            'storeID' => 2,
            'captureID' => 4,
            'merchantReference' => 5,
            'paymentReference' => 6,
            'userID' => 7,
            'amount' => 9,
            'currency' => 10,
            'transactionStatus' => 12,
            'orderStatus' => 13,
            'additionalData' => 15,
            'timestamp' => 16,
            'version' => 1.5,
            'mac' => 'f0008f749bd490332cf66795f64c4197593916401',
        );

        $processor = new \Upg\Library\Tests\Mock\Mns\MockProcessor();

        try {
            $handler = new Handler($this->config, $data, $processor);
            $handler->run();
        } catch(ParamNotProvided $exception) {
            $this->assertFileExists($this->getLogLocation());
            throw $exception;
        }
    }

    /**
     * Test if the mac validation exception is thrown
     * @expectedException \Upg\Library\Callback\Exception\MacValidation
     */
    public function testMacValidationException()
    {
        $data = array(
            'merchantID' => 1,
            'storeID' => 2,
            'orderID' => 3,
            'captureID' => 4,
            'merchantReference' => 5,
            'paymentReference' => 6,
            'userID' => 7,
            'amount' => 9,
            'currency' => 10,
            'transactionStatus' => 12,
            'orderStatus' => 13,
            'additionalData' => 15,
            'timestamp' => 16,
            'version' => 1.5,
            'mac' => 'fbdc46ef7ab1ccf195781983caf60782a81bb1e2',
        );

        $processor = new \Upg\Library\Tests\Mock\Mns\MockProcessor();

        $handler = new Handler($this->config, $data, $processor);
        $handler->run();
    }
}
